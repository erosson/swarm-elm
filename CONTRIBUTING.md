# Contributing to Swarm Simulator

## Contributing code

Most Swarm Simulator code is written in [Elm](https://elm-lang.org), with a bit of Javascript. The build system is Yarn and NodeJS-based.

[Merge requests (aka pull requests)](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html) are awesome. UI enhancements and bug fixes are very likely to be accepted. Gameplay changes (new units/upgrades, balance changes) are very *unlikely* to be accepted. If in doubt, talk to @erosson first.

Swarmsim is GPLv3-licensed. I'll assume your contributions will also be GPLv3-licensed when merging them.

### Feature flags

Incomplete features and other changes I'm not yet ready for everyone to see are hidden behind *feature flags*. Their code runs in production, but it's hidden without a specific URL parameter. For example, `?debug=1` is a feature flag that hides the debugging tools.

TODO discuss usage, implementation. Route/Feature.elm

### Building and running

* Clone the [git repository](https://gitlab.com/erosson/swarm-elm/tree/master) to your machine.
  * TODO: cloud9 directions/link, if I ever get around to setting that up
* `yarn` sets up the project and git hooks. Do this first.
* `yarn start` runs the server. It automatically refreshes whenever something changes.
* `yarn test` runs the automated tests. These also run before committing code and pushing.
  * `yarn test --watch` keeps running the automated tests whenever something changes

## Contributing translations

Swarm Simulator supports multiple languages, but I'm only fluent in English. I would love your help translating!

English is the only complete language so far, so you could be the first. There may be a few bumps along the way, but I want to support other languages.

Adding a new language has three steps:

### 1) Translate all the user interface text

Translation-text files for [English: en-us.json](https://gitlab.com/erosson/swarm-elm/tree/master/packages/www/public/lang/en-us.json), or [all languages](https://gitlab.com/erosson/swarm-elm/tree/master/packages/www/public/lang/).

Swarm Simulator has a lot of text. This will be most of the work. Before you start, [see if someone's already started translating your language](https://gitlab.com/erosson/swarm-elm/tree/master/packages/www/public/lang/)!

The format is `"some.text.id": "Translated text."`. Don't change the ids, on the left. A complete translation will need to translate the text on the right for *every id* in the English file. I'm willing to use partially complete translations; missing ids will use the English translation.

[Swarmsim's Translating Tools](https://elm.swarmsim.com/translator) can help test your translations. Paste your translation file into the text box, then go play the game for a while. The game will immediately use your translated text as long as `?translator=1` is in the URL.

Some text has {brackets}. This is where the program inserts numbers and other things that change over time. If you change them, the program won't know where to insert numbers, so be careful not to do too much of that. This bracketed text is loosely based on [MessageFormat](https://messageformat.github.io/messageformat/page-guide), but there are some differences. I haven't yet documented the differences, so please ask if you have questions here!

Once you have a (mostly-)complete translation-text file:

* If you're comfortable with Git, [create a merge request on GitLab](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).
* Otherwise, [put the file on Pastebin](https://pastebin.com), and either [create a GitLab issue](https://gitlab.com/erosson/swarm-elm/issues/new) or [send me the file on Reddit](https://www.reddit.com/message/compose/?to=kawaritai).

### 2) Language-specific localization code

Localization for [English: En_US.elm](https://gitlab.com/erosson/swarm-elm/blob/master/packages/www/src/View/Lang/En_US.elm), or [all languages](https://gitlab.com/erosson/swarm-elm/tree/master/packages/www/src/View/Lang).

Some things specific to your language, like number and list formatting, can't be written in the `.json` file above. They need more complex program code.

If you're a programmer and want to take a stab at this - great! Copy the English file above to get started, and send me a merge request when you're done.

Otherwise, you'll need to talk to me about how formatting for your language is different than English. For example:

* Lists ("A, B, and C")
* Numbers ("1,234,567.89"; "1.23 million")
  * TODO: number suffixes (million, billion, trillion...) will need a separate file per language too. [English suffixes are here](https://gitlab.com/erosson/swarm-elm/blob/master/packages/number-suffixes/src/NumberSuffixData.elm), but that file isn't localizable. Send me a list of suffixes for your language, and I'll eventually figure this out.
* Any other differences?

### 3) Release the translation to the public

Once I'm happy with your translation, it's time to add it to the options screen so everyone can use it. I'll do this one.

Thanks so much for your help!
