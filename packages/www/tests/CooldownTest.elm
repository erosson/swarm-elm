module CooldownTest exposing (all)

import Cooldown exposing (Cooldown)
import Duration exposing (Duration)
import Expect
import Test exposing (..)
import Time exposing (Posix)


all : Test
all =
    describe "cooldown tests"
        [ test "from ready" <|
            \_ ->
                Cooldown.Ready
                    |> Expect.all
                        [ Cooldown.tick (posix 100) >> Expect.equal Cooldown.Ready
                        , Cooldown.trigger (posix 100) (Duration.fromMillis 200) >> Expect.equal (Just <| Cooldown.Until <| posix 300)
                        , Cooldown.repeat (posix 100) (Duration.fromMillis 200) >> Expect.equal ( 1, Cooldown.Until <| posix 300 )
                        ]
        , test "from cooldown" <|
            \_ ->
                Cooldown.Until (posix 100)
                    |> Expect.all
                        [ Cooldown.tick (posix 99) >> Expect.equal (Cooldown.Until <| posix 100)
                        , Cooldown.tick (posix 100) >> Expect.equal Cooldown.Ready
                        , Cooldown.tick (posix 101) >> Expect.equal Cooldown.Ready
                        , Cooldown.tick (posix 150) >> Expect.equal Cooldown.Ready
                        , Cooldown.trigger (posix 99) (Duration.fromMillis 200) >> Expect.equal Nothing
                        , Cooldown.trigger (posix 100) (Duration.fromMillis 200) >> Expect.equal (Just <| Cooldown.Until <| posix 300)
                        , Cooldown.trigger (posix 101) (Duration.fromMillis 200) >> Expect.equal (Just <| Cooldown.Until <| posix 301)
                        , Cooldown.trigger (posix 150) (Duration.fromMillis 200) >> Expect.equal (Just <| Cooldown.Until <| posix 350)
                        , Cooldown.repeat (posix 99) (Duration.fromMillis 200) >> Expect.equal ( 0, Cooldown.Until <| posix 100 )
                        , Cooldown.repeat (posix 100) (Duration.fromMillis 200) >> Expect.equal ( 1, Cooldown.Until <| posix 300 )
                        , Cooldown.repeat (posix 101) (Duration.fromMillis 200) >> Expect.equal ( 1, Cooldown.Until <| posix 300 )
                        , Cooldown.repeat (posix 150) (Duration.fromMillis 200) >> Expect.equal ( 1, Cooldown.Until <| posix 300 )
                        , Cooldown.repeat (posix 350) (Duration.fromMillis 200) >> Expect.equal ( 2, Cooldown.Until <| posix 500 )
                        ]
        ]


posix : Int -> Posix
posix =
    Time.millisToPosix
