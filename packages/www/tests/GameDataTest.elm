module GameDataTest exposing (all)

-- this import is the entire test

import Expect
import GameDataFixture exposing (gameData)
import Test exposing (..)


all : Test
all =
    test "GameDataFixture parsed successfully" <| \_ -> Expect.pass
