module TestUtil.Expect exposing (decimal, decimalGreaterThan, decimals, decimalsGreaterThan, isNotVisible, isVisible, remoteFailure, remoteSuccess)

import Decimal exposing (Decimal)
import Expect
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData as GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameDataFixture exposing (gameData)
import RemoteData exposing (RemoteData)
import TestUtil


decimal : String -> Decimal -> Expect.Expectation
decimal expectedStr actual =
    let
        expected =
            Decimal.fromString expectedStr |> TestUtil.unsafeJust ("invalid decimal.fromString: " ++ expectedStr)
    in
    decimals expected actual


decimals : Decimal -> Decimal -> Expect.Expectation
decimals expected actual =
    if expected == actual then
        -- the most basic support for decimals too large to be floats
        Expect.equal expected actual

    else
        case ( Decimal.toFiniteFloat expected, Decimal.toFiniteFloat actual ) of
            ( Just e, Just a ) ->
                Expect.within (Expect.Relative 0.01) e a

            _ ->
                Expect.true
                    ("Expect decimals within 1% of each other: " ++ Decimal.toString expected ++ ", " ++ Decimal.toString actual)
                    ((Decimal.abs (Decimal.div expected actual) |> Decimal.toFloat) < 0.01)


decimalGreaterThan : String -> Decimal -> Expect.Expectation
decimalGreaterThan expectedStr actual =
    let
        expected =
            Decimal.fromString expectedStr |> TestUtil.unsafeJust ("invalid decimal.fromString: " ++ expectedStr)
    in
    decimalsGreaterThan expected actual


decimalsGreaterThan : Decimal -> Decimal -> Expect.Expectation
decimalsGreaterThan expected actual =
    Expect.true
        ("Expect " ++ Decimal.toString actual ++ " > " ++ Decimal.toString expected)
        (Decimal.gt actual expected)


isVisible : String -> UnitsSnapshot -> Expect.Expectation
isVisible name =
    TestUtil.isVisible name >> Expect.true ("Expected item to be visible: " ++ name)


isNotVisible : String -> UnitsSnapshot -> Expect.Expectation
isNotVisible name =
    TestUtil.isVisible name >> Expect.false ("Expected item NOT to be visible: " ++ name)


{-| based on <https://github.com/elm-explorations/test/blob/1.2.0/src/Expect.elm#L451>
-}
remoteSuccess : RemoteData e a -> Expect.Expectation
remoteSuccess remote =
    case remote of
        RemoteData.Success _ ->
            Expect.pass

        _ ->
            Expect.fail <| "expected RemoteData.Success _, got: \n" ++ Debug.toString remote


{-| based on <https://github.com/elm-explorations/test/blob/1.2.0/src/Expect.elm#L451>
-}
remoteFailure : RemoteData e a -> Expect.Expectation
remoteFailure remote =
    case remote of
        RemoteData.Failure _ ->
            Expect.pass

        _ ->
            Expect.fail <| "expected RemoteData.Failure _, got: \n" ++ Debug.toString remote
