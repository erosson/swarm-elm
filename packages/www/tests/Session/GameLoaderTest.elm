module Session.GameLoaderTest exposing (all)

import Expect
import GameDataFixture exposing (gameData)
import Json.Decode as D
import Json.Encode as E
import RemoteData exposing (RemoteData)
import Session.GameLoader as GameLoader
import Test exposing (..)
import TestUtil
import TestUtil.Expect
import Time exposing (Posix)


all : Test
all =
    describe "GameLoader"
        [ test "loads empty save; new game" <|
            \_ ->
                GameLoader.empty (Ok gameData)
                    |> GameLoader.setJson (RemoteData.Success Nothing)
                    |> GameLoader.tryReady (Just now)
                    |> GameLoader.toRemote
                    |> TestUtil.Expect.remoteSuccess
        , test "loads nonempty save; new game" <|
            \_ ->
                GameLoader.empty (Ok gameData)
                    |> GameLoader.setJson (RemoteData.Success <| Just example)
                    |> GameLoader.tryReady (Just now)
                    |> GameLoader.toRemote
                    |> TestUtil.Expect.remoteSuccess
        , test "loads nonempty broken save" <|
            \_ ->
                GameLoader.empty (Ok gameData)
                    |> GameLoader.setJson (RemoteData.Success <| Just brokenExample)
                    |> GameLoader.tryReady (Just now)
                    |> GameLoader.toRemote
                    |> TestUtil.Expect.remoteFailure
        ]


now : Posix
now =
    Time.millisToPosix 1


brokenExample : D.Value
brokenExample =
    "{}" |> D.decodeString D.value |> TestUtil.unsafeOk


example : D.Value
example =
    exampleStr |> D.decodeString D.value |> TestUtil.unsafeOk


exampleStr : String
exampleStr =
    "{\"unittypes\":{\"invisiblehatchery\":\"1\",\"meat\":\"35\",\"larva\":\"10\",\"cocoon\":\"0\",\"territory\":\"0\",\"energy\":\"0\",\"respecEnergy\":\"0\",\"mtxEnergy\":\"0\",\"nexus\":\"0\",\"crystal\":\"0\",\"mutagen\":\"0\",\"premutagen\":\"0\",\"ascension\":\"0\",\"freeRespec\":\"4\",\"drone\":\"0\",\"queen\":\"0\",\"nest\":\"0\",\"greaterqueen\":\"0\",\"hive\":\"0\",\"hivequeen\":\"0\",\"empress\":\"0\",\"prophet\":\"0\",\"goddess\":\"0\",\"pantheon\":\"0\",\"pantheon2\":\"0\",\"pantheon3\":\"0\",\"pantheon4\":\"0\",\"pantheon5\":\"0\",\"overmind\":\"0\",\"overmind2\":\"0\",\"overmind3\":\"0\",\"overmind4\":\"0\",\"overmind5\":\"0\",\"overmind6\":\"0\",\"swarmling\":\"0\",\"stinger\":\"0\",\"spider\":\"0\",\"mosquito\":\"0\",\"locust\":\"0\",\"roach\":\"0\",\"giantspider\":\"0\",\"centipede\":\"0\",\"wasp\":\"0\",\"devourer\":\"0\",\"goon\":\"0\",\"nightbug\":\"0\",\"moth\":\"0\",\"bat\":\"0\",\"mutanthatchery\":\"0\",\"mutantbat\":\"0\",\"mutantclone\":\"0\",\"mutantswarmwarp\":\"0\",\"mutantrush\":\"0\",\"mutanteach\":\"0\",\"mutantfreq\":\"0\",\"mutantnexus\":\"0\",\"mutantarmy\":\"0\",\"mutantmeat\":\"0\"},\"date\":{\"started\":\"2019-01-23T20:44:25.828Z\",\"restarted\":\"2019-01-23T20:44:25.828Z\",\"saved\":\"2019-01-23T20:44:29.255Z\",\"reified\":\"2019-01-23T20:44:25.828Z\",\"closed\":\"2019-01-23T20:44:25.828Z\"},\"options\":{},\"upgrades\":{},\"statistics\":{\"byUnit\":{},\"byUpgrade\":{},\"clicks\":0},\"achievements\":{\"debug\":3416},\"watched\":{},\"skippedMillis\":0,\"version\":{\"started\":\"1.1.11\",\"saved\":\"1.1.11\"}}"
