module View.DescriptionVarsTest exposing (all)

import Duration
import Expect
import Fools
import Game exposing (Game)
import GameDataFixture exposing (gameData)
import Locale
import Test exposing (..)
import TestUtil
import TestUtil.Expect
import Time exposing (Posix)
import View.DescriptionVars as Desc
import View.Lang as T exposing (Lang)


all : Test
all =
    describe "description-var"
        [ test "expansions/empty" <|
            \_ ->
                Game.empty gameData now
                    |> Expect.all
                        [ \g -> Desc.evalField g "hatchery" Desc.Prod |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "1"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "expansion_base" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "1.1"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "expansion" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "1"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "mutation" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "1"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "bonus" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "1"
                        ]
        , test "errors" <|
            \_ ->
                Game.empty gameData now
                    |> Expect.all
                        [ \g -> Desc.evalLabel g "fail" Desc.Prod "bonus" |> Expect.err
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "fail" |> Expect.err
                        ]
        , test "expansions" <|
            \_ ->
                Game.empty gameData now
                    |> Game.updateUpgrades (TestUtil.decimals "expansion: 3")
                    |> TestUtil.updateUnits "hatchery: 37, mutanthatchery: 9990"
                    |> Expect.all
                        -- 4 * 1.331 = 5.324
                        [ \g -> Desc.evalField g "hatchery" Desc.Prod |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "5.324"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "expansion_base" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "1.1"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "expansion" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "1.331"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "mutation" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "4"
                        , \g -> Desc.evalLabel g "hatchery" Desc.Prod "bonus" |> TestUtil.unsafeOk |> TestUtil.Expect.decimal "5.324"
                        ]
        , describe "desc messages" <|
            let
                game =
                    Game.empty gameData now

                lang =
                    T.localeToLang gameData Fools.Off Nothing Nothing Locale.En_US
            in
            List.map (\text -> test ("desc message: " ++ Debug.toString text) <| \_ -> lang.rstr text |> Expect.ok)
                [ T.Title
                , T.Header
                , T.FrameRate 42 { max = Duration.fromMillis 42, avg = Duration.fromMillis 42 }
                , T.ServiceWorkerUpdateDownloaded
                , T.UnitDesc (Desc.evalVar game) (TestUtil.unitId "hatchery")
                , T.UpgradeDesc (Desc.evalVar game) (TestUtil.upgradeId "expansion")
                , T.UnitDesc (Desc.evalVar game) (TestUtil.unitId "nightbug")
                , T.UnitDesc (Desc.evalVar game) (TestUtil.unitId "moth")
                , T.UnitDesc (Desc.evalVar game) (TestUtil.unitId "bat")
                , T.UnitDesc (Desc.evalVar game) (TestUtil.unitId "mutantnexus")
                ]
        ]


now : Posix
now =
    Time.millisToPosix 0
