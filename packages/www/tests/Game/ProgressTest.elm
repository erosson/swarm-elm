module Game.ProgressTest exposing (all)

import Duration exposing (Duration)
import Expect
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot
import GameDataFixture exposing (gameData)
import Test exposing (..)
import TestUtil
import TestUtil.Expect
import Time exposing (Posix)


all : Test
all =
    describe "Progress duration"
        (let
            game0 : Game
            game0 =
                Game.empty gameData now
         in
         [ test "expected infinite duration" <|
            \_ ->
                game0
                    |> TestUtil.updateUnits "hatchery: 1, meat: 100"
                    |> TestUtil.setBuyN "hatchery" "1"
                    |> TestUtil.progressDuration "hatchery"
                    |> Expect.equal [ ( "meat", Nothing ) ]
         , test "expected deg1 (linear) duration" <|
            \_ ->
                game0
                    |> TestUtil.updateUnits "hatchery: 1, drone: 10, meat: 200"
                    |> TestUtil.setBuyN "hatchery" "1"
                    |> TestUtil.progressDuration "hatchery"
                    |> Expect.equal [ ( "meat", Just <| Duration.fromSecs 10 ) ]
         , test "expected deg2 duration" <|
            \_ ->
                game0
                    |> TestUtil.updateUnits "hatchery: 1, queen: 100, meat: 200"
                    |> TestUtil.setBuyN "hatchery" "1"
                    |> TestUtil.progressDuration "hatchery"
                    -- The tolerance is 200ms, but it should be deterministic
                    |> Expect.equal [ ( "meat", Just <| Duration.fromMillis 950 ) ]
         , test "expected deg2 duration (2)" <|
            \_ ->
                game0
                    -- a single queen is simply the polynomial t^2; easy to check by hand
                    |> TestUtil.updateUnits "hatchery: 1, queen: 1, meat: 0"
                    |> TestUtil.setBuyN "hatchery" "1"
                    |> TestUtil.progressDuration "hatchery"
                    -- The tolerance is 200ms, but it should be deterministic
                    |> Expect.equal [ ( "meat", Just <| Duration.fromMillis 17291 ) ]
         , test "expected deg2 duration (3)" <|
            \_ ->
                game0
                    |> TestUtil.updateUnits "hatchery: 2, queen: 1, meat: 0"
                    |> TestUtil.setBuyN "hatchery" "1"
                    |> TestUtil.progressDuration "hatchery"
                    -- The tolerance is 200ms, but it should be deterministic
                    |> Expect.equal [ ( "meat", Just <| Duration.fromMillis 54787 ) ]
         , test "expected deg2 duration (4)" <|
            \_ ->
                game0
                    |> TestUtil.updateUnits "hatchery: 3, queen: 1, meat: 0"
                    |> TestUtil.setBuyN "hatchery" "1"
                    |> TestUtil.progressDuration "hatchery"
                    -- The tolerance is 200ms, but it should be deterministic
                    |> Expect.equal [ ( "meat", Just <| Duration.fromMillis 173204 ) ]
         , test "expected deg2 duration (5)" <|
            \_ ->
                game0
                    -- 300 = t^2 + 10t + 100
                    -- t = 10
                    |> TestUtil.updateUnits "hatchery: 1, queen: 1, drone: 10, meat: 100"
                    |> TestUtil.setBuyN "hatchery" "1"
                    |> TestUtil.progressDuration "hatchery"
                    -- The tolerance is 200ms, but it should be deterministic
                    |> Expect.equal [ ( "meat", Just <| Duration.fromMillis 10016 ) ]
         ]
        )


nowMs : Int
nowMs =
    0


now : Posix
now =
    Time.millisToPosix nowMs
