module Game.OfflineSyncTest exposing (all)

import Expect
import Game exposing (Game)
import Game.OfflineSync as OfflineSync
import GameDataFixture exposing (gameData)
import RemoteData exposing (RemoteData)
import Test exposing (..)
import Time exposing (Posix)


all : Test
all =
    describe "OfflineSync tests"
        [ test "user-loading" <|
            \_ ->
                OfflineSync.rawCreate RemoteData.Loading (RemoteData.Success emptyGame0) (Just "encoded")
                    |> Expect.equal RemoteData.Loading
        , test "empty-user" <|
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success Nothing) (RemoteData.Success emptyGame0) (Just "encoded")
                    |> Expect.equal (RemoteData.Success OfflineSync.UseLocal)
        , test "empty-remote-encodedgame" <|
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just Nothing) (RemoteData.Success emptyGame0) (Just "encoded")
                    |> Expect.equal (RemoteData.Success OfflineSync.UseLocal)
        , test "empty-remote-game" <|
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.NotAsked, "encoded" )) (RemoteData.Success emptyGame0) (Just "encoded")
                    -- this should never happen - user.game starts in the loading state, while waiting for js to decode it
                    |> Expect.equal (RemoteData.Failure "user.game is empty")
        , test "loading-remote-game" <|
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Loading, "encoded" )) (RemoteData.Success emptyGame0) (Just "encoded")
                    |> Expect.equal RemoteData.Loading
        , test "failure-remote-game" <|
            \_ ->
                -- TODO copy local to remote?
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Failure "oops", "encoded" )) (RemoteData.Success emptyGame0) (Just "encoded")
                    |> Expect.equal (RemoteData.Failure "oops")
        , test "empty-local-game" <|
            -- this one happens when the local game is brand new, just auto-created. Maybe the user cleared their saved-game cookie but not their login cookie.
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Success emptyGame0, "encoded" )) (RemoteData.Success emptyGame0) Nothing
                    |> Expect.equal (RemoteData.Success <| OfflineSync.UseRemote { game = emptyGame0, encoded = "encoded" })
        , test "loading-local-game" <|
            -- remote finished loading before local does? this should be very rare!
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Success emptyGame0, "encoded" )) RemoteData.Loading (Just "encoded")
                    |> Expect.equal RemoteData.Loading
        , test "failure-local-game" <|
            -- TODO: copy remote to local? what if local is corrupt and needs rescuing?
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Success emptyGame0, "encoded" )) (RemoteData.Failure "oops") (Just "encoded")
                    |> Expect.equal (RemoteData.Failure "oops")
        , test "empty-both-games" <|
            \_ ->
                -- this happens for brand new visitors to the site. Use the already-created local game
                OfflineSync.rawCreate (RemoteData.Success <| Just Nothing) (RemoteData.Success emptyGame0) Nothing
                    |> Expect.equal (RemoteData.Success OfflineSync.UseLocal)
        , test "conflict" <|
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Success (emptyGame 1), "encoded1" )) (RemoteData.Success (emptyGame 2)) (Just "encoded2")
                    |> RemoteData.map OfflineSync.isConflict
                    |> Expect.equal (RemoteData.Success True)
        , test "failure-both" <|
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Failure "oops1", "encoded1" )) (RemoteData.Failure "oops2") (Just "encoded2")
                    |> Expect.equal (RemoteData.Failure "oops1")
        , test "no-conflict: exact-match" <|
            \_ ->
                OfflineSync.rawCreate (RemoteData.Success <| Just <| Just ( RemoteData.Success (emptyGame 1), "encoded" )) (RemoteData.Success (emptyGame 1)) (Just "encoded")
                    |> Expect.equal (RemoteData.Success OfflineSync.UseLocal)
        ]


emptyGame : Int -> Game
emptyGame time0 =
    Game.empty gameData (Time.millisToPosix time0)


emptyGame0 : Game
emptyGame0 =
    emptyGame 1
