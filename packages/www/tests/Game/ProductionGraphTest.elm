module Game.ProductionGraphTest exposing (all)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Expect
import Game.ProductionGraph as ProdGraph exposing (Graph)
import GameData exposing (GameData)
import GameData.Unit as Unit exposing (Unit)
import GameDataFixture exposing (gameData)
import Graph
import IntDict
import Polynomial exposing (Polynomial)
import Test exposing (..)
import TestUtil
import TestUtil.Expect


{-| an empty save from old-swarmsim; same one as the empty example in persist.test.js
-}
all : Test
all =
    describe "ProdGraph tests"
        [ test "queen children" <|
            \_ ->
                let
                    unit =
                        gameData |> GameData.unitBySlug "queen" |> TestUtil.unsafeJust "no queens?"
                in
                ProdGraph.graph gameData (always Nothing)
                    |> ProdGraph.unitChildren unit
                    |> Graph.nodes
                    |> List.map (\n -> Unit.name n.label)
                    |> Expect.equal [ "meat", "drone", "queen" ]
        , test "queen parents" <|
            \_ ->
                let
                    unit =
                        gameData |> GameData.unitBySlug "queen" |> TestUtil.unsafeJust "no queens?"
                in
                ProdGraph.graph gameData (always Nothing)
                    |> ProdGraph.unitParents unit
                    |> Graph.nodes
                    |> List.map (\n -> Unit.name n.label)
                    |> List.take 3
                    |> Expect.equal [ "queen", "nest", "greaterqueen" ]
        , test "prune: remove unused units before polynomials" <|
            \_ ->
                let
                    get name =
                        Dict.get name >> Maybe.withDefault (Decimal.fromFloat (0 / 0))
                in
                ProdGraph.graph gameData (always Nothing)
                    |> ProdGraph.polynomials (TestUtil.decimals "queen: 1, hatchery: 0, swarmling: 0")
                    |> Dict.map (\_ -> Polynomial.evaluate (Duration.fromSecs 0))
                    |> Expect.all
                        [ get "queen" >> TestUtil.Expect.decimal "1"

                        -- children are in the graph - their count is zero, but their parents' count is nonzero
                        , get "drone" >> TestUtil.Expect.decimal "0"
                        , get "meat" >> TestUtil.Expect.decimal "0"

                        -- this one has no parents, but it was already present, so it stays
                        , get "hatchery" >> TestUtil.Expect.decimal "0"
                        , get "swarmling" >> TestUtil.Expect.decimal "0"

                        -- children are added, even though their parent's count is zero
                        , get "larva" >> TestUtil.Expect.decimal "0"
                        , get "territory" >> TestUtil.Expect.decimal "0"

                        -- no parents in the graph, don't output a polynomial
                        , Dict.get "nest" >> Expect.equal Nothing
                        , Dict.get "overmind6" >> Expect.equal Nothing
                        ]
        , test "big numbers" <|
            \_ ->
                let
                    get name =
                        Dict.get name >> Maybe.withDefault (Decimal.fromFloat (0 / 0))
                in
                ProdGraph.graph gameData (always Nothing)
                    |> ProdGraph.polynomials (TestUtil.decimals "drone: 1e400")
                    |> Dict.map (\_ -> Polynomial.evaluate (Duration.fromSecs 0))
                    |> Expect.all
                        [ get "drone" >> Expect.equal (TestUtil.decimal "1e400")
                        , get "meat" >> Expect.equal Decimal.zero
                        ]
        , test "bigger numbers" <|
            \_ ->
                let
                    get name =
                        Dict.get name >> Maybe.withDefault (Decimal.fromFloat (0 / 0))
                in
                ProdGraph.graph gameData (always Nothing)
                    |> ProdGraph.polynomials (TestUtil.decimals "overmind6: 1e10000")
                    |> Dict.map (\_ -> Polynomial.evaluate (Duration.fromSecs 1))
                    |> Expect.all
                        [ get "overmind6" >> Expect.equal (TestUtil.decimal "1e10000")
                        , get "overmind5" >> Expect.equal (TestUtil.decimal "2e10001")
                        ]
        , test "big coefficients" <|
            \_ ->
                let
                    get name =
                        Dict.get name >> Maybe.withDefault (Decimal.fromFloat (0 / 0))
                in
                ProdGraph.graph gameData (TestUtil.getVar <| TestUtil.decimals "droneprod: 4000")
                    |> ProdGraph.polynomials (TestUtil.decimals "drone: 1")
                    |> Dict.map (\_ -> Polynomial.evaluate (Duration.fromSecs 1))
                    |> Expect.all
                        [ get "drone" >> Expect.equal (TestUtil.decimal "1")
                        , get "meat" >> Expect.equal (TestUtil.decimal "1.3182040934310915e1204")
                        ]
        ]
