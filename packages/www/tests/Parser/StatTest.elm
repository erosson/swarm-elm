module Parser.StatTest exposing (all)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Expect
import GameData.Util
import Json.Decode as D
import Json.Decode.Pipeline as P
import Parser
import Parser.Stat
import Set exposing (Set)
import Test exposing (..)
import TestUtil


{-| an empty save from old-swarmsim; same one as the empty example in persist.test.js
-}
all : Test
all =
    describe "Stat tests"
        [ test "parse val" <|
            \_ ->
                Parser.Stat.parse "3"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> evalWithZero (always Nothing) >> Expect.equal (decimal "3")
                        ]
        , test "parse val with spaces" <|
            \_ ->
                Parser.Stat.parse "    -3  "
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> evalWithZero (always Nothing) >> Expect.equal (decimal "-3")
                        ]
        , test "parse var" <|
            \_ ->
                Parser.Stat.parse "a"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ evalWithZero (getVar []) >> Expect.equal (decimal "0")
                                , Parser.Stat.eval (getVar []) >> Expect.err
                                , evalWithZero (getVar [ ( "a", "3" ) ]) >> Expect.equal (decimal "3")
                                , Parser.Stat.eval (getVar [ ( "a", "3" ) ]) >> Expect.equal (Ok <| decimal "3")
                                ]
                        ]
        , test "parse var + val" <|
            \_ ->
                Parser.Stat.parse "a + 3"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ evalWithZero (getVar []) >> Expect.equal (decimal "3")
                                , Parser.Stat.eval (getVar []) >> Expect.err
                                , evalWithZero (getVar [ ( "a", "3" ) ]) >> Expect.equal (decimal "6")
                                , Parser.Stat.eval (getVar [ ( "a", "3" ) ]) >> Expect.equal (Ok <| decimal "6")
                                ]
                        ]
        , test "parse var - val" <|
            \_ ->
                Parser.Stat.parse "a - 3"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ Parser.Stat.toString >> Expect.equal "(a - 3)"
                                , evalWithZero (getVar []) >> Expect.equal (decimal "-3")
                                , Parser.Stat.eval (getVar []) >> Expect.err
                                , evalWithZero (getVar [ ( "a", "-3" ) ]) >> Expect.equal (decimal "-6")
                                , Parser.Stat.eval (getVar [ ( "a", "-3" ) ]) >> Expect.equal (Ok <| decimal "-6")
                                ]
                        ]
        , test "parse many subtractions (non-commutative)" <|
            \_ ->
                Parser.Stat.parse "a - 1 - 2 - 3 - 4"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ Parser.Stat.toString >> Expect.equal "((((a - 1) - 2) - 3) - 4)"
                                , evalWithZero (getVar []) >> Expect.equal (decimal "-10")
                                , Parser.Stat.eval (getVar []) >> Expect.err
                                , evalWithZero (getVar [ ( "a", "-5" ) ]) >> Expect.equal (decimal "-15")
                                , Parser.Stat.eval (getVar [ ( "a", "-5" ) ]) >> Expect.equal (Ok <| decimal "-15")
                                ]
                        ]
        , test "parse precedence" <|
            \_ ->
                Parser.Stat.parse "1 + 2 * 3"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> Parser.Stat.toString >> Expect.equal "(1 + (2 * 3))"
                        , TestUtil.unsafeOk >> evalWithZero (getVar []) >> Expect.equal (decimal "7")
                        ]
        , test "parse precedence 2" <|
            \_ ->
                Parser.Stat.parse "7 - 4 / 2"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> Parser.Stat.toString >> Expect.equal "(7 - (4 / 2))"
                        , TestUtil.unsafeOk >> evalWithZero (getVar []) >> Expect.equal (decimal "5")
                        ]
        , test "parse precedence 3" <|
            \_ ->
                Parser.Stat.parse "7 - 4 / 2 / 2"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> Parser.Stat.toString >> Expect.equal "(7 - ((4 / 2) / 2))"
                        , TestUtil.unsafeOk >> evalWithZero (getVar []) >> Expect.equal (decimal "6")
                        ]
        , test "parse precedence 4" <|
            \_ ->
                Parser.Stat.parse "2 ^ 3 - 4 * 5"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> Parser.Stat.toString >> Expect.equal "((2 ^ 3) - (4 * 5))"
                        , TestUtil.unsafeOk >> evalWithZero (getVar []) >> Expect.equal (decimal "-12")
                        ]
        , test "parse precedence 5" <|
            \_ ->
                Parser.Stat.parse "2 ^ (9-6) - 4 * ((((((5))))))"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> Parser.Stat.toString >> Expect.equal "((2 ^ (9 - 6)) - (4 * 5))"
                        , TestUtil.unsafeOk >> evalWithZero (getVar []) >> Expect.equal (decimal "-12")
                        ]
        , test "parse precedence 6" <|
            \_ ->
                Parser.Stat.parse "(2) ^ (3) - ((4)) * (5)"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk >> Parser.Stat.toString >> Expect.equal "((2 ^ 3) - (4 * 5))"
                        , TestUtil.unsafeOk >> evalWithZero (getVar []) >> Expect.equal (decimal "-12")
                        ]
        , test "parse variables" <|
            \_ ->
                Parser.Stat.parse "a ^ (b-c) - d * ((((((eeeee+0))))))"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ Parser.Stat.toString >> Expect.equal "((a ^ (b - c)) - (d * (eeeee + 0)))"
                                , evalWithZero (getVar []) >> Expect.equal (decimal "1")
                                , Parser.Stat.eval (getVar []) >> Expect.err
                                , evalWithZero (getVar [ ( "a", "2" ), ( "b", "9" ), ( "c", "6" ), ( "d", "4" ), ( "eeeee", "5" ) ]) >> Expect.equal (decimal "-12")
                                , Parser.Stat.eval (getVar [ ( "a", "2" ), ( "b", "9" ), ( "c", "6" ), ( "d", "4" ), ( "eeeee", "5" ) ]) >> Expect.equal (Ok <| decimal "-12")
                                ]
                        ]
        , test "version 2: exports" <|
            \_ ->
                Parser.Stat.parse "let a=2;b = bb: a + 1   ; in c: a * b"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ Parser.Stat.toString >> Expect.equal "let a = 2;\n    b = (bb: (a + 1));\nin\n(c: (a * b))"
                                , evalWithZero (getVar []) >> Expect.equal (decimal "6")
                                , Parser.Stat.eval (getVar []) >> Expect.equal (Ok <| decimal "6")
                                , Parser.Stat.evalFullWithDefault Decimal.zero (getVar []) >> Expect.equal { exports = vars [ ( "bb", "3" ), ( "c", "6" ) ], out = decimal "6" }
                                , Parser.Stat.evalFull (getVar []) >> Expect.equal (Ok <| { exports = vars [ ( "bb", "3" ), ( "c", "6" ) ], out = decimal "6" })
                                ]
                        ]
        , test "version 3: builtin functions" <|
            \_ ->
                Parser.Stat.parse "min(a, 3)"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ Parser.Stat.toString >> Expect.equal "min(a, 3)"
                                , evalWithZero (getVar []) >> Expect.equal (decimal "0")
                                , evalWithZero (getVar [ ( "a", "6" ) ]) >> Expect.equal (decimal "3")
                                , Parser.Stat.eval (getVar [ ( "a", "6" ) ]) >> Expect.equal (Ok <| decimal "3")
                                , Parser.Stat.eval (getVar [ ( "a", "0" ) ]) >> Expect.equal (Ok <| decimal "0")
                                ]
                        ]
        , test "builtin log(val, base) function" <|
            \_ ->
                Parser.Stat.parse "log(100, 10)"
                    |> Expect.all
                        [ Expect.ok
                        , TestUtil.unsafeOk
                            >> Expect.all
                                [ Parser.Stat.toString >> Expect.equal "log(100, 10)"
                                , evalWithZero (getVar []) >> Expect.equal (decimal "2")
                                , Parser.Stat.eval (getVar []) >> Expect.equal (Ok <| decimal "2")
                                ]
                        ]
        , test "builtin log function fails for bad args" <|
            \_ -> Parser.Stat.parse "log(100, 10, 10)" |> Expect.err
        , describe "fromjson"
            (let
                parser : D.Decoder (Maybe Parser.Stat.Expr)
                parser =
                    -- The semantics in the tests below are what we want: script is either absent or optional.
                    -- I couldn't get them to pass without either decode.pipeline or a huge custom function, though!
                    --
                    -- D.field "a" Parser.Stat.fromJson |> D.maybe
                    D.succeed identity
                        |> P.optional "a" (Parser.Stat.fromJson |> D.map Just) Nothing
             in
             [ test "missing field" <| \_ -> D.decodeString parser "{}" |> Expect.equal (Ok Nothing)
             , test "empty string" <| \_ -> D.decodeString parser "{\"a\": \"\"}" |> Expect.err
             , test "invalid string" <| \_ -> D.decodeString parser "{\"a\": \"-\"}" |> Expect.err
             , test "valid string" <| \_ -> D.decodeString parser "{\"a\": \"3\"}" |> Expect.equal (Ok <| Just <| unsafeParse "3")
             ]
            )
        , test "evaluate big numbers: add" <|
            \_ ->
                Parser.Stat.parse "a + b"
                    |> TestUtil.unsafeOk
                    |> Parser.Stat.eval (getVar [ ( "a", "1e400" ), ( "b", "2e400" ) ])
                    |> Expect.equal (Ok <| decimal "3e400")
        , test "evaluate big numbers: mul" <|
            \_ ->
                Parser.Stat.parse "a * b"
                    |> TestUtil.unsafeOk
                    |> Parser.Stat.eval (getVar [ ( "a", "1e200" ), ( "b", "2e300" ) ])
                    |> Expect.equal (Ok <| decimal "2e500")
        , test "evaluate big numbers: pow" <|
            \_ ->
                Parser.Stat.parse "2 ^ a"
                    |> TestUtil.unsafeOk
                    |> Parser.Stat.eval (getVar [ ( "a", "4000" ) ])
                    |> Expect.equal (Ok <| decimal "1.3182040934310915e1204")
        ]


unsafeParse : String -> Parser.Stat.Expr
unsafeParse str =
    case Parser.Stat.parse str of
        Err err ->
            Debug.todo "unsafeParse failed" err

        Ok expr ->
            expr


decimal : String -> Decimal
decimal =
    Decimal.fromString >> TestUtil.unsafeJust "decimal"


evalWithZero =
    Parser.Stat.evalWithDefault Decimal.zero


evalWithOne =
    Parser.Stat.evalWithDefault Decimal.one


vars : List ( String, String ) -> Dict String Decimal
vars =
    List.map (Tuple.mapSecond (Decimal.fromString >> TestUtil.unsafeJust "not a Decimal")) >> Dict.fromList


getVar : List ( String, String ) -> String -> Maybe Decimal
getVar list =
    let
        dict =
            vars list
    in
    \name -> Dict.get name dict
