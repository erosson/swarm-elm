module FrameRate exposing (FrameRate, empty, maximum, push, toAverage, toFPS)

import Duration exposing (Duration)
import Time exposing (Posix)


type FrameRate
    = Empty
    | Nonempty Posix (List Duration)


empty : FrameRate
empty =
    Empty


window : Int
window =
    200


push : Posix -> FrameRate -> FrameRate
push now fr =
    case fr of
        Empty ->
            Nonempty now []

        Nonempty prev frames ->
            Nonempty now (List.take window <| Duration.since { before = prev, after = now } :: frames)


toAverage : FrameRate -> Duration
toAverage fr =
    case fr of
        Empty ->
            Duration.zero

        Nonempty _ frames ->
            Duration.mean frames


maximum : FrameRate -> Duration
maximum fr =
    List.foldl Duration.max Duration.zero <|
        case fr of
            Empty ->
                []

            Nonempty _ frames ->
                frames


toFPS : FrameRate -> Int
toFPS fr =
    1000 // Duration.toMillis (toAverage fr)
