module Polynomial exposing
    ( Polynomial
    , evaluate, evaluateTerms
    , toString, format, formatCoefficients, formatTerms
    )

{-| Polynomial equations over time.

For example, `[7, 5, 3]` represents `f(t) = 3t^2 + 5t + 7`


# Definition

@docs Polynomial


# Evaluation

@docs evaluate, evaluateTerms


# Printing and Formatting

@docs toString, format, formatCoefficients, formatTerms

-}

import Decimal exposing (Decimal)
import Duration exposing (Duration)


type alias Polynomial =
    List Decimal


{-| The result of evaluating a polynomial. Next step is to sum them.
-}
type alias PolyTerms =
    List Decimal


evaluate : Duration -> Polynomial -> Decimal
evaluate t =
    evaluateTerms t >> List.foldl Decimal.add Decimal.zero


evaluateTerms : Duration -> Polynomial -> PolyTerms
evaluateTerms dur =
    let
        t =
            Duration.toSecs dur

        eval1 degree term =
            -- term * t ^ degree
            term |> Decimal.mul (t ^ toFloat degree |> Decimal.fromFloat)
    in
    List.indexedMap eval1


{-| for example, "12345 + 321 + 1"
-}
formatTerms : (Decimal -> String) -> PolyTerms -> String
formatTerms numFormat =
    List.map numFormat >> List.reverse >> String.join " + "


{-| just the coefficients for `format`. Useful for fancier rendering.
-}
formatCoefficients : (Decimal -> String) -> Polynomial -> List String
formatCoefficients numFormat poly =
    let
        format1 degree term =
            if term == Decimal.one && degree /= 0 then
                ""

            else
                term |> numFormat
    in
    List.indexedMap format1
        (if poly == [] then
            -- without this special case, an empty polynomial would output blank. "0" is nicer.
            [ Decimal.zero ]

         else
            poly
        )


{-| for example, "3t^2 + 2t + 1"
-}
format : (Decimal -> String) -> Polynomial -> String
format numFormat =
    let
        degreeToString : Int -> String -> String
        degreeToString degree coeff =
            let
                pow =
                    case degree of
                        0 ->
                            ""

                        1 ->
                            " t"

                        _ ->
                            -- " t^" ++ (degree |> toFloat |> numFormat)
                            " t^" ++ String.fromInt degree
            in
            coeff ++ pow
    in
    formatCoefficients numFormat
        >> List.indexedMap degreeToString
        >> List.reverse
        >> String.join " + "


toString : Polynomial -> String
toString =
    format Decimal.toString
