module Session exposing
    ( Session, create
    , gameData, nav, loaded, now, safeNow, timezone, game, frameRate, query, persistExport, serviceWorker, error, errors, user, userRole, version, environment, iframe, undoDuration, welcomeBack, hostname
    , initTime, setNow, setQuery, resetGame, setGame, mapGame, setPersistRead, setPersistExport, setServiceWorker, pushError, dropError, pushWarning, setUser, clearUserError, undo, clearWelcomeBack
    , UserState, changelog, clearOfflineSyncConflict, clearUnits, evalUnits, grantAchievement, keyCombosHelp, logout, mapIframe, mapIframeCmd, mapUserSync, offlineSync, offlineSyncConflict, orderProgress, paypalEnv, persistWrite, safeUnits, setKeyCombosHelp, setRawUser, setTranslator, toggleKeyCombosHelp, translatorEntry, translatorJson, undoState, units, userState, userSyncCooldownDuration, userSyncRequest, userSyncResponse
    )

{-| Global application state, shared among all pages.


# Create

@docs Session, create


# Read

@docs gameData, nav, loaded, now, safeNow, timezone, game, frameRate, query, persistExport, serviceWorker, error, errors, user, userRole, version, environment, iframe, undoDuration, welcomeBack, hostname


# Update

@docs initTime, setNow, setQuery, resetGame, setGame, mapGame, setPersistRead, setPersistExport, setServiceWorker, pushError, dropError, pushWarning, setUser, clearUserError, undo, clearWelcomeBack

-}

import Browser.Navigation as Nav
import Cooldown exposing (Cooldown)
import Dict exposing (Dict)
import Duration exposing (Duration)
import FrameRate exposing (FrameRate)
import Game exposing (Game)
import Game.OfflineSync as OfflineSync
import Game.Order as Order exposing (Order)
import Game.Order.Progress as Progress exposing (OrderProgress, Progress)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Lang
import Json.Decode as D
import Kongregate.Stats
import Maybe.Extra
import PlayFab.Error
import PlayFab.User as User exposing (User)
import Ports
import Ports.Kongregate
import RemoteData exposing (RemoteData)
import Route.Config as Config
import Route.Feature as Feature
import Route.QueryDict as QueryDict
import Session.Environment as Environment exposing (Environment)
import Session.GameLoader as GameLoader exposing (GameLoader)
import Session.Iframe as Iframe exposing (Iframe)
import Session.PaypalEnv as PaypalEnv exposing (PaypalEnv)
import Session.ServiceWorker as ServiceWorker
import Time exposing (Posix)


type Session
    = Session SessionData


type alias SessionData =
    { version : String
    , hostname : String
    , changelog : String
    , nav : Nav.Key
    , errors : List ( String, Maybe Posix )
    , loaded : Maybe Posix
    , now : Maybe Posix
    , timezone : Maybe Time.Zone
    , persistExport : Maybe String
    , game : GameLoader
    , query : Dict String String
    , frameRate : Maybe FrameRate
    , serviceWorker : Maybe ServiceWorker.Event
    , environment : Environment
    , iframe : Iframe
    , welcomeBack : Maybe { before : UnitsSnapshot, after : UnitsSnapshot }
    , keyCombosHelp : Bool

    -- `RemoteData`: user info's fetched from PlayFab servers.
    --
    -- `Maybe`: the user might not be logged in at all. Compare `Success Nothing`
    -- to `NotAsked` - `NotAsked` means we don't know one way or the other;
    -- `Success Nothing` means we're certain they're not logged in.
    , user : RemoteData PlayFab.Error.Error (Maybe UserState)
    , unitsCache : Maybe UnitsSnapshot
    , orderProgressCache : Dict String OrderProgress
    , translator : Maybe GameData.Lang.Translator
    }


type alias UserState =
    { user : User
    , sync : Result OfflineSync.ConflictData UserSyncState
    }


type alias UserSyncState =
    { request : RemoteData String ()
    , requested : Maybe Posix
    , succeeded : Maybe Posix
    , cooldown : Cooldown
    }


create :
    { f | environment : String, hostname : String, version : String, changelog : String }
    -> Nav.Key
    -> Result String GameData
    -> Maybe GameData.Lang.Translator
    -> Session
create args nav_ gameData_ translator_ =
    Session
        { version = args.version
        , changelog = args.changelog
        , hostname = args.hostname
        , environment = Environment.fromString args.environment
        , nav = nav_
        , errors = []
        , loaded = Nothing
        , now = Nothing
        , timezone = Nothing
        , persistExport = Nothing
        , game = GameLoader.empty gameData_
        , query = Dict.empty
        , frameRate = Nothing
        , serviceWorker = Nothing
        , user = RemoteData.NotAsked
        , welcomeBack = Nothing
        , iframe = Iframe.WWW
        , keyCombosHelp = False
        , unitsCache = Nothing
        , orderProgressCache = Dict.empty
        , translator = translator_
        }



-- READ


unwrap : Session -> SessionData
unwrap (Session s) =
    s


nav : Session -> Nav.Key
nav =
    unwrap >> .nav


gameData : Session -> Result String GameData
gameData =
    unwrap >> .game >> GameLoader.gameData


loaded : Session -> Maybe Posix
loaded =
    unwrap >> .loaded


now : Session -> Maybe Posix
now =
    unwrap >> .now


safeNow : Game -> Session -> Posix
safeNow g =
    -- If we have a `Game`, I promise we also have `now`
    now >> Maybe.withDefault (Game.snapshotted g)


timezone : Session -> Maybe Time.Zone
timezone =
    unwrap >> .timezone


game : Session -> RemoteData String Game
game =
    unwrap >> .game >> GameLoader.toRemote


persistExport : Session -> Maybe String
persistExport =
    unwrap >> .persistExport


query : Session -> Dict String String
query =
    unwrap >> .query


setQuery : Dict String String -> Session -> Session
setQuery q (Session s) =
    Session <|
        if s.query == q then
            s

        else
            { s
                | query = q
                , frameRate =
                    if Feature.isActive Feature.Fps q then
                        s.frameRate |> Maybe.withDefault FrameRate.empty |> Just

                    else
                        Nothing
            }


frameRate : Session -> Maybe FrameRate
frameRate =
    unwrap >> .frameRate


serviceWorker : Session -> Maybe ServiceWorker.Event
serviceWorker =
    unwrap >> .serviceWorker


userState : Session -> RemoteData PlayFab.Error.Error (Maybe UserState)
userState =
    unwrap >> .user


user : Session -> RemoteData PlayFab.Error.Error (Maybe User)
user =
    userState >> RemoteData.map (Maybe.map .user)


userRole : Session -> RemoteData PlayFab.Error.Error User.Role
userRole =
    user >> RemoteData.map (Maybe.map User.role >> Maybe.withDefault User.defaultRole)


version : Session -> String
version =
    unwrap >> .version


changelog : Session -> String
changelog =
    unwrap >> .changelog


keyCombosHelp : Session -> Bool
keyCombosHelp =
    unwrap >> .keyCombosHelp


{-| `process.node.NODE_ENV`. Usually `development` or `production`. Override with `?environment=...`

Try not to use this often; dev and prod differences are a pain to test.

Definitely don't use it to secure devtools (like we did in the old coffeescript-
swarmsim), because it's easily overridden.

-}
environment : Session -> Environment
environment session =
    case query session |> Config.get Config.Environment of
        Nothing ->
            (unwrap session).environment

        Just env ->
            Environment.fromString env


hostname : Session -> String
hostname session =
    query session |> Config.get Config.Hostname |> Maybe.withDefault (unwrap session).hostname


paypalEnv : Session -> PaypalEnv
paypalEnv session =
    PaypalEnv.fromQuery (environment session) (iframe session) (hostname session) (query session)


{-| Browser environment/iframe wrapper, if any.
-}
iframe : Session -> Iframe
iframe =
    unwrap >> .iframe


mapIframe : (Iframe -> Iframe) -> Session -> Session
mapIframe map (Session s) =
    Session { s | iframe = map s.iframe }


mapIframeCmd : (Iframe -> ( Iframe, Cmd msg )) -> Session -> ( Session, Cmd msg )
mapIframeCmd map (Session s) =
    let
        ( iframe_, cmd ) =
            map s.iframe
    in
    ( Session { s | iframe = iframe_ }, cmd )


undoDuration : Session -> Maybe Duration
undoDuration (Session s) =
    Maybe.andThen (\now_ -> GameLoader.undoDuration now_ s.game) s.now


undo : Session -> ( Session, Cmd msg )
undo ((Session s) as session0) =
    case s.now of
        Just now_ ->
            s.game
                |> GameLoader.undo now_
                |> Tuple.mapFirst (\g -> Session { s | game = g })

        Nothing ->
            ( session0, Cmd.none )


undoState : Session -> Maybe Game
undoState =
    unwrap >> .game >> GameLoader.undoState >> Maybe.map Tuple.second


errors : Session -> List ( String, Maybe Posix )
errors =
    unwrap >> .errors


error : Session -> Maybe ( String, Maybe Posix )
error =
    errors >> List.head



-- UPDATE


initTime : Posix -> Time.Zone -> Session -> Session
initTime loaded_ timezone_ (Session session) =
    Session
        { session
            | now = Just loaded_
            , loaded = Maybe.Extra.or session.loaded (Just loaded_)
            , timezone = Maybe.Extra.or session.timezone (Just timezone_)
        }


setNow : Posix -> { evalUnits : Maybe (List Order) } -> Session -> Session
setNow now_ p (Session session) =
    Session
        { session
            | now = Just now_
            , frameRate = session.frameRate |> Maybe.map (FrameRate.push now_)
            , game = session.game |> GameLoader.map (Game.expireNewAchievements now_)

            -- We chould show welcome-back here if it's been a while since their last action.
            -- Currently choosing not to ever do that.
            --, welcomeBack =
            --case ( GameLoader.toRemote session.game, session.now ) of
            --    ( RemoteData.Success g, Just now0 ) ->
            --        tryWelcomeBack { after = now_, before = now0 } g
        }
        |> (case p.evalUnits of
                Just os ->
                    evalUnits os

                Nothing ->
                    clearUnits
           )


evalUnits : List Order -> Session -> Session
evalUnits orders ((Session s) as session) =
    case ( s.now, GameLoader.toRemote s.game ) of
        ( Just t, RemoteData.Success game_ ) ->
            let
                units_ =
                    Game.evalUnits t game_
            in
            Session
                { s
                    | unitsCache = Just units_
                    , orderProgressCache = Progress.updates game_ units_ orders s.orderProgressCache
                }

        _ ->
            clearUnits session


clearUnits : Session -> Session
clearUnits ((Session s) as session) =
    Session { s | unitsCache = Nothing, orderProgressCache = Dict.empty }


units : Session -> Maybe UnitsSnapshot
units =
    unwrap >> .unitsCache


orderProgress : String -> Session -> Maybe OrderProgress
orderProgress name =
    unwrap >> .orderProgressCache >> Dict.get name


safeUnits : Game -> Session -> UnitsSnapshot
safeUnits g session =
    case units session of
        Just u ->
            u

        Nothing ->
            Game.evalUnits (safeNow g session) g


welcomeBackCooldown : Duration
welcomeBackCooldown =
    Duration.fromSecs <| 5 * 60


isWelcomeBackCooledDown : Duration -> Bool
isWelcomeBackCooledDown dur =
    Duration.toMillis dur >= Duration.toMillis welcomeBackCooldown


welcomeBack : Session -> Maybe { before : UnitsSnapshot, after : UnitsSnapshot }
welcomeBack =
    unwrap >> .welcomeBack


clearWelcomeBack : Session -> Session
clearWelcomeBack (Session s) =
    Session { s | welcomeBack = Nothing }


setPersistRead : Result String (Maybe D.Value) -> Session -> Session
setPersistRead r ((Session s) as session) =
    let
        game_ =
            s.game
                |> GameLoader.setJson (RemoteData.fromResult r)
                |> GameLoader.tryReady s.now
    in
    session
        |> (case GameLoader.toRemote game_ of
                RemoteData.Success newGame ->
                    tryWelcomeBackFromGame newGame

                _ ->
                    identity
           )
        |> (\(Session ss) -> Session { ss | game = game_ })


setPersistExport : Maybe String -> Session -> Session
setPersistExport exp (Session session) =
    Session { session | persistExport = exp }


setGame : Bool -> Game -> Session -> Session
setGame undoable game_ session0 =
    tryWelcomeBackFromGame game_ session0
        |> (\((Session s) as session) ->
                if undoable then
                    Session { s | game = s.game |> GameLoader.undoableSet (safeNow game_ session) game_ }

                else
                    Session { s | game = s.game |> GameLoader.set game_ }
           )
        -- if game was reified, clear cached order progressbars
        |> (if (game session0 |> RemoteData.toMaybe |> Maybe.map (Game.units >> UnitsSnapshot.at)) == (game_ |> Game.units |> UnitsSnapshot.at |> Just) then
                identity

            else
                clearUnits
           )
        |> evalUnits []


tryWelcomeBackFromGame : Game -> Session -> Session
tryWelcomeBackFromGame newGame ((Session s) as session) =
    Session
        { s
            | welcomeBack =
                case game session of
                    RemoteData.Success oldGame ->
                        if Game.created newGame == Game.created oldGame then
                            -- we modified an already-loaded game. Clear welcome-back,
                            -- even if it's already open.
                            Nothing

                        else
                            -- we loaded a new game. Has it been a while since it was saved?
                            tryWelcomeBack { after = safeNow newGame session, before = Game.encoded newGame } newGame

                    _ ->
                        -- we loaded a new game, our first since this page load.
                        tryWelcomeBack { after = safeNow newGame session, before = Game.encoded newGame } newGame
        }


tryWelcomeBack : { after : Posix, before : Posix } -> Game -> Maybe { after : UnitsSnapshot, before : UnitsSnapshot }
tryWelcomeBack t g =
    if isWelcomeBackCooledDown (Duration.since t) then
        Just { before = Game.evalUnits t.before g, after = Game.evalUnits t.after g }

    else
        Nothing


mapGame : Bool -> (Game -> Game) -> Session -> Session
mapGame undoable fn session =
    case game session of
        RemoteData.Success g ->
            setGame undoable (fn g) session

        _ ->
            session


resetGame : Session -> Result String Session
resetGame ((Session session) as session0) =
    Result.map2 (\gd t -> Session { session | game = session.game |> GameLoader.set (Game.empty gd t) })
        (gameData session0)
        (session.now |> Result.fromMaybe "time not set")


grantAchievement : String -> Session -> ( Session, Cmd msg )
grantAchievement name session =
    case game session of
        RemoteData.Success g0 ->
            case Game.grantAchievement name (safeNow g0 session) g0 |> Maybe.map (persistWrite session) of
                Just ( g1, cmd1 ) ->
                    ( setGame True g1 session, cmd1 )

                Nothing ->
                    ( pushError ("no such achievement: " ++ name) session, Cmd.none )

        _ ->
            ( session, Cmd.none )


setServiceWorker : ServiceWorker.Event -> Session -> Session
setServiceWorker event (Session session) =
    Session { session | serviceWorker = Just event }


setUser : RemoteData PlayFab.Error.Error (Maybe User) -> Session -> Session
setUser userRes (Session session) =
    let
        conflictState : RemoteData String OfflineSync.ConflictState
        conflictState =
            OfflineSync.create (userRes |> RemoteData.mapError PlayFab.Error.toString) session.game session.persistExport

        userState_ : RemoteData PlayFab.Error.Error (Maybe UserState)
        userState_ =
            RemoteData.map (Maybe.map (createUserState conflictState)) userRes

        remote : Maybe { game : Game, encoded : String }
        remote =
            conflictState |> RemoteData.toMaybe |> Maybe.andThen OfflineSync.toLocalOverwrite
    in
    Session
        { session
            | user = userState_
            , persistExport =
                remote |> Maybe.Extra.unwrap session.persistExport (.encoded >> Just)
            , game =
                remote |> Maybe.Extra.unwrap session.game (.game >> GameLoader.fromGame)
        }


setRawUser : User -> Session -> Session
setRawUser user_ (Session session) =
    Session { session | user = session.user |> RemoteData.map (Maybe.map (\u -> { u | user = user_ })) }


clearUserError : Session -> Session
clearUserError session =
    case user session of
        RemoteData.Failure err ->
            session |> setUser (RemoteData.Success Nothing)

        _ ->
            session


createUserState : RemoteData String OfflineSync.ConflictState -> User -> UserState
createUserState conflictState user_ =
    { user = user_
    , sync =
        case conflictState |> RemoteData.map OfflineSync.toConflict of
            RemoteData.Success (Just conflict) ->
                Err conflict

            _ ->
                Ok emptyUserSyncState
    }


emptyUserSyncState : UserSyncState
emptyUserSyncState =
    { request = RemoteData.NotAsked
    , requested = Nothing
    , succeeded = Nothing
    , cooldown = Cooldown.Ready
    }


mapUserSync : (UserSyncState -> UserSyncState) -> Session -> Session
mapUserSync mapSync session =
    filterMapUserSync (mapSync >> Just) session |> Maybe.withDefault session


filterMapUserSync : (UserSyncState -> Maybe UserSyncState) -> Session -> Maybe Session
filterMapUserSync mapSync ((Session s) as session) =
    case s.user of
        RemoteData.Success (Just ustate0) ->
            case ustate0.sync |> Result.map mapSync of
                Ok (Just sync) ->
                    Just <| Session { s | user = RemoteData.Success (Just { ustate0 | sync = Ok sync }) }

                _ ->
                    Nothing

        _ ->
            Nothing


userSyncCooldownDuration =
    Duration.fromSecs 120


userSyncRequest : { ignoreCooldown : Bool } -> Session -> Maybe Session
userSyncRequest { ignoreCooldown } session =
    case now session of
        Nothing ->
            Nothing

        Just now_ ->
            let
                req sync cooldown =
                    { sync
                        | request = RemoteData.Loading
                        , requested = Just now_
                        , cooldown = cooldown
                    }
            in
            session
                |> filterMapUserSync
                    (\sync ->
                        if ignoreCooldown then
                            Just <| req sync (Cooldown.Until <| Duration.addPosix userSyncCooldownDuration now_)

                        else
                            Cooldown.trigger now_ userSyncCooldownDuration sync.cooldown
                                |> Maybe.map (req sync)
                    )


userSyncResponse : Maybe Posix -> Result PlayFab.Error.Error () -> Session -> ( Session, Cmd msg )
userSyncResponse now_ res session =
    case res of
        Ok () ->
            ( session |> mapUserSync (\sync -> { sync | request = RemoteData.Success (), succeeded = now_ }), Cmd.none )

        Err ((PlayFab.Error.AuthError _ _) as err) ->
            -- Calling playfab failed because the user's session expired!
            -- The user must log in again.
            -- TODO try remember-me here
            ( session
                |> mapUserSync (\sync -> { sync | request = RemoteData.Failure <| PlayFab.Error.toString err })
                |> setUser (RemoteData.Failure err)
            , Ports.writeUserSessionTicket Nothing
            )

        Err err ->
            ( session |> mapUserSync (\sync -> { sync | request = RemoteData.Failure <| PlayFab.Error.toString err }), Cmd.none )


logout : Session -> ( Session, Cmd msg )
logout session =
    ( session |> setUser (RemoteData.Success Nothing)
    , Cmd.batch
        [ Ports.writeUserSessionTicket Nothing
        , Ports.writeUserRememberId Nothing
        ]
    )


offlineSync : Session -> RemoteData PlayFab.Error.Error (Maybe (Result OfflineSync.ConflictData UserSyncState))
offlineSync =
    unwrap >> .user >> RemoteData.map (Maybe.map .sync)


offlineSyncConflict : Session -> Maybe OfflineSync.ConflictData
offlineSyncConflict session =
    case offlineSync session of
        RemoteData.Success (Just (Err conflict)) ->
            Just conflict

        _ ->
            Nothing


clearOfflineSyncConflict : Session -> Session
clearOfflineSyncConflict (Session s) =
    Session { s | user = s.user |> RemoteData.map (Maybe.map (\u -> { u | sync = Ok emptyUserSyncState })) }


pushError : String -> Session -> Session
pushError message (Session session) =
    Session { session | errors = ( message, session.now ) :: session.errors }


{-| Problems that aren't important enough to interrupt the user for.

The developer ought to know about these because they really shouldn't happen,
but they're not the user's problem.

It's a no-op for now, but later maybe we'll add some sort of error reporting.

-}
pushWarning : String -> Session -> Session
pushWarning _ session =
    session


dropError : Session -> Session
dropError (Session session) =
    Session { session | errors = session.errors |> List.drop 1 }


persistWrite : Session -> Game -> ( Game, Cmd msg )
persistWrite session game0 =
    let
        ( game1, cmd ) =
            Game.persistWrite (safeNow game0 session) game0
    in
    ( game1
    , Cmd.batch
        [ cmd
        , Kongregate.Stats.collect game1 (Game.units game1)
            |> Kongregate.Stats.encoder
            |> Ports.Kongregate.kongregateSubmitStats
        ]
    )


setKeyCombosHelp : Bool -> Session -> Session
setKeyCombosHelp val (Session s) =
    Session { s | keyCombosHelp = val }


toggleKeyCombosHelp : Session -> Session
toggleKeyCombosHelp s =
    setKeyCombosHelp (not <| keyCombosHelp s) s


translatorEntry : Session -> Maybe GameData.Lang.Entry
translatorEntry s =
    if Feature.isActive Feature.Translator <| query s then
        s |> unwrap |> .translator |> Maybe.map Tuple.first

    else
        Nothing


translatorJson : Session -> Maybe D.Value
translatorJson =
    unwrap >> .translator >> Maybe.map Tuple.second


setTranslator : Maybe GameData.Lang.Translator -> Session -> Session
setTranslator t (Session s) =
    Session { s | translator = t }
