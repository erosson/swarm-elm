module GameData.Lang exposing (Entry, LangSet, Translator, decoder, translatorDecoder)

import Dict exposing (Dict)
import GameData.Util
import Json.Decode as D
import JsonUtil
import Locale exposing (Locale)
import MessageFormat exposing (Message)


type alias LangSet =
    -- Translation tools use the raw unformatted data
    { raw_en_us : List ( String, List ( String, String ) )
    , en_us : Entry
    }


type alias Entry =
    Dict String Message


type alias Translator =
    ( Entry, D.Value )


decoder : D.Decoder LangSet
decoder =
    D.map2 LangSet
        (D.field "en_us" rawDecoder)
        (D.field "en_us" entryDecoder)


translatorDecoder : D.Decoder Translator
translatorDecoder =
    D.map2 Tuple.pair entryDecoder D.value


rawDecoder : D.Decoder (List ( String, List ( String, String ) ))
rawDecoder =
    D.keyValuePairs (D.keyValuePairs D.string)


entryDecoder : D.Decoder Entry
entryDecoder =
    D.keyValuePairs (D.field "message" messageDecoder) |> D.map Dict.fromList


messageDecoder : D.Decoder Message
messageDecoder =
    D.string |> D.andThen (MessageFormat.parse >> JsonUtil.decodeResult)
