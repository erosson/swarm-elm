module GameData.Achievement exposing (Achievement, Id, Trigger(..), decoder, idToString, name, triggerExpression)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import GameData.Achievement.Id as Id
import GameData.Item as Item exposing (Item)
import GameData.Util
import Json.Decode as D
import Maybe.Extra
import Parser.Visibility
import Time exposing (Posix)


type alias Id =
    Id.Id


idToString : Id -> String
idToString =
    Id.toString


type alias Achievement =
    { id : Id
    , points : Int
    , visible : Parser.Visibility.Expr
    , trigger : Trigger
    }


name : Achievement -> String
name =
    .id >> idToString


type Trigger
    = Expr Parser.Visibility.Expr
    | Event String


triggerExpression : Achievement -> Maybe Parser.Visibility.Expr
triggerExpression achievement =
    case achievement.trigger of
        Expr e ->
            Just e

        _ ->
            Nothing


decoder : D.Decoder (List Achievement)
decoder =
    D.map2 (\a b -> [ a, b ] |> List.concat)
        (D.field "achievements"
            (D.field "name" Id.decoder
                |> D.andThen
                    (\id ->
                        D.map3 (Achievement id)
                            (D.field "points" D.int)
                            (D.field "visible" Parser.Visibility.fromJson)
                            (D.field "trigger" <| D.map Expr Parser.Visibility.fromJson)
                    )
                |> D.list
            )
        )
        (D.field "achievements.events"
            (D.field "name" Id.decoder
                |> D.andThen
                    (\id ->
                        D.map3 (Achievement id)
                            (D.field "points" D.int)
                            (D.field "visible" Parser.Visibility.fromJson)
                            (D.field "event" <| D.map Event D.string)
                    )
                |> D.list
            )
        )
