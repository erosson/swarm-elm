module GameData.OnBuy exposing (AddRandomUnitsData, Cooldown, Index, OnBuy(..), decoder, indexFromList)

import Dict exposing (Dict)
import Dict.Extra
import Duration exposing (Duration)
import GameData.Item.Id as ItemId
import GameData.Unit.Id as UnitId
import Json.Decode as D
import Parser.Stat
import Set exposing (Set)
import Time exposing (Posix)


type alias UnitId =
    UnitId.Id


type OnBuy
    = AddUnits UnitId Parser.Stat.Expr
    | MulUnits UnitId Parser.Stat.Expr
    | AddTime Parser.Stat.Expr
    | AddRandomUnits AddRandomUnitsData
    | AddUnitsCooldown UnitId Parser.Stat.Expr Cooldown
    | Respec UnitId Float (Set String)
    | Ascend


type alias AddRandomUnitsData =
    { child : UnitId.Id
    , minimum : Int
    , chance : Parser.Stat.Expr
    , count : Parser.Stat.Expr
    }


type alias Cooldown =
    { group : String, val : Duration }



-- INDEXING BY TYPE


type alias Index =
    { all : List OnBuy
    , addUnits : List ( UnitId, Parser.Stat.Expr )
    , mulUnits : List ( UnitId, Parser.Stat.Expr )
    , addTime : List Parser.Stat.Expr
    , addRandomUnits : List AddRandomUnitsData
    , addUnitsCooldown : List ( UnitId, Parser.Stat.Expr, Cooldown )
    , respec : List ( UnitId, Float, Set String )
    , ascend : Bool
    , addUnitsByChild : Dict String Parser.Stat.Expr
    , mulUnitsByChild : Dict String Parser.Stat.Expr
    , addUnitsCooldownByChild : Dict String ( Parser.Stat.Expr, Cooldown )
    }


indexEmpty : Index
indexEmpty =
    Index [] [] [] [] [] [] [] False Dict.empty Dict.empty Dict.empty


indexFromList : List OnBuy -> Index
indexFromList list =
    let
        indexPush : OnBuy -> Index -> Index
        indexPush onBuy index =
            case onBuy of
                AddUnits unitId expr ->
                    { index
                        | addUnits = ( unitId, expr ) :: index.addUnits
                        , addUnitsByChild = Dict.insert (UnitId.toString unitId) expr index.addUnitsByChild
                    }

                MulUnits unitId expr ->
                    { index
                        | mulUnits = ( unitId, expr ) :: index.mulUnits
                        , mulUnitsByChild = Dict.insert (UnitId.toString unitId) expr index.mulUnitsByChild
                    }

                AddTime expr ->
                    { index | addTime = expr :: index.addTime }

                AddRandomUnits data ->
                    { index | addRandomUnits = data :: index.addRandomUnits }

                AddUnitsCooldown unitId expr cooldown ->
                    { index
                        | addUnitsCooldown = ( unitId, expr, cooldown ) :: index.addUnitsCooldown
                        , addUnitsCooldownByChild = Dict.insert (UnitId.toString unitId) ( expr, cooldown ) index.addUnitsCooldownByChild
                    }

                Respec unitId rate resetItems ->
                    { index | respec = ( unitId, rate, resetItems ) :: index.respec }

                Ascend ->
                    { index | ascend = True }
    in
    List.foldr indexPush { indexEmpty | all = list } list



-- SERIALIZATION


addUnits : UnitId -> Parser.Stat.Expr -> Maybe Cooldown -> OnBuy
addUnits child expr cooldown =
    case cooldown of
        Nothing ->
            AddUnits child expr

        Just cd ->
            AddUnitsCooldown child expr cd


decoder : D.Decoder (Dict String (List OnBuy))
decoder =
    D.map6 (\a b c d e f -> List.concat [ a, b, c, d, e, f ])
        (D.field "onBuy.addUnits"
            (D.map2 Tuple.pair
                (D.field "parent" D.string)
                (D.map3 addUnits
                    (D.field "child" UnitId.decoder)
                    (D.field "val" Parser.Stat.fromJson)
                    (D.maybe <|
                        D.map2 Cooldown
                            (D.field "cooldown.group" D.string)
                            (D.field "cooldown.sec" <| D.map Duration.fromSecs <| D.float)
                    )
                )
                |> D.list
            )
        )
        (D.field "onBuy.mulUnits"
            (D.map2 Tuple.pair
                (D.field "parent" D.string)
                (D.map2 MulUnits
                    (D.field "child" UnitId.decoder)
                    (D.field "count" Parser.Stat.fromJson)
                )
                |> D.list
            )
        )
        (D.field "onBuy.addTime"
            (D.map2 Tuple.pair
                (D.field "parent" D.string)
                (D.field "secs" <| D.map AddTime <| Parser.Stat.fromJson)
                |> D.list
            )
        )
        (D.field "onBuy.addRandomUnits"
            (D.map2 Tuple.pair
                (D.field "parent" D.string)
                (D.map4 AddRandomUnitsData
                    (D.field "child" UnitId.decoder)
                    (D.field "minimum" D.int)
                    (D.field "chance" Parser.Stat.fromJson)
                    (D.field "count" Parser.Stat.fromJson)
                    |> D.map AddRandomUnits
                )
                |> D.list
            )
        )
        (D.field "onBuy.ascend"
            (D.map (\p -> ( p, Ascend ))
                (D.field "parent" D.string)
                |> D.list
            )
        )
        (D.field "onBuy.respec"
            (D.map2 Tuple.pair
                (D.field "parent" D.string)
                (D.map3 Respec
                    (D.field "currency" UnitId.decoder)
                    (D.field "rate" D.float)
                    (D.field "resetItems" <| D.map (Set.fromList << String.split ",") <| D.string)
                )
                |> D.list
            )
        )
        |> D.map (Dict.Extra.groupBy Tuple.first >> Dict.map (\_ -> List.map Tuple.second))
