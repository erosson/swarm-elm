module GameData.Cost exposing
    ( Cost, CostVal(..), toExpr
    , decoder
    )

{-| Purchase costs for any buyable object - unit, upgrade, or ability (or more?)

Costs are always paid with `unit`s as currency, but the buyable "thing" has no such restriction.


# Definitions

@docs Cost, CostVal, toExpr


# Serialization

@docs decoder

-}

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Dict.Extra
import GameData.Unit.Id as UnitId
import Json.Decode as D
import Parser.Stat


type CostVal
    = FixedCost Parser.Stat.Expr
    | FactorCost Parser.Stat.Expr Float


type alias Cost =
    ( UnitId.Id, CostVal )


toExpr : CostVal -> Parser.Stat.Expr
toExpr cost =
    case cost of
        FixedCost expr ->
            expr

        FactorCost expr _ ->
            expr



-- serialization


decoder : D.Decoder (Dict String (List Cost))
decoder =
    D.map2 (++)
        (D.field "costs.fixed"
            (D.map3 costEntry
                (D.field "name" D.string)
                (D.field "currency" UnitId.decoder)
                (D.field "val" <| D.map FixedCost <| Parser.Stat.fromJson)
                |> D.list
            )
        )
        (D.field "costs.factor"
            (D.map3 costEntry
                (D.field "name" D.string)
                (D.field "currency" UnitId.decoder)
                (D.map2
                    (\init factor ->
                        if factor == 1 then
                            -- optimization: FactorCost with a factor of 1 is the same as a FixedCost
                            FixedCost init

                        else
                            FactorCost init factor
                    )
                    (D.field "init" Parser.Stat.fromJson)
                    -- Factor is used as a log base, and Decimal only supports float log-bases.
                    -- Factors are all small enough that that's fine.
                    (D.field "factor" D.float)
                )
                |> D.list
            )
        )
        |> D.map (Dict.Extra.groupBy Tuple.first >> Dict.map (\_ -> List.map Tuple.second))


costEntry : String -> UnitId.Id -> CostVal -> ( String, Cost )
costEntry unit currency count =
    ( unit, ( currency, count ) )
