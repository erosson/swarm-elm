module GameData.Tab exposing (Id, Tab, decoder, idToString, name)

import GameData.Tab.Id as Id
import GameData.Unit.Id as UnitId
import GameData.Util
import Json.Decode as D
import Maybe.Extra


type alias Id =
    Id.Id


idToString : Id -> String
idToString =
    Id.toString


type alias Tab =
    { id : Id
    , unitId : UnitId.Id
    , secondaryUnitId : Maybe UnitId.Id
    , showAchievements : Bool
    }


name : Tab -> String
name =
    .id >> idToString


decoder : D.Decoder Tab
decoder =
    let
        builder id unitId secondaryUnitId =
            D.map (Tab id unitId secondaryUnitId)
                (D.field "achievements" D.bool)
    in
    D.map3 builder
        (D.field "name" Id.decoder)
        (D.field "name" UnitId.decoder)
        (D.field "secondaryUnit" UnitId.decoder |> D.maybe)
        |> D.andThen identity
