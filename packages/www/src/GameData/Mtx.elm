module GameData.Mtx exposing (Id, Mtx, decoder, idToString, name)

import GameData.Mtx.Id as Id
import GameData.Util
import Json.Decode as D


type alias Id =
    Id.Id


idToString : Id -> String
idToString =
    Id.toString


type alias Mtx =
    { id : Id
    , priceUsdPaypal : Int
    , count : Int
    , paypalUrl : PaypalUrls
    , priceKreds : Int
    , bulkBonus : Float
    }


type alias PaypalUrls =
    { sandbox : String
    , elmBeta : String
    , prod : String
    }


name : Mtx -> String
name =
    .id >> idToString


decoder : D.Decoder Mtx
decoder =
    D.map6 Mtx
        (D.field "name" Id.decoder)
        (D.field "price_usd_paypal" D.int)
        (D.field "pack.val" D.int)
        (D.map3 PaypalUrls
            (D.field "paypalSandboxUrl" D.string)
            (D.field "paypalElmSwarmsimDotComUrl" D.string)
            (D.field "paypalSwarmsimDotComUrl" D.string)
        )
        (D.field "price_kreds" D.int)
        (D.field "bulkbonus" D.float)
