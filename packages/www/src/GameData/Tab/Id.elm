module GameData.Tab.Id exposing (Id, all, decoder, fromString, toString)

import Json.Decode as D


type Id
    = Id String


{-| Hardcoded tab id. Use sparingly.
-}
fromString : String -> Id
fromString =
    Id


all : Id
all =
    Id "all"


toString : Id -> String
toString (Id id) =
    id


decoder : D.Decoder Id
decoder =
    D.string |> D.map Id
