module GameData.Item exposing (Id, Item(..), buyGroup, cost, id, idToString, isWatchable, name, onBuy, resToDec, slug, visible)

{-| A unit, upgrade, or spell.
-}

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import GameData.Cost as Cost exposing (Cost, CostVal)
import GameData.Item.Id as Id exposing (Id(..))
import GameData.OnBuy as OnBuy exposing (OnBuy)
import GameData.Spell as Spell exposing (Spell)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Parser.Visibility


type alias Id =
    Id.Id


type Item
    = Unit Unit
    | Upgrade Upgrade
    | Spell Spell


id : Item -> Id
id item =
    case item of
        Unit u ->
            Id.UnitId u.id

        Upgrade u ->
            Id.UpgradeId u.id

        Spell s ->
            Id.SpellId s.id


idToString : Id -> String
idToString =
    Id.toString


name : Item -> String
name =
    id >> idToString


slug : Item -> Maybe String
slug item =
    case item of
        Unit u ->
            Just u.slug

        Spell u ->
            Just u.slug

        Upgrade _ ->
            Nothing


cost : Item -> List Cost
cost item =
    case item of
        Unit u ->
            u.cost

        Upgrade u ->
            u.cost

        Spell u ->
            u.cost


visible : Item -> Parser.Visibility.Expr
visible item =
    case item of
        Unit u ->
            u.visible

        Upgrade u ->
            u.visible

        Spell s ->
            s.visible


isWatchable : Item -> Bool
isWatchable item =
    case item of
        Spell _ ->
            False

        Upgrade u ->
            True

        Unit u ->
            u.watchable


buyGroup : Item -> String
buyGroup item =
    case item of
        Unit u ->
            Unit.name u

        Upgrade u ->
            u.buyGroup

        Spell u ->
            Spell.name u


onBuy : Item -> OnBuy.Index
onBuy item =
    case item of
        Unit u ->
            u.onBuy

        Upgrade u ->
            u.onBuy

        Spell u ->
            u.onBuy


resToDec : Result Decimal Int -> Decimal
resToDec res =
    case res of
        Ok n ->
            Decimal.fromFloat (toFloat n)

        Err d ->
            d
