module Parser.Visibility exposing
    ( Expr, Progress, parse, and
    , fromJson
    , eval, evalWithDefault, evalSet, evalSetWithDefault, progress, variables
    )

{-| A little language for determining the conditions when something is visible, based on the player's units/upgrades.

This is honestly a bit of overkill for what this app needs - a more limited
spreadsheet-based structure would probably be fine. I'm doing this mostly to
learn Elm parsers. Baby's first elm-parser over here.

This was a helpful example:
<https://github.com/elm/parser/blob/1.1.0/examples/Math.elm>


# Parsing

@docs Expr, Progress, parse, and


# Deserializing

@docs fromJson


# Evaluating

@docs eval, evalWithDefault, evalSet, evalSetWithDefault, progress, variables

-}

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Json.Decode as D
import Parser exposing ((|.), (|=), Parser)
import Result.Extra
import Set exposing (Set)


type Expr
    = Ops Op (List Expr)
    | Order DecimalExpr Cmp Decimal
    | Val Bool


type DecimalExpr
    = DecimalVar String
    | Sum (List String)


type Op
    = And
    | Or


type Cmp
    = GT
    | GTE
    | EQ
    | NE
    | LTE
    | LT


{-| Combine two visibility expressions.
-}
and : List Expr -> Expr
and =
    Ops And


parse : String -> Result (List Parser.DeadEnd) Expr
parse =
    Parser.run parser


parser : Parser Expr
parser =
    Parser.succeed identity
        |. Parser.spaces
        |= expression
        |. Parser.spaces
        |. Parser.end


term : Parser Expr
term =
    Parser.oneOf
        [ Parser.succeed (Val True) |. Parser.keyword "TRUE" |. Parser.spaces
        , Parser.succeed (Val False) |. Parser.keyword "FALSE" |. Parser.spaces

        -- Addition support was hacked on pretty quickly - backtrackable is
        -- needed because the leading "(" in "(a + b) > 3" [DecimalExpr] and
        -- "(a > 3)" [Expr] look the same. That said, the Right Way to do this
        -- is a oneOf somewhere after that leading paren.
        , Parser.backtrackable leaf
        , Parser.succeed identity
            |. Parser.symbol "("
            |. Parser.spaces
            |= Parser.lazy (\_ -> expression)
            |. Parser.spaces
            |. Parser.symbol ")"
            |. Parser.spaces
        ]


type ExprSteps
    = ExprStep0 Expr
    | ExprStepN Op (List Expr)


expression : Parser Expr
expression =
    let
        loop : ExprSteps -> Parser (Parser.Step ExprSteps Expr)
        loop step =
            -- No mixing boolean operators: `a && b && c` or `a || b || c`, but no `a && b || c`.
            -- Precedence isn't obvious, so every expression must be explicit with parens.
            -- We do that by passing around the operator, when available.
            Parser.oneOf
                [ let
                    ( lastOp, exprs ) =
                        case step of
                            ExprStep0 e ->
                                ( Nothing, [ e ] )

                            ExprStepN op exprs_ ->
                                ( Just op, exprs_ )
                  in
                  Parser.succeed (\op expr -> ExprStepN op (expr :: exprs) |> Parser.Loop)
                    |= (lastOp |> Maybe.map onlyOperator |> Maybe.withDefault operator)
                    |. Parser.spaces
                    |= term
                    |. Parser.spaces
                , Parser.succeed ()
                    |> Parser.map
                        (\_ ->
                            case step of
                                ExprStep0 expr ->
                                    expr

                                ExprStepN op exprs ->
                                    Ops op exprs
                        )
                    |> Parser.map Parser.Done
                ]
    in
    Parser.succeed identity
        |= term
        |. Parser.spaces
        |> Parser.andThen (\t -> Parser.loop (ExprStep0 t) loop)


operator : Parser Op
operator =
    [ And, Or ] |> List.map onlyOperator |> Parser.oneOf


onlyOperator : Op -> Parser Op
onlyOperator op =
    case op of
        And ->
            Parser.succeed And |. Parser.symbol "&&"

        Or ->
            Parser.succeed Or |. Parser.symbol "||"


label : Parser String
label =
    Parser.variable { start = Char.isLower, inner = \c -> Char.isAlphaNum c || c == '_', reserved = Set.empty }


leaf : Parser Expr
leaf =
    Parser.succeed Order
        |= decimalExpression
        |. Parser.spaces
        |= comparator
        |. Parser.spaces
        |= decimal


decimalExpression : Parser DecimalExpr
decimalExpression =
    Parser.oneOf
        [ Parser.succeed DecimalVar
            |= label
        , Parser.succeed Sum
            |= Parser.sequence
                { start = "("
                , end = ")"
                , separator = "+"
                , spaces = Parser.spaces
                , item = label
                , trailing = Parser.Forbidden
                }
        ]


comparator : Parser Cmp
comparator =
    Parser.oneOf
        [ Parser.succeed GTE |. Parser.keyword ">="
        , Parser.succeed EQ |. Parser.keyword "=="
        , Parser.succeed NE |. Parser.keyword "/="
        , Parser.succeed LTE |. Parser.keyword "<="
        , Parser.succeed LT |. Parser.keyword "<"
        , Parser.succeed GT |. Parser.keyword ">"
        ]


decimal : Parser Decimal
decimal =
    Parser.number
        { int = Just Decimal.fromInt
        , float = Just Decimal.fromFloat
        , hex = Nothing
        , octal = Nothing
        , binary = Nothing
        }


cmpToFn : Cmp -> Decimal -> Decimal -> Bool
cmpToFn cmp =
    case cmp of
        GT ->
            Decimal.gt

        GTE ->
            Decimal.gte

        LT ->
            Decimal.lt

        LTE ->
            Decimal.lte

        EQ ->
            (==)

        NE ->
            (/=)


{-| Evaluate a visibility rule, returning Err if any required variables are missing.
-}
eval : (String -> Maybe Decimal) -> Expr -> Result String Bool
eval getVar rule =
    case rule of
        Val val ->
            Ok val

        Order left0 cmp right ->
            let
                get name =
                    case getVar name of
                        Just left ->
                            Ok left

                        Nothing ->
                            Err <| "no such var: " ++ name

                left1 =
                    case left0 of
                        DecimalVar name ->
                            get name

                        Sum names ->
                            names |> List.map get |> Result.Extra.combine |> Result.map Decimal.sum
            in
            left1 |> Result.map (\left -> cmpToFn cmp left right)

        Ops And exprs ->
            exprs |> List.map (eval getVar) |> Result.Extra.combine |> Result.map (List.all identity)

        Ops Or exprs ->
            exprs |> List.map (eval getVar) |> Result.Extra.combine |> Result.map (List.any identity)


{-| Evaluate a visibility rule, using a default value if any required variables are missing.
-}
evalWithDefault : Decimal -> (String -> Maybe Decimal) -> Expr -> Bool
evalWithDefault defaultVar getVar rule =
    case rule of
        Val val ->
            val

        Order left0 cmp right ->
            let
                left1 =
                    case left0 of
                        DecimalVar name ->
                            name |> getVar |> Maybe.withDefault defaultVar

                        Sum names ->
                            names |> List.map (getVar >> Maybe.withDefault defaultVar) |> Decimal.sum
            in
            cmpToFn cmp left1 right

        Ops And exprs ->
            exprs |> List.all (evalWithDefault defaultVar getVar)

        Ops Or exprs ->
            exprs |> List.any (evalWithDefault defaultVar getVar)


variables : Expr -> Set String
variables rule =
    let
        loop : Expr -> Set String -> Set String
        loop expr ret =
            case expr of
                Val _ ->
                    ret

                Order left _ _ ->
                    case left of
                        DecimalVar name ->
                            Set.insert name ret

                        Sum names ->
                            Set.union ret <| Set.fromList names

                Ops _ exprs ->
                    List.foldl loop ret exprs
    in
    loop rule Set.empty


type alias Progress =
    { name : String, percent : Float, val : Decimal, max : Decimal }


progress : (String -> Maybe Decimal) -> Expr -> List Progress
progress getVar rule =
    let
        loop : Expr -> List Progress -> List Progress
        loop expr ret =
            case expr of
                Val _ ->
                    ret

                Order (DecimalVar name) GTE right ->
                    let
                        val =
                            getVar name |> Maybe.withDefault Decimal.zero

                        pct =
                            Decimal.div val right |> Decimal.toFloat |> clamp 0 1
                    in
                    Progress name pct val right :: ret

                Order _ _ _ ->
                    ret

                Ops _ exprs ->
                    List.foldl loop ret exprs
    in
    loop rule []


{-| Evaluate a set of visibility rules, returning Err if any required variables are missing.

Once a value is visible, it never becomes invisible.

-}
evalSet : (String -> Maybe Decimal) -> List ( String, Expr ) -> Set String -> Result String (Set String)
evalSet getVar rules alreadyVisible =
    rules
        -- once it's visible, it should never be invisible again. Don't re-eval.
        |> List.filter (\( name, _ ) -> not <| Set.member name alreadyVisible)
        -- eval all, merge with previous results
        |> List.map (\( name, expr ) -> expr |> eval getVar |> Result.map (Tuple.pair name))
        |> Result.Extra.combine
        |> Result.map
            (List.filter Tuple.second
                >> List.map Tuple.first
                >> Set.fromList
                >> Set.union alreadyVisible
            )


{-| Evaluate a set of visibility rules, using a default value if any required variables are missing.

Once a value is visible, it never becomes invisible.

-}
evalSetWithDefault : Decimal -> (String -> Maybe Decimal) -> List ( String, Expr ) -> Set String -> Set String
evalSetWithDefault defaultVar getVar rules alreadyVisible =
    rules
        -- once it's visible, it should never be invisible again. Don't re-eval.
        |> List.filter (\( name, _ ) -> not <| Set.member name alreadyVisible)
        -- eval all, merge with previous results
        |> List.map (Tuple.mapSecond (evalWithDefault defaultVar getVar))
        |> List.filter Tuple.second
        |> List.map Tuple.first
        |> Set.fromList
        |> Set.union alreadyVisible


fromJson : D.Decoder Expr
fromJson =
    let
        decode str =
            case parse str of
                Ok expr ->
                    D.succeed expr

                Err err ->
                    D.fail (Parser.deadEndsToString err)
    in
    D.oneOf
        [ D.string |> D.andThen decode

        -- The simplest visibility rules of "TRUE" and "FALSE" appear in the
        -- spreadsheet JSON as boolean types, not strings. Sigh.
        , D.bool |> D.map Val
        ]
