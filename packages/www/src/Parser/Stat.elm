module Parser.Stat exposing
    ( Expr, parse, parseInt, parseFloat, parseDecimal
    , fromJson
    , eval, evalFull, evalWithDefault, evalFullWithDefault, variables, toString
    , EvalContext
    )

{-| A little language for doing math and showing your work along the way.

    -- Start with some variables to pass to your equation:
    vars : Dict String Decimal
    vars = Dict.singleton "a" <| Decimal.fromString "3"

    -- Evaluate some math:
    parse "3 * 2 ^ a" |> Result.andThen (eval vars) --> Ok (Decimal.fromString "24")

    -- Label different sections of your formula. You'll get the same result:
    parse "(base: 3) * (bonus: 2 ^ a)" |> Result.andThen (eval vars) --> Ok (Decimal.fromString "24")

    -- You can choose to output the value of each label, too:
    parse "(base: 3) * (bonus: 2 ^ a)" |> Result.andThen (evalFull vars)
        --> Ok ({exports: Dict.fromList [("base", Decimal.fromString "3"), ("bonus", Decimal.fromString "8")], out: Decimal.fromString "24"})

This is useful for defining the formulas that drive a game, and then describing
the components of those formulas to the player on a detailed statistics screen,
without slowing down your gameplay calculations when the more detailed output
isn't needed.

---

This was a helpful example:
<https://github.com/elm/parser/blob/1.1.0/examples/Math.elm>


# Parsing

@docs Expr, parse, parseInt, parseFloat, parseDecimal


# Deserializing

@docs fromJson


# Evaluating

@docs eval, evalFull, evalWithDefault, evalFullWithDefault, variables, toString

-}

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Json.Decode as D
import Maybe.Extra
import Parser exposing ((|.), (|=), Parser)
import Result.Extra
import Set exposing (Set)


type Expr
    = Var String
    | Val Decimal
    | Let (List ( String, Expr )) Expr
    | Export String Expr
    | Function Function (List Expr)
    | BinaryOp BinaryOp Expr Expr


type BinaryOp
    = Add
    | Sub
    | Mul
    | Div
    | Exp


type Function
    = Min
    | Max
    | Log


parseInt : Int -> Expr
parseInt =
    Decimal.fromInt >> parseDecimal


parseFloat : Float -> Expr
parseFloat =
    Decimal.fromFloat >> parseDecimal


parseDecimal : Decimal -> Expr
parseDecimal =
    Val


parse : String -> Result (List Parser.DeadEnd) Expr
parse =
    Parser.run parser


parser : Parser Expr
parser =
    Parser.succeed identity
        |. Parser.spaces
        |= expression
        |. Parser.spaces
        |. Parser.end


term : Parser Expr
term =
    Parser.oneOf
        [ Parser.succeed Let
            |. Parser.keyword "let"
            |. Parser.spaces
            |= bindings
            |. Parser.spaces
            |. Parser.keyword "in"
            |. Parser.spaces
            |= Parser.lazy (\_ -> expression)
        , Parser.succeed Tuple.pair
            -- backtrackable sucks, but this looks identical to label/export until we see the parens
            |= Parser.backtrackable functionName
            |. Parser.backtrackable Parser.spaces
            |= Parser.sequence
                { start = "("
                , end = ")"
                , separator = ","
                , spaces = Parser.spaces
                , item = Parser.lazy (\_ -> expression)
                , trailing = Parser.Forbidden
                }
            |> Parser.andThen (\( name, params ) -> function name params)
        , Parser.succeed (\name fn -> fn name)
            |= label
            |= Parser.oneOf
                [ Parser.succeed (\expr name -> Export name expr)
                    |. Parser.backtrackable Parser.spaces
                    |. Parser.symbol ":"
                    |. Parser.spaces
                    |= Parser.lazy (\_ -> expression)
                , Parser.succeed Var
                ]
        , Parser.succeed Val
            |= decimal
        , Parser.succeed identity
            |. Parser.symbol "("
            |. Parser.spaces
            |= Parser.lazy (\_ -> expression)
            |. Parser.spaces
            |. Parser.symbol ")"
        ]


functionName : Parser Function
functionName =
    label
        |> Parser.andThen
            (\name ->
                case name of
                    "min" ->
                        Parser.succeed Min

                    "max" ->
                        Parser.succeed Max

                    "log" ->
                        Parser.succeed Log

                    _ ->
                        Parser.problem ("no such function: " ++ name)
            )


function : Function -> List Expr -> Parser Expr
function fn params =
    case fn of
        Min ->
            Parser.succeed (Function fn params)

        Max ->
            Parser.succeed (Function fn params)

        Log ->
            case params of
                [ val ] ->
                    Parser.succeed (Function fn params)

                [ val, base ] ->
                    Parser.succeed (Function fn params)

                _ ->
                    Parser.problem <| "invalid log() param count: " ++ String.fromInt (List.length params)


expression : Parser Expr
expression =
    let
        loop : ( Expr, List ( BinaryOp, Expr ) ) -> Parser (Parser.Step ( Expr, List ( BinaryOp, Expr ) ) Expr)
        loop ( head, revOps ) =
            Parser.succeed identity
                |. Parser.spaces
                |= Parser.oneOf
                    [ Parser.succeed (\op expr -> ( head, ( op, expr ) :: revOps ) |> Parser.Loop)
                        |= binaryOperator
                        |. Parser.spaces
                        |= term
                    , Parser.succeed (finalize head (List.reverse revOps) |> Parser.Done)
                    ]
    in
    Parser.succeed identity
        |= term
        |> Parser.andThen (\t -> Parser.loop ( t, [] ) loop)


binding : Parser ( String, Expr )
binding =
    Parser.succeed Tuple.pair
        |= label
        |. Parser.spaces
        |. Parser.symbol "="
        |. Parser.spaces
        |= Parser.lazy (\_ -> expression)
        |. Parser.spaces
        -- bindings end with a ;
        |. Parser.chompIf (\c -> c == ';')


bindings : Parser (List ( String, Expr ))
bindings =
    let
        loop revBindings =
            Parser.oneOf
                [ Parser.succeed (\b -> b :: revBindings |> Parser.Loop)
                    |= binding
                    |. Parser.spaces
                , Parser.succeed (List.reverse revBindings |> Parser.Done)
                ]
    in
    Parser.succeed identity
        |= binding
        |. Parser.spaces
        |> Parser.andThen (\b -> Parser.loop [ b ] loop)


finalize : Expr -> List ( BinaryOp, Expr ) -> Expr
finalize expr0 ops =
    -- Based on https://github.com/elm/parser/blob/1.1.0/examples/Math.elm : finalize
    -- I made up the precedence-comparing part myself, but it seems to work okay!
    case ops of
        [] ->
            expr0

        -- only one operation left, precedence doesn't matter
        ( op1, expr1 ) :: [] ->
            BinaryOp op1 expr0 expr1

        -- compare op1's precedence to the next operator, op2. Don't actually apply op2 yet, just peek.
        ( op1, expr1 ) :: ((( op2, _ ) :: _) as tail) ->
            if precedence op1 >= precedence op2 then
                -- expr0 * expr1 + ... -> (expr0 * expr1) + ...
                -- expr0 ^ expr1 * ... -> (expr0 ^ expr1) * ...
                -- expr0 - expr1 + ... -> (expr0 - expr1) + ...
                -- expr0 / expr1 * ... -> (expr0 / expr1) * ...
                finalize (BinaryOp op1 expr0 expr1) tail

            else
                -- expr0 + expr1 * ... -> expr0 + (expr1 * ...)
                -- expr0 * expr1 ^ ... -> expr0 * (expr1 ^ ...)
                BinaryOp op1 expr0 (finalize expr1 tail)


{-| Higher precedence operators are applied first.

`PEMDAS`, just like math class, except our parser already grouped by parens

-}
precedence : BinaryOp -> Int
precedence op =
    case op of
        Exp ->
            9

        Mul ->
            5

        Div ->
            5

        Add ->
            1

        Sub ->
            1


binaryOperator : Parser BinaryOp
binaryOperator =
    Parser.oneOf
        [ Parser.succeed Add
            |. Parser.symbol "+"
        , Parser.succeed Sub
            |. Parser.symbol "-"
        , Parser.succeed Mul
            |. Parser.symbol "*"
        , Parser.succeed Div
            |. Parser.symbol "/"
        , Parser.succeed Exp
            |. Parser.symbol "^"
        ]


decimal : Parser Decimal
decimal =
    Parser.oneOf
        [ Parser.succeed Decimal.neg
            |. Parser.symbol "-"
            -- no spaces!
            |= unsignedDecimal
        , unsignedDecimal
        ]


unsignedDecimal : Parser Decimal
unsignedDecimal =
    Parser.number
        { int = Just Decimal.fromInt
        , float = Just Decimal.fromFloat
        , hex = Nothing
        , octal = Nothing
        , binary = Nothing
        }


label : Parser String
label =
    Parser.variable { start = Char.isLower, inner = \c -> Char.isAlphaNum c || c == '_', reserved = Set.fromList [ "let", "in" ] }



-- Evaluating parsed rules


{-| check two dicts for a value. Prefer the first.
-}
dictGet2 : comparable -> Dict comparable val -> Dict comparable val -> Maybe val
dictGet2 key dict1 dict2 =
    case Dict.get key dict1 of
        Nothing ->
            Dict.get key dict2

        just ->
            just


type alias EvalContext =
    { exports : Dict String Decimal, out : Decimal }


{-| Evaluate a visibility rule, returning Err if any required variables are missing.
-}
eval : (String -> Maybe Decimal) -> Expr -> Result String Decimal
eval getVar rule =
    evalFull getVar rule |> Result.map .out


evalFull : (String -> Maybe Decimal) -> Expr -> Result String EvalContext
evalFull getVar expr =
    let
        ctx =
            -- the default var here doesn't matter, any use of it is an error
            -- (unifying this and evalWithDefault means the compiler can't prove
            -- that anymore, but it's worth it)
            evalInternal Decimal.zero getVar expr
    in
    case ctx.error of
        Just err ->
            Err err

        Nothing ->
            Ok <| EvalContext ctx.exports ctx.out


{-| Evaluate a visibility rule, using a default value if any required variables are missing.
-}
evalWithDefault : Decimal -> (String -> Maybe Decimal) -> Expr -> Decimal
evalWithDefault defaultVar getVar rule =
    (evalFullWithDefault defaultVar getVar rule).out


evalFullWithDefault : Decimal -> (String -> Maybe Decimal) -> Expr -> EvalContext
evalFullWithDefault defaultVar getVar expr =
    let
        ctx =
            evalInternal defaultVar getVar expr
    in
    EvalContext ctx.exports ctx.out


type alias EvalInternal =
    { error : Maybe String, exports : Dict String Decimal, out : Decimal }


{-| Evaluate a visibility rule and its top-level exports.

Track errors as we go, and also keep evaluating with a default value in the face
of errors. This makes the code a little more complex, but allows eval and
evalWithDefault to use the same code, instead of writing one `Result`-based and
one default-based function. Worth the extra complexity.

-}
evalInternal : Decimal -> (String -> Maybe Decimal) -> Expr -> EvalInternal
evalInternal defaultVar getVar =
    let
        loop : Maybe String -> Dict String Decimal -> Dict String Decimal -> Expr -> EvalInternal
        loop err0 exports0 env0 rule =
            case rule of
                Val val ->
                    EvalInternal err0 exports0 val

                Var var ->
                    case getVar var |> Maybe.Extra.orElse (Dict.get var env0) of
                        Just val ->
                            EvalInternal err0 exports0 val

                        Nothing ->
                            EvalInternal (Just <| "no such var: " ++ var) exports0 defaultVar

                Let binds body ->
                    let
                        fold : ( String, Expr ) -> ( Maybe String, Dict String Decimal, Dict String Decimal ) -> ( Maybe String, Dict String Decimal, Dict String Decimal )
                        fold ( name, expr ) ( error1, exports1, env1 ) =
                            let
                                ctx2 =
                                    loop error1 exports1 env1 expr
                            in
                            ( ctx2.error, ctx2.exports, Dict.insert name ctx2.out env1 )

                        ( err, exports, env ) =
                            List.foldl fold ( err0, exports0, env0 ) binds
                    in
                    loop err exports env body

                Export name body0 ->
                    let
                        ctx =
                            loop err0 exports0 env0 body0
                    in
                    { ctx | exports = Dict.insert name ctx.out ctx.exports }

                Function fnEnum params0 ->
                    let
                        fold : Expr -> ( Maybe String, Dict String Decimal, List Decimal ) -> ( Maybe String, Dict String Decimal, List Decimal )
                        fold expr ( err1, exports1, revParams1 ) =
                            let
                                ctx =
                                    loop err1 exports1 env0 expr
                            in
                            ( ctx.error, ctx.exports, ctx.out :: revParams1 )

                        fn : List Decimal -> Result String Decimal
                        fn =
                            case fnEnum of
                                Min ->
                                    Decimal.minimum >> Result.fromMaybe "cannot apply fn to empty list"

                                Max ->
                                    Decimal.maximum >> Result.fromMaybe "cannot apply fn to empty list"

                                Log ->
                                    \params ->
                                        case params of
                                            [ val ] ->
                                                Ok <| Decimal.fromFloat <| Decimal.logBase 10 val

                                            [ val, base ] ->
                                                Ok <| Decimal.fromFloat <| Decimal.logBase (Decimal.toFloat base) val

                                            _ ->
                                                Err ("invalid log arg count (but the parser should have caught this - bug!) " ++ String.fromInt (List.length params))

                        ( err, exports, revParams ) =
                            List.foldl fold ( err0, exports0, [] ) params0
                    in
                    case fn (List.reverse revParams) of
                        Ok out ->
                            EvalInternal err exports out

                        Err err_ ->
                            EvalInternal (err |> Maybe.withDefault err_ |> Just) exports defaultVar

                BinaryOp op left right ->
                    let
                        opFn =
                            case op of
                                Add ->
                                    Decimal.add

                                Sub ->
                                    Decimal.sub

                                Mul ->
                                    Decimal.mul

                                Div ->
                                    Decimal.div

                                Exp ->
                                    \base exp -> Decimal.powFloat base <| Decimal.toFloat exp

                        ctx1 =
                            loop err0 exports0 env0 left

                        ctx2 =
                            loop err0 exports0 env0 right
                    in
                    EvalInternal (Maybe.Extra.or ctx1.error ctx2.error)
                        (Dict.union ctx1.exports ctx2.exports)
                        (opFn ctx1.out ctx2.out)
    in
    loop Nothing Dict.empty Dict.empty


{-| List all external variable names used in the expression.
-}
variables : Expr -> Set String
variables topExpr =
    let
        loop : Expr -> Set String -> Set String
        loop expr ret =
            case expr of
                Val _ ->
                    ret

                Var name ->
                    Set.insert name ret

                Let binds body ->
                    Set.diff (List.foldl loop ret (body :: List.map Tuple.second binds))
                        -- let-bindings aren't *external* variables; ignore them
                        (Set.fromList <| List.map Tuple.first binds)

                Function _ params ->
                    List.foldl loop ret params

                Export _ body ->
                    loop body ret

                BinaryOp _ left right ->
                    ret |> loop left |> loop right
    in
    loop topExpr Set.empty


{-| Stringify the fully-parenthesized parsed expression. Mostly for debugging.
-}
toString : Expr -> String
toString expr =
    case expr of
        Val val ->
            Decimal.toString val

        Var var ->
            var

        Let binds body ->
            "let "
                ++ (binds
                        |> List.map (\( name, bindExpr ) -> name ++ " = " ++ toString bindExpr ++ ";")
                        |> String.join "\n    "
                   )
                ++ "\nin\n"
                ++ toString body

        Export name body ->
            "(" ++ name ++ ": " ++ toString body ++ ")"

        Function enum params ->
            let
                name =
                    case enum of
                        Min ->
                            "min"

                        Max ->
                            "max"

                        Log ->
                            "log"
            in
            name ++ "(" ++ String.join ", " (params |> List.map toString) ++ ")"

        BinaryOp op left right ->
            let
                opFn =
                    case op of
                        Add ->
                            "+"

                        Sub ->
                            "-"

                        Mul ->
                            "*"

                        Div ->
                            "/"

                        Exp ->
                            "^"
            in
            "(" ++ String.join " " [ toString left, opFn, toString right ] ++ ")"


fromJson : D.Decoder Expr
fromJson =
    let
        decode str =
            case parse str of
                Ok expr ->
                    D.succeed expr

                Err err ->
                    -- D.fail (Debug.toString err)
                    D.fail (Parser.deadEndsToString err)
    in
    D.oneOf
        [ D.string |> D.andThen decode

        -- The simplest numeric stat rules appear in the
        -- spreadsheet JSON as number types, not strings. Sigh.
        , D.float |> D.map (Decimal.fromFloat >> Val)
        ]
