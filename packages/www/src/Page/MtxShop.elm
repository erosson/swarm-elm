module Page.MtxShop exposing (Model, Msg(..), create, keyBindings, subscriptions, toSession, update, view)

{-| Crystal microtransaction page.

This is mostly just another Unit page, except that Mtx needs its own messages.
This is important enough to extract to its own page. The url is like that of any
other unit.

There is a lot copied from the Unit/Spell pages. I'd rather have a bit of
duplicate code than introduce weird branching.

-}

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Item.Id as ItemId
import GameData.Mtx as Mtx exposing (Mtx)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Decode as D
import KeyCombo
import Kongregate.Inventory
import Maybe.Extra
import Page.Unit
import PlayFab
import PlayFab.Error
import PlayFab.Inventory
import PlayFab.TitleId
import PlayFab.User as User exposing (User)
import Ports.Kongregate as Ports
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Route.Feature as Feature
import Select
import Session exposing (Session)
import Session.Iframe as Iframe exposing (Iframe)
import Session.PaypalEnv as PaypalEnv exposing (PaypalEnv)
import Time exposing (Posix)
import View.Description.Hatchery
import View.DescriptionVars
import View.Device
import View.Icon
import View.ItemDebug
import View.Lang as T exposing (Lang)
import View.NotReady
import View.TabNav
import View.UnitSidebar


type
    Model
    -- TODO: this model techincally allows a PlayFab InventoryResponse with a
    -- Kongregate PurchaseResult, or a Kongregate InventoryResponse with a
    -- Paypal PurchaseResult - these make no sense. The business logic *should*
    -- disallow these, but we really should refactor to make those "impossible"
    -- states truly impossible.
    = Model InventoryResponse String (Maybe PurchaseResult) Session


type InventoryResponse
    = PlayFabInventory (RemoteData PlayFab.Error.Error PlayFab.Inventory.Inventory)
    | KongregateInventory (RemoteData String Kongregate.Inventory.Inventory)


type PurchaseResult
    = PaypalPurchase (RemoteData PlayFab.Error.Error String) Route.PaypalReturnArgs
    | KongregatePurchase Mtx


create : String -> Maybe Route.PaypalReturnArgs -> Session -> ( Model, Cmd Msg )
create tab paypalReturnArgs session0 =
    recreate tab (paypalReturnArgs |> Maybe.map (PaypalPurchase RemoteData.NotAsked)) session0


recreate : String -> Maybe PurchaseResult -> Session -> ( Model, Cmd Msg )
recreate tab purchase session0 =
    -- The loading sequence here is complex.
    -- * Wait for the game to load.
    -- * Wait for the playfab user to load.
    -- * Notify playfab/paypal of the paypal PDT transaction, if any. (If we just returned from a Paypal purchase)
    --   https://developer.paypal.com/docs/classic/paypal-payments-standard/integration-guide/paymentdatatransfer/
    --   sandbox example: ?tx=7F684044B1316500U
    -- * Finally, load the user's Playfab or Kongregate inventory.
    let
        notAsked =
            case Session.iframe session0 of
                Iframe.WWW ->
                    PlayFabInventory RemoteData.NotAsked

                Iframe.Kongregate _ ->
                    KongregateInventory RemoteData.NotAsked
    in
    case Session.game session0 of
        RemoteData.Success game0 ->
            let
                ( game, cmd ) =
                    game0 |> Game.setLastUnitByTab tab (Just "crystal") |> Session.persistWrite session0
            in
            case Session.user session0 of
                RemoteData.Success (Just user) ->
                    case Session.iframe session0 of
                        Iframe.Kongregate kong ->
                            ( Model (KongregateInventory RemoteData.Loading) tab purchase session0
                            , Cmd.batch
                                [ cmd
                                , Ports.kongregateRequestUserItemListReq ()
                                ]
                            )

                        Iframe.WWW ->
                            let
                                loadInventory =
                                    ( Model (PlayFabInventory RemoteData.Loading) tab purchase session0
                                    , Cmd.batch
                                        [ cmd
                                        , PlayFab.inventory (PlayFab.TitleId.fromSession session0) user GotPlayFabInventory
                                        ]
                                    )
                            in
                            case purchase of
                                Just (PaypalPurchase paypalStatus paypalReturnArgs) ->
                                    case paypalStatus of
                                        RemoteData.NotAsked ->
                                            -- We just returned from a paypal purchase. Let Playfab know.
                                            ( Model notAsked tab (Just (PaypalPurchase RemoteData.Loading paypalReturnArgs)) session0
                                            , PlayFab.paypalNotify (PlayFab.TitleId.fromSession session0) user paypalReturnArgs.tx GotPaypalNotify
                                            )

                                        RemoteData.Loading ->
                                            -- We're still notifying paypal, do nothing new yet
                                            ( Model notAsked tab purchase session0, Cmd.none )

                                        _ ->
                                            -- Request over. Success or failure doesn't matter
                                            loadInventory

                                _ ->
                                    -- We didn't just return from a paypal purchase.
                                    -- Load the user's playfab inventory.
                                    loadInventory

                _ ->
                    ( Model notAsked tab purchase session0, cmd )

        _ ->
            ( Model notAsked tab purchase session0, Cmd.none )


toSession : Model -> Session
toSession (Model _ _ _ s) =
    s


type Msg
    = GotSession Session
    | GotPlayFabInventory (Result PlayFab.Error.Error PlayFab.Inventory.Inventory)
    | GotKongregateInventory { success : Bool, data : D.Value }
    | GotKongregatePurchaseResult { success : Bool, item : String }
    | GotPaypalNotify (Result PlayFab.Error.Error String)
    | ClickedRoute Route
    | ClickedKongBuyKreds
    | ClickedKongBuyMtx Mtx
    | GotTabNavMsg View.TabNav.Msg
    | Noop


keyBindings : Model -> Result String (KeyCombo.Bindings Msg)
keyBindings (Model invRes tabSlug paypalReturnArgs session) =
    Page.Unit.itemKeyBindings tabSlug "crystal" session Noop ClickedRoute


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model invRes0 tabName purchase session) as model) =
    case msg of
        Noop ->
            ( model, Cmd.none )

        GotSession s ->
            case invRes0 of
                PlayFabInventory RemoteData.NotAsked ->
                    recreate tabName purchase s

                KongregateInventory RemoteData.NotAsked ->
                    recreate tabName purchase s

                _ ->
                    ( Model invRes0 tabName purchase s, Cmd.none )

        GotPaypalNotify res ->
            case purchase of
                Just (PaypalPurchase _ paypalReturnArgs) ->
                    -- The result doesn't actually matter. Once Playfab's notified, we load the player's inventory.
                    recreate tabName (Just (PaypalPurchase (RemoteData.fromResult res) paypalReturnArgs)) session

                _ ->
                    -- should be impossible!
                    ( Model invRes0 tabName purchase session, Cmd.none )

        GotPlayFabInventory invRes ->
            -- Try to add missing mtx on load
            let
                session1 =
                    case ( invRes, Session.game session ) of
                        ( Ok inv, RemoteData.Success game ) ->
                            Session.setGame False (Game.updatePlayFabMtxHistory inv game) session

                        _ ->
                            session
            in
            ( Model (PlayFabInventory (RemoteData.fromResult invRes)) tabName purchase session1, Cmd.none )

        GotKongregateInventory json ->
            let
                invRes =
                    D.decodeValue Kongregate.Inventory.decoder json.data
                        |> Result.mapError D.errorToString

                -- Try to add missing mtx on load
                session1 =
                    case ( invRes, Session.game session ) of
                        ( Ok inv, RemoteData.Success game ) ->
                            Session.setGame False (Game.updateKongMtxHistory inv game) session

                        _ ->
                            session
            in
            ( Model (KongregateInventory (RemoteData.fromResult invRes)) tabName purchase session1, Cmd.none )

        ClickedRoute route ->
            ( Model invRes0 tabName purchase session, Route.pushUrl (Session.nav session) (Session.query session) route )

        ClickedKongBuyKreds ->
            ( model, Ports.kongregateShowKredPurchaseDialog () )

        ClickedKongBuyMtx mtx ->
            ( model, Ports.kongregatePurchaseItemsReq <| Mtx.idToString mtx.id )

        GotKongregatePurchaseResult { success, item } ->
            if success then
                case Session.gameData session of
                    Ok gd ->
                        case GameData.mtxByName item gd of
                            Just mtx ->
                                ( Model invRes0 tabName (Just <| KongregatePurchase mtx) session
                                , Ports.kongregateRequestUserItemListReq ()
                                )

                            Nothing ->
                                ( model, Cmd.none )

                    Err _ ->
                        ( model, Cmd.none )

            else
                ( model, Cmd.none )

        GotTabNavMsg submsg ->
            View.TabNav.update submsg session
                |> Tuple.mapFirst (Model invRes0 tabName purchase)


subscriptions : Sub Msg
subscriptions =
    Sub.batch
        [ Ports.kongregateRequestUserItemListRes GotKongregateInventory
        , Ports.kongregatePurchaseItemsRes GotKongregatePurchaseResult
        ]



-- VIEW


view : Model -> Game -> List (Html Msg)
view (Model invRes tabSlug purchase session) game =
    View.NotReady.viewResult session (Game.gameData game) <|
        let
            snap =
                Session.safeUnits game session
        in
        case
            ( Select.tabBySlug tabSlug game snap
            , Select.unitBySlug "crystal" game snap
            )
        of
            ( Just stab, Just ( unit, count ) ) ->
                (View.TabNav.view (Maybe.map .id <| Select.unwrapTab stab) session game snap
                    |> List.map (H.map GotTabNavMsg)
                )
                    ++ (View.UnitSidebar.wrap ClickedRoute stab (Item.Unit unit) session game snap <|
                            viewUnit stab session game snap unit count invRes purchase
                       )
                    ++ View.ItemDebug.view session game snap [ Item.Unit unit ]
                    |> Ok

            ( Nothing, _ ) ->
                Err "no such tab"

            ( _, Nothing ) ->
                Err "no such unit"


viewUnit :
    Select.STab
    -> Session
    -> Game
    -> UnitsSnapshot
    -> Unit
    -> Decimal
    -> InventoryResponse
    -> Maybe PurchaseResult
    -> List (Html Msg)
viewUnit stab session game snapshot unit count invRes purchase =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabSlug =
            Select.tabToSlug stab

        production =
            Game.production unit game snapshot

        order =
            Order.fromBuyN game snapshot (Item.Unit unit)
    in
    [ h3 []
        [ if Feature.isActive Feature.MobileUI query then
            View.Device.onlyMobileInline [ a [ Route.href query <| Route.Tab tabSlug ] [ View.Icon.fas "bars", text " " ] ]

          else
            span [] []
        , lang.html <| T.ViewUnitHeader unit.id
        ]
    , div
        [ class "desc-icon-resource"
        , class <| "desc-icon-" ++ Unit.name unit
        , class <| "icon-" ++ Unit.name unit
        ]
        []
    ]
        ++ viewDescription session game unit count
        ++ [ p []
                ((lang.html <| T.ViewUnitCount count unit.id)
                    :: (case invRes of
                            PlayFabInventory RemoteData.Loading ->
                                [ View.Icon.loading ]

                            KongregateInventory RemoteData.Loading ->
                                [ View.Icon.loading ]

                            _ ->
                                if RemoteData.isLoading (Session.user session) then
                                    [ View.Icon.loading ]

                                else
                                    []
                       )
                )
           ]
        ++ viewPaypalResult session game purchase
        ++ viewMtxs session game
        ++ [ p []
                [ a
                    [ class "btn btn-link"
                    , Route.href query <| Route.unit "energy" "energy"
                    ]
                    [ lang.html T.ViewMtxShopConvert ]
                ]
           , p []
                [ a
                    [ class "btn btn-link"
                    , Route.href query Route.MtxHistory
                    ]
                    [ lang.html T.ViewMtxShopHistory ]
                ]
           ]


viewPaypalResult : Session -> Game -> Maybe PurchaseResult -> List (Html Msg)
viewPaypalResult session game purchase =
    let
        lang =
            T.gameToLang session game
    in
    case purchase of
        Nothing ->
            []

        Just (KongregatePurchase mtx) ->
            [ div [ class "alert alert-success" ]
                [ h6 [] [ lang.html T.ViewMtxShopConfirmed ]
                ]
            ]

        Just (PaypalPurchase res args) ->
            let
                paypalUrl =
                    case Session.paypalEnv session of
                        PaypalEnv.Production ->
                            "https://www.paypal.com/activity/payment/" ++ args.tx

                        _ ->
                            "https://www.sandbox.paypal.com/activity/payment/" ++ args.tx

                txnHref =
                    p [] [ a [ href paypalUrl, target "_blank" ] [ lang.html <| T.ViewMtxShopPaypalTx args.tx ] ]
            in
            case res of
                RemoteData.Success instanceId ->
                    [ div [ class "alert alert-success" ]
                        [ h6 [] [ lang.html T.ViewMtxShopConfirmed ]
                        , txnHref
                        ]
                    ]

                RemoteData.Failure err ->
                    [ div [ class "alert alert-danger" ]
                        [ text <| PlayFab.Error.toString err
                        , txnHref
                        ]
                    ]

                _ ->
                    [ div [ class "alert alert-info" ]
                        [ p [] [ View.Icon.loading ]
                        , txnHref
                        ]
                    ]


viewDescription : Session -> Game -> Unit -> Decimal -> List (Html Msg)
viewDescription session game unit count =
    let
        lang =
            T.gameToLang session game

        hatchery =
            GameData.unitByName "hatchery" (Game.gameData game)

        hatcheryTimer =
            Maybe.andThen
                (\h ->
                    View.Description.Hatchery.crystalStats session game h.onBuy.addUnitsCooldown
                )
                hatchery
    in
    [ lang.html <| T.UnitDesc (View.DescriptionVars.evalVar game) unit.id
    , p []
        (Maybe.map2
            (\h t ->
                [ lang.html <| T.DescHatcheryCrystals (ItemId.UnitId h.id) t ]
            )
            hatchery
            hatcheryTimer
            |> Maybe.withDefault []
        )
    ]


viewMtxs : Session -> Game -> List (Html Msg)
viewMtxs session game =
    case Session.iframe session of
        Iframe.WWW ->
            viewPaypalMtxs session game

        Iframe.Kongregate kong ->
            viewKongMtxs session game


viewKongMtxs : Session -> Game -> List (Html Msg)
viewKongMtxs session game =
    let
        lang =
            T.gameToLang session game

        mtxs : List Mtx
        mtxs =
            GameData.mtxs (Game.gameData game)
    in
    case Session.user session of
        RemoteData.Success (Just user) ->
            if Feature.isActive Feature.Mtx (Session.query session) then
                -- Normally I wouldn't warn people, the feaure switch is enough
                -- of a warning, but real money is Serious Business.
                [ p []
                    [ button [ class "btn btn-outline-primary", onClick ClickedKongBuyKreds ]
                        [ img [ class "img-icon", src "/images/kred_stack.png" ] []
                        , text " "
                        , lang.html T.ViewMtxShopBuyKreds
                        ]
                    ]
                , div [] (mtxs |> List.map (viewKongMtx session game user))
                , p [] [ i [] [ lang.html T.ViewMtxShopSupport ] ]
                ]

            else
                [ p [] [ i [] [ text "Crystal purchases will return soon!" ] ] ]

        RemoteData.Success Nothing ->
            [ div [ class "alert alert-danger" ]
                [ a [ Route.href (Session.query session) Route.Login ]
                    [ lang.html T.ViewMtxShopLogin ]
                ]
            ]

        RemoteData.Failure err ->
            [ div [ class "alert alert-danger" ] [ text <| PlayFab.Error.toString err ] ]

        _ ->
            [ View.Icon.loading ]


viewKongMtx : Session -> Game -> User -> Mtx -> Html Msg
viewKongMtx session game user mtx =
    let
        lang =
            T.gameToLang session game
    in
    div []
        [ button [ class "btn btn-outline-primary", onClick <| ClickedKongBuyMtx mtx ]
            [ lang.html <| T.ViewMtxKongButton { cost = mtx.priceKreds, count = mtx.count } ]
        ]


viewPaypalMtxs : Session -> Game -> List (Html Msg)
viewPaypalMtxs session game =
    let
        lang =
            T.gameToLang session game

        mtxs : List Mtx
        mtxs =
            GameData.mtxs (Game.gameData game)
    in
    case Session.user session of
        RemoteData.Success (Just user) ->
            if Feature.isActive Feature.Mtx (Session.query session) then
                -- Normally I wouldn't warn people, the feaure switch is enough
                -- of a warning, but real money is Serious Business.
                [ div [] (mtxs |> List.map (viewPaypalMtx session game user))
                , p [] [ i [] [ lang.html T.ViewMtxShopSupport ] ]
                ]

            else
                [ p [] [ i [] [ text "Crystal purchases will return soon!" ] ] ]

        RemoteData.Success Nothing ->
            [ div [ class "alert alert-danger" ]
                [ a [ Route.href (Session.query session) Route.Login ]
                    [ lang.html T.ViewMtxShopLogin ]
                ]
            ]

        RemoteData.Failure err ->
            [ div [ class "alert alert-danger" ] [ text <| PlayFab.Error.toString err ] ]

        _ ->
            [ View.Icon.loading ]


viewPaypalMtx : Session -> Game -> User -> Mtx -> Html Msg
viewPaypalMtx session game user mtx =
    let
        lang =
            T.gameToLang session game
    in
    div []
        [ a [ class "btn btn-outline-primary", href <| PaypalEnv.url (Session.paypalEnv session) (User.id user) mtx ]
            [ lang.html <| T.ViewMtxPaypalButton { cost = mtx.priceUsdPaypal, count = mtx.count } ]
        ]
