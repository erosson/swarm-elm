module Page.MtxHistory exposing (Model, Msg(..), create, subscriptions, toSession, update, view)

import Cooldown exposing (Cooldown)
import DateFormat
import Duration exposing (Duration)
import Game exposing (Game)
import Game.MtxHistory as MtxHistory exposing (MtxHistory)
import Game.OfflineSync as OfflineSync
import GameData exposing (GameData)
import GameData.Mtx as Mtx exposing (Mtx)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Decode as D
import Json.Encode
import Kongregate.Inventory
import Markdown
import Maybe.Extra
import PlayFab
import PlayFab.Error
import PlayFab.Inventory
import PlayFab.TitleId
import PlayFab.User as User exposing (User)
import Ports.Kongregate as Ports
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Session exposing (Session)
import Session.Iframe as Iframe exposing (Iframe)
import Session.PaypalEnv as PaypalEnv exposing (PaypalEnv)
import Set exposing (Set)
import Time exposing (Posix)
import View.Error
import View.Icon
import View.Lang as T
import View.NotReady
import View.OfflineSync
import View.TabNav


type Model
    = Model InventoryResponse Session


type InventoryResponse
    = PlayFabInventory (RemoteData PlayFab.Error.Error PlayFab.Inventory.Inventory)
    | KongregateInventory (RemoteData String Kongregate.Inventory.Inventory)


type Msg
    = GotTabNavMsg View.TabNav.Msg
    | GotSession Session
    | GotPlayFabInventory (Result PlayFab.Error.Error PlayFab.Inventory.Inventory)
    | GotKongregateInventory { success : Bool, data : Json.Encode.Value }


create : Session -> ( Model, Cmd Msg )
create session =
    case Session.user session of
        RemoteData.Success (Just user) ->
            case Session.iframe session of
                Iframe.WWW ->
                    ( Model (PlayFabInventory RemoteData.Loading) session
                    , PlayFab.inventory (PlayFab.TitleId.fromSession session) user GotPlayFabInventory
                    )

                Iframe.Kongregate _ ->
                    ( Model (KongregateInventory RemoteData.Loading) session
                    , Ports.kongregateRequestUserItemListReq ()
                    )

        _ ->
            case Session.iframe session of
                Iframe.WWW ->
                    ( Model (PlayFabInventory RemoteData.NotAsked) session, Cmd.none )

                Iframe.Kongregate _ ->
                    ( Model (KongregateInventory RemoteData.NotAsked) session, Cmd.none )


toSession : Model -> Session
toSession (Model _ s) =
    s


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model invReq session) as model) =
    case msg of
        GotTabNavMsg submsg ->
            View.TabNav.update submsg session |> Tuple.mapFirst (Model invReq)

        GotSession s ->
            case invReq of
                PlayFabInventory RemoteData.NotAsked ->
                    create s

                KongregateInventory RemoteData.NotAsked ->
                    create s

                _ ->
                    ( Model invReq s, Cmd.none )

        GotPlayFabInventory invRes ->
            -- Try to add missing mtx on load
            let
                session1 =
                    case ( invRes, Session.game session ) of
                        ( Ok inv, RemoteData.Success game ) ->
                            Session.setGame False (Game.updatePlayFabMtxHistory inv game) session

                        _ ->
                            session
            in
            ( Model (PlayFabInventory (RemoteData.fromResult invRes)) session1, Cmd.none )

        GotKongregateInventory json ->
            let
                invRes =
                    D.decodeValue Kongregate.Inventory.decoder json.data
                        |> Result.mapError D.errorToString

                -- Try to add missing mtx on load
                session1 =
                    case ( invRes, Session.game session ) of
                        ( Ok inv, RemoteData.Success game ) ->
                            Session.setGame False (Game.updateKongMtxHistory inv game) session

                        _ ->
                            session
            in
            ( Model (KongregateInventory (RemoteData.fromResult invRes)) session1, Cmd.none )


subscriptions : Sub Msg
subscriptions =
    Ports.kongregateRequestUserItemListRes GotKongregateInventory



-- VIEW


view : Model -> Game -> List (Html Msg)
view ((Model _ session) as model) game =
    (View.TabNav.viewFromGame Nothing session game
        |> List.map (H.map GotTabNavMsg)
    )
        ++ viewBody model game


viewBody : Model -> Game -> List (Html Msg)
viewBody ((Model _ session) as model) game =
    let
        lang =
            T.gameToLang session game
    in
    case Session.userState session of
        RemoteData.NotAsked ->
            []

        RemoteData.Loading ->
            [ View.Icon.loading ]

        RemoteData.Failure err ->
            [ p [] [ text <| PlayFab.Error.toString err ] ]

        RemoteData.Success Nothing ->
            [ p [] [ lang.html T.ViewUserAnonymous ]
            , p []
                [ div [] [ a [ class "btn btn-link", Route.href (Session.query session) Route.Login ] [ lang.html T.ViewUserAnonymousLogin ] ]
                , div [] [ a [ class "btn btn-link", Route.href (Session.query session) Route.Register ] [ lang.html T.ViewUserAnonymousRegister ] ]
                ]
            ]

        RemoteData.Success (Just ustate) ->
            viewReady model game ustate


viewReady : Model -> Game -> Session.UserState -> List (Html Msg)
viewReady (Model invReq session) game ({ user } as ustate) =
    let
        lang =
            T.gameToLang session game

        userId =
            User.idToString <| User.id user

        titleId =
            PlayFab.TitleId.fromSession session
    in
    [ h1 [] [ lang.html T.ViewMtxHistoryTitle ]
    , p [] [ text <| User.displayName (Session.iframe session) user, text " #", text userId ]
    , case invReq of
        PlayFabInventory req ->
            case req of
                RemoteData.Success serverItems ->
                    div [] <| viewPlayfabItems session lang <| MtxHistory.playfabInventoryStatus serverItems <| Game.mtxHistory game

                RemoteData.Failure err ->
                    div [ class "alert alert-danger" ] [ text <| PlayFab.Error.toString err ]

                _ ->
                    View.Icon.loading

        KongregateInventory req ->
            case req of
                RemoteData.Success serverItems ->
                    div [] <| viewKongItems session lang <| MtxHistory.kongInventoryStatus serverItems <| Game.mtxHistory game

                RemoteData.Failure err ->
                    div [ class "alert alert-danger" ] [ text err ]

                _ ->
                    View.Icon.loading
    ]


viewKongItems : Session -> T.Lang msg -> List MtxHistory.KongStatus -> List (Html msg)
viewKongItems session lang items =
    [ table [ property "border" <| Json.Encode.string "1" ]
        (tr []
            [ th [] [ lang.html T.ViewMtxHistoryColKongId ]
            , th [] [ lang.html T.ViewMtxHistoryColOrder ]
            , th [] [ lang.html T.ViewMtxHistoryColAdded ]
            ]
            :: List.map (viewKongItem session lang) items
        )
    ]


viewKongItem : Session -> T.Lang msg -> MtxHistory.KongStatus -> Html msg
viewKongItem session lang status =
    case status of
        MtxHistory.ServerOnly item ->
            viewKongServerItem session lang False item

        MtxHistory.Synced item ->
            viewKongServerItem session lang True item

        MtxHistory.ClientOnly id ->
            tr []
                [ td [] [ text <| id ]
                , td [] [ text "???" ]
                , td [] [ View.Icon.fas "check", text " ", lang.html T.ViewMtxHistoryAddedYes ]
                ]


viewKongServerItem : Session -> T.Lang msg -> Bool -> Kongregate.Inventory.Item -> Html msg
viewKongServerItem session lang isSynced item =
    tr []
        [ td [] [ text <| Kongregate.Inventory.idToString item.id ]
        , td [ title <| Mtx.idToString item.identifier ] [ text item.name ]
        , if isSynced then
            td [] [ View.Icon.fas "check", text " ", lang.html T.ViewMtxHistoryAddedYes ]

          else
            td [] [ View.Icon.fas "exclamation-triangle", text " ", lang.html T.ViewMtxHistoryAddedNo ]
        ]


viewPlayfabItems : Session -> T.Lang msg -> List MtxHistory.PlayFabStatus -> List (Html msg)
viewPlayfabItems session lang items =
    [ table [ property "border" <| Json.Encode.string "1" ]
        (tr []
            [ th [] [ lang.html T.ViewMtxHistoryColDate ]
            , th [] [ lang.html T.ViewMtxHistoryColPlayFabId ]
            , th [] [ lang.html T.ViewMtxHistoryColOrder ]
            , th [] [ lang.html T.ViewMtxHistoryColPayPalTx ]
            , th [] [ lang.html T.ViewMtxHistoryColAdded ]
            ]
            :: List.map (viewPlayfabItem session lang) items
        )
    ]


viewPlayfabItem : Session -> T.Lang msg -> MtxHistory.PlayFabStatus -> Html msg
viewPlayfabItem session lang status =
    case status of
        MtxHistory.ServerOnly item ->
            viewPlayfabServerItem session lang False item

        MtxHistory.Synced item ->
            viewPlayfabServerItem session lang True item

        MtxHistory.ClientOnly id ->
            tr []
                [ td [] [ text "???" ]
                , td [] [ text id ]
                , td [] [ text "???" ]
                , td [] [ text "???" ]
                , td [] [ View.Icon.fas "check", text " ", lang.html T.ViewMtxHistoryAddedYes ]
                ]


dateFormatter : Posix -> String
dateFormatter =
    -- https://package.elm-lang.org/packages/ryannhg/date-format/latest/DateFormat
    DateFormat.format
        [ DateFormat.yearNumber
        , DateFormat.text " "
        , DateFormat.monthNameAbbreviated
        , DateFormat.text " "
        , DateFormat.dayOfMonthFixed
        , DateFormat.text ", "
        , DateFormat.hourMilitaryFixed
        , DateFormat.text ":"
        , DateFormat.minuteFixed
        , DateFormat.text " UTC"
        ]
        Time.utc


viewPlayfabServerItem : Session -> T.Lang msg -> Bool -> PlayFab.Inventory.Item -> Html msg
viewPlayfabServerItem session lang synced item =
    let
        paypalUrl =
            case Session.paypalEnv session of
                PaypalEnv.Production ->
                    "https://www.paypal.com/activity/payment/" ++ PlayFab.Inventory.paypalTxnId item

                _ ->
                    "https://www.sandbox.paypal.com/activity/payment/" ++ PlayFab.Inventory.paypalTxnId item
    in
    tr []
        [ td [] [ text <| dateFormatter item.purchaseDate ]
        , td [] [ text <| PlayFab.Inventory.instanceIdToString item.instanceId ]
        , td [ title <| PlayFab.Inventory.itemIdToString item.itemId ] [ text item.displayName ]
        , td [] [ a [ target "_blank", href paypalUrl ] [ text <| PlayFab.Inventory.paypalTxnId item ] ]
        , td []
            (if synced then
                [ View.Icon.fas "check", text " ", lang.html T.ViewMtxHistoryAddedYes ]

             else
                [ View.Icon.fas "exclamation-triangle", text " ", lang.html T.ViewMtxHistoryAddedNo ]
            )
        ]
