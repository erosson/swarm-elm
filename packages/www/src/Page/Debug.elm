module Page.Debug exposing (Model, Msg(..), init, toSession, update, view)

{-| My debugging tools.

Unlike most other pages, this one won't be internationalized (View.Lang) because
I'm the only one using it and I speak English.

-}

import Dict exposing (Dict)
import Fools
import Game exposing (Game)
import GameData exposing (GameData)
import GameData.Unit as Unit exposing (Unit)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as D
import Json.Encode as E
import Maybe.Extra
import PlayFab.TitleId
import PlayFab.User as User exposing (User)
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Route.Config as Config exposing (Config)
import Route.Feature as Feature exposing (Feature)
import Session exposing (Session)
import Session.Environment as Environment
import Session.Iframe as Iframe
import Session.Iframe.Kongregate as Kongregate
import Session.PaypalEnv as PaypalEnv
import Session.ServiceWorker as ServiceWorker
import Time exposing (Posix)
import View.Header
import View.Icon
import View.NotReady


type Model
    = Model Data


type alias Data =
    { session : Session
    , editorText : Maybe String
    , editorStatus : RemoteData String ()
    }


init : Session -> ( Model, Cmd msg )
init session0 =
    let
        ( session, cmd ) =
            session0 |> Session.grantAchievement "debug"
    in
    ( Model
        { session = session
        , editorText = Nothing
        , editorStatus = RemoteData.NotAsked
        }
    , cmd
    )


unwrap : Model -> Data
unwrap (Model m) =
    m


toSession : Model -> Session
toSession =
    unwrap >> .session


type Msg
    = GotSession Session
    | FakedServiceWorkerEvent E.Value
    | SaveStateInput String
    | SaveStateConfirmed
    | PushAchievement
    | FakedSessionError
    | FakedBrokenGamedata
    | FakedBrokenGameRead
    | FakedBrokenGameJson


update : Msg -> Model -> ( Model, Cmd Msg )
update msg (Model model) =
    case msg of
        GotSession s ->
            ( Model { model | session = s }, Cmd.none )

        FakedServiceWorkerEvent json ->
            case D.decodeValue ServiceWorker.decoder json of
                Err err ->
                    ( Model { model | session = Session.pushError (D.errorToString err) model.session }, Cmd.none )

                Ok event ->
                    ( Model { model | session = Session.setServiceWorker event model.session }, Cmd.none )

        FakedBrokenGamedata ->
            let
                s =
                    model.session
            in
            ( Model
                { model
                    | session =
                        Session.create
                            { environment = Session.environment s |> Environment.toString
                            , hostname = Session.hostname s
                            , version = Session.version s
                            , changelog = Session.changelog s
                            }
                            (Session.nav s)
                            (Err "debug: gamedata test error")
                            Nothing
                }
            , Cmd.none
            )

        FakedBrokenGameRead ->
            ( Model { model | session = model.session |> Session.setPersistRead (Err "debug: game-read test error") }, Cmd.none )

        FakedBrokenGameJson ->
            let
                json : E.Value
                json =
                    E.object [ ( "debug", E.string "game-json test error" ) ]
            in
            ( Model { model | session = model.session |> Session.setPersistRead (Ok <| Just json) }, Cmd.none )

        FakedSessionError ->
            ( Model { model | session = model.session |> Session.pushError "debug: session test error" }, Cmd.none )

        SaveStateInput str ->
            ( Model { model | editorText = Just str }, Cmd.none )

        SaveStateConfirmed ->
            let
                str =
                    model.editorText |> Maybe.withDefault ""

                gameData =
                    Session.gameData model.session |> Result.toMaybe

                now =
                    Session.now model.session

                game1 : RemoteData String Game
                game1 =
                    case Maybe.map2 (\gd t -> D.decodeString (Game.decoder gd t) str) gameData now of
                        Nothing ->
                            RemoteData.Loading

                        Just (Err err) ->
                            RemoteData.Failure <| D.errorToString err

                        Just (Ok g) ->
                            RemoteData.succeed g
            in
            ( Model
                { model
                    | editorStatus = game1 |> RemoteData.map (always ())
                    , session =
                        case game1 of
                            RemoteData.Success g ->
                                model.session |> Session.setGame False g

                            _ ->
                                model.session
                }
            , Cmd.none
            )

        PushAchievement ->
            case
                ( Session.game model.session
                , Session.gameData model.session
                    |> Result.andThen
                        (GameData.achievements
                            >> List.head
                            >> Result.fromMaybe "no achievements?"
                        )
                )
            of
                ( RemoteData.Success game, Ok achievement ) ->
                    let
                        game1 =
                            Game.debugNewAchievement (Session.safeNow game model.session) achievement game
                    in
                    ( Model { model | session = Session.setGame False game1 model.session }, Cmd.none )

                _ ->
                    ( Model model, Cmd.none )


view : Model -> Game -> List (Html Msg)
view (Model m) game =
    let
        role =
            m.session |> Session.userRole
    in
    [ h3 [] [ text "Debugging Tools" ]
    , p [] [ text "Here are some of the developer's debugging tools! They're now visible even in production - have fun." ]
    , p [] <|
        case role of
            RemoteData.Success User.AdminRole ->
                [ a [ target "_blank", href "https://youtu.be/AJKx1XDBqNo?t=12" ] [ text "You're an admin! " ]
                , text "You can edit your saved game below, too."
                ]

            RemoteData.Loading ->
                [ View.Icon.loading
                ]

            _ ->
                [ text "The cheaty admin tools like save-editing and time-skipping aren't here ("
                , a [ Route.href (Session.query m.session) Route.Login ] [ text "unless you're an admin" ]
                , text ")."
                ]
    ]
        ++ ([ viewFlagLinks m.session game
            , viewStaticData m.session game
            , viewEventDebug m.session game
            , viewServiceWorkerDebug m.session game
            , case role of
                RemoteData.Success User.AdminRole ->
                    viewGameStateEditor (Model m) game

                _ ->
                    viewGameState m.session game
            ]
                |> List.map (div [ class "card" ])
           )


viewFlagRow : String -> String -> Dict String String -> Feature -> Html msg
viewFlagRow desc title_ query feature =
    tr []
        [ td [ title <| desc ++ " flag: " ++ title_ ] [ text desc ]
        , td []
            [ a [ class "card-link", Route.href (Feature.toggle feature query) Route.Debug ]
                [ text "?", text <| Feature.toString feature ]
            ]
        , td [] <|
            if Feature.isActive feature query then
                [ span [ class "active" ] [ View.Icon.fas "check" ] ]

            else
                [ span [ class "inactive" ] [ View.Icon.fas "times" ] ]
        , td [] []
        ]


viewConfigRow : String -> String -> Dict String String -> ( Config, String ) -> Html msg
viewConfigRow desc title_ query ( config, fancyval ) =
    tr []
        [ td [ title <| desc ++ ": " ++ title_ ] [ text desc ]
        , td []
            [ a [ class "card-link", Route.href (Config.setNextEnum config query) Route.Debug ]
                [ text "?", text <| Config.toString config ]
            ]
        , td []
            [ case Config.get config query of
                Nothing ->
                    b [ class "empty", title "(none)" ] [ text "∅" ]

                Just s ->
                    code [] [ text s ]
            ]
        , td [] [ code [] [ text fancyval ] ]
        ]


viewFlagLinks : Session -> Game -> List (Html msg)
viewFlagLinks session game =
    let
        query =
            Session.query session
    in
    [ h5 [ class "card-header" ] [ text "Flags 🏁🏴\u{200D}☠️🏳️\u{200D}🌈🚩" ]
    , div [ class "card-body" ]
        [ table [ class "flags" ]
            ([ th [] [ text "Flag type" ]
             , th [] [ text "Querystring" ]
             , th [] [ text "" ]
             , th [] [ text "Value" ]
             ]
                ++ List.map (viewFlagRow "feature" "temporary" query) Feature.temps
                ++ List.map (viewConfigRow "debug" "permanent" query)
                    [ ( Config.Fools, Fools.status session |> Fools.toString )
                    ]
                ++ List.map (viewFlagRow "debug" "permanent" query) Feature.debugs
                ++ (case session |> Session.userRole of
                        RemoteData.Success User.AdminRole ->
                            []
                                ++ List.map (viewConfigRow "env" "permanent, hidden from non-admins" query)
                                    [ ( Config.PlayFab, PlayFab.TitleId.fromSession session |> PlayFab.TitleId.toString )
                                    , ( Config.Environment, Session.environment session |> Environment.toString )
                                    , ( Config.Iframe, Session.iframe session |> Iframe.toString )
                                    , ( Config.PaypalEnv, Session.paypalEnv session |> PaypalEnv.toString )
                                    , ( Config.Hostname, Session.hostname session )
                                    ]
                                ++ List.map (viewFlagRow "env" "permanent flag, hidden from non-admins" query)
                                    [ Feature.Kongregate
                                    ]

                        _ ->
                            []
                   )
            )
        ]
    ]


nextFlagStr : List String -> Maybe String -> Maybe String
nextFlagStr values value =
    let
        rvalues =
            List.reverse values

        valuesMap : Dict String (Maybe String)
        valuesMap =
            List.map2 Tuple.pair rvalues (Nothing :: List.map Just rvalues)
                |> Dict.fromList
    in
    case value of
        Nothing ->
            List.head values

        Just v ->
            Dict.get v valuesMap |> Maybe.andThen identity



--viewAchievementDebug : Session -> Game -> List (Html Msg)
--viewAchievementDebug session game =
--    [ div [ title "Alternately, \"every video game ever\"" ] [ text "Achievement simulator" ]
--    , case Dict.get "drone1" model.game.gameData.achievesByName of
--        Just achieve ->
--            button [ class "btn btn-info", onClick <| ReadyModel.AchievementToastTest achieve ] [ text <| "toast " ++ achieve.name ]
--
--        Nothing ->
--            button [ class "btn btn-info disabled", disabled True ] [ text <| "achieve fetch failed" ]
--    ]


viewStaticData : Session -> Game -> List (Html Msg)
viewStaticData session game =
    [ h5 [ class "card-header" ] [ text "Static data" ]
    , div [ class "card-body" ]
        [ a [ class "card-link", target "_blank", href "/gamedata.ods" ] [ text "gamedata.ods (libreoffice spreadsheet)" ]
        , a [ class "card-link", target "_blank", href "/gamedata.json" ] [ text "gamedata.json" ]
        , a [ class "card-link", target "_blank", href "/version.json" ] [ text "version.json" ]
        , a [ class "card-link", target "_blank", href "/lang/en-us.json" ] [ text "lang/en-us.json" ]
        , div [] <|
            case Session.iframe session of
                Iframe.WWW ->
                    []

                Iframe.Kongregate kong ->
                    [ div []
                        [ text "kongregate_api: "
                        , code []
                            (case kong.api of
                                RemoteData.NotAsked ->
                                    [ text "notasked" ]

                                RemoteData.Loading ->
                                    [ View.Icon.loading ]

                                RemoteData.Failure err ->
                                    [ text "failure: ", text err ]

                                RemoteData.Success () ->
                                    [ text "ready" ]
                            )
                        ]
                    , div []
                        [ text "kongregate_user: "
                        , code [] <|
                            case kong.user of
                                Nothing ->
                                    [ text "(none)" ]

                                Just user ->
                                    [ Kongregate.userIdToString user.userId
                                    , user.username
                                    , Kongregate.gameAuthTokenToString user.gameAuthToken
                                    , if user.isGuest then
                                        "guest"

                                      else
                                        "not-guest"
                                    ]
                                        |> List.intersperse ", "
                                        |> List.map text
                        ]
                    ]
        ]
    ]


viewGameState : Session -> Game -> List (Html Msg)
viewGameState session game =
    [ h5 [ class "card-header" ] [ text "Game state" ]
    , div [ class "card-body" ]
        [ pre [] [ game |> Game.encoder (Session.safeNow game session) |> E.encode 2 |> text ]
        ]
    ]


viewGameStateEditor : Model -> Game -> List (Html Msg)
viewGameStateEditor (Model m) game =
    let
        now =
            Session.safeNow game m.session

        pnow =
            Time.posixToMillis now

        content =
            case m.editorText of
                Nothing ->
                    game |> Game.encoder now |> E.encode 2

                Just "" ->
                    game |> Game.encoder now |> E.encode 2

                Just str ->
                    str
    in
    [ h5 [ class "card-header" ] [ text "Game state editor" ]
    , div [ class "card-body" ]
        [ p []
            [ text "the time is now "
            , code [] [ text <| String.fromInt (pnow - modBy 5000 pnow) ]
            , text " + "
            , text <| String.fromInt (modBy 5000 pnow)
            ]
        , textarea
            [ class "alert"
            , style "width" "100%"
            , rows (content |> String.lines |> List.length)
            , classList
                [ ( "alert-success", RemoteData.isSuccess m.editorStatus )
                , ( "alert-danger", RemoteData.isFailure m.editorStatus )
                , ( "alert-secondary", not (RemoteData.isSuccess m.editorStatus || RemoteData.isFailure m.editorStatus) )
                ]
            , onInput SaveStateInput
            , onBlur SaveStateConfirmed
            ]
            [ text content ]
        , pre [] <| View.NotReady.viewRemoteInline <| RemoteData.map (always []) m.editorStatus
        ]
    ]


viewServiceWorkerDebug : Session -> Game -> List (Html Msg)
viewServiceWorkerDebug session game =
    [ h5 [ class "card-header" ] [ text "Service worker event simulator" ]
    , div [ class "card-body" ]
        [ serviceWorkerStatusButton "update-found" [ class "btn-info" ]
        , serviceWorkerStatusButton "update-installed" [ class "btn-info" ]
        , serviceWorkerStatusButton "offline-installed" [ class "btn-info" ]
        , serviceWorkerButton "error" [ class "btn-warning" ] <| E.object [ ( "status", E.string "error" ), ( "error", E.string "debug tools test error" ) ]
        ]
    ]


viewEventDebug : Session -> Game -> List (Html Msg)
viewEventDebug session game =
    [ h5 [ class "card-header" ] [ text "Event simulator" ]
    , div [ class "card-body" ]
        [ div [] [ button [ class "btn btn-info", onClick PushAchievement ] [ text "debug achievement toast" ] ]
        , div [] [ button [ class "btn btn-warning", onClick FakedSessionError ] [ text "debug session error" ], text " generic ignorable top-level error message" ]
        , div [] [ button [ class "btn btn-danger", onClick FakedBrokenGamedata ] [ text "debug broken gamedata" ], text " spreadsheet data, including Lang/i18n, is bogus" ]
        , div [] [ button [ class "btn btn-danger", onClick FakedBrokenGameRead ] [ text "debug broken game-read" ], text " player progress isn't json, or isn't accessible" ]
        , div [] [ button [ class "btn btn-danger", onClick FakedBrokenGameJson ] [ text "debug broken game-json" ], text " player progress json is bogus" ]
        ]
    ]


serviceWorkerStatusButton : String -> List (Html.Attribute Msg) -> Html Msg
serviceWorkerStatusButton status attrs =
    serviceWorkerButton status attrs <| E.object [ ( "status", E.string status ) ]


serviceWorkerButton : String -> List (Html.Attribute Msg) -> E.Value -> Html Msg
serviceWorkerButton label attrs event =
    button (attrs ++ [ class "btn", onClick <| FakedServiceWorkerEvent <| event ]) [ text label ]
