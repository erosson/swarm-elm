module Page.Spell exposing (Model, Msg(..), create, keyBindings, toSession, update, view, visibleOrders)

import Decimal exposing (Decimal)
import Game exposing (Game)
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import KeyCombo
import Maybe.Extra
import Page.Unit
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Route.Feature as Feature
import Select
import Session exposing (Session)
import Time exposing (Posix)
import View.BuyN
import View.Description
import View.DescriptionVars
import View.Device
import View.Icon
import View.ItemDebug
import View.Lang as T exposing (Lang)
import View.NotReady
import View.TabNav
import View.UnitSidebar


type Model
    = Model String String Session


create : String -> String -> Session -> ( Model, Cmd Msg )
create tab spell session0 =
    case Session.game session0 of
        RemoteData.Success game0 ->
            let
                ( game, cmd ) =
                    game0 |> Game.setLastUnitByTab tab (Just spell) |> Session.persistWrite session0
            in
            ( Model tab spell (Session.setGame False game session0), cmd )

        _ ->
            ( Model tab spell session0, Cmd.none )


toSession : Model -> Session
toSession (Model _ _ s) =
    s


type Msg
    = GotSession Session
    | ClickedRoute Route
    | GotBuyNMsg View.BuyN.Msg
    | GotTabNavMsg View.TabNav.Msg
    | ClickedBuySpell


keyBindings : Model -> Result String (KeyCombo.Bindings Msg)
keyBindings (Model tabSlug spellSlug session) =
    Page.Unit.itemKeyBindings tabSlug spellSlug session ClickedBuySpell ClickedRoute


visibleOrders : Model -> List Order
visibleOrders (Model tabName spellName session) =
    case Session.game session of
        RemoteData.Success game ->
            let
                snapshot =
                    Session.safeUnits game session
            in
            case Select.spellBySlug spellName game snapshot of
                Just spell ->
                    let
                        items =
                            [ Item.Spell spell ]
                    in
                    List.filterMap (Order.fromBuyN game snapshot >> Result.toMaybe) items

                _ ->
                    []

        _ ->
            []


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model tabName spellName session) as model) =
    case msg of
        GotSession s ->
            ( Model tabName spellName s, Cmd.none )

        ClickedRoute route ->
            ( Model tabName spellName session, Route.pushUrl (Session.nav session) (Session.query session) route )

        GotBuyNMsg submsg ->
            case Session.game session of
                RemoteData.Success game ->
                    View.BuyN.update (Route.Spell tabName spellName) submsg game session
                        |> Tuple.mapBoth
                            (Model tabName spellName)
                            (Cmd.map GotBuyNMsg)

                _ ->
                    ( model, Cmd.none )

        GotTabNavMsg submsg ->
            View.TabNav.update submsg session
                |> Tuple.mapFirst (Model tabName spellName)

        ClickedBuySpell ->
            case Session.game session of
                RemoteData.Success game ->
                    let
                        snapshot =
                            Session.safeUnits game session
                    in
                    case Select.spellBySlug spellName game snapshot of
                        Just spell ->
                            case Order.fromBuyN game snapshot (Item.Spell spell) of
                                Ok order ->
                                    View.BuyN.update (Route.Spell tabName spellName) (View.BuyN.ClickedBuy order) game session
                                        |> Tuple.mapBoth
                                            (Model tabName spellName)
                                            (Cmd.map GotBuyNMsg)

                                _ ->
                                    ( model, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )


view : Model -> Game -> List (Html Msg)
view (Model tabSlug spellSlug session) game =
    View.NotReady.viewResult session (Game.gameData game) <|
        let
            snap =
                Session.safeUnits game session
        in
        case
            ( Select.tabBySlug tabSlug game snap
            , Select.spellBySlug spellSlug game snap
            )
        of
            ( Just stab, Just spell ) ->
                (View.TabNav.view (Maybe.map .id <| Select.unwrapTab stab) session game snap
                    |> List.map (H.map GotTabNavMsg)
                )
                    ++ (View.UnitSidebar.wrap ClickedRoute stab (Item.Spell spell) session game snap <|
                            viewSpell stab session game snap spell
                       )
                    ++ View.ItemDebug.view session game snap [ Item.Spell spell ]
                    |> Ok

            ( Nothing, _ ) ->
                Err "no such tab"

            ( _, Nothing ) ->
                Err "no such spell"


viewSpell : Select.STab -> Session -> Game -> UnitsSnapshot -> Spell -> List (Html Msg)
viewSpell stab session game snapshot spell =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabSlug =
            Select.tabToSlug stab

        order =
            Order.fromBuyN game snapshot (Item.Spell spell)
    in
    [ h3 []
        [ if Feature.isActive Feature.MobileUI query then
            View.Device.onlyMobileInline [ a [ Route.href query <| Route.Tab tabSlug ] [ View.Icon.fas "bars", text " " ] ]

          else
            span [] []
        , lang.html <| T.ViewSpellHeader spell.id
        ]
    , div
        [ class "desc-icon-resource"
        , class <| "desc-icon-" ++ Spell.name spell
        , class <| "icon-" ++ Spell.name spell
        ]
        []
    , case View.Description.view session game (Item.Spell spell) of
        Just html ->
            html

        Nothing ->
            lang.html <| T.SpellDesc (View.DescriptionVars.evalVar game) spell.id
    , View.BuyN.view session game snapshot order { tabindex = 1 } |> Maybe.withDefault (div [] []) |> H.map GotBuyNMsg
    , View.BuyN.viewHelpModal lang
    ]
