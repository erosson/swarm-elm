module Page.Unit exposing (Model, Msg(..), create, itemKeyBindings, keyBindings, toSession, update, view, visibleOrders)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.BuyN
import Game.HatchableRate
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import Game.Watched as Watched exposing (Watched)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import KeyCombo
import List.Extra
import Maybe.Extra
import Parser.Stat
import RemoteData exposing (RemoteData)
import Result.Extra
import Route exposing (Route)
import Route.Feature as Feature
import Select
import Session exposing (Session)
import Time exposing (Posix)
import View.BuyN
import View.Description
import View.DescriptionVars
import View.Device
import View.Icon
import View.ItemDebug
import View.Lang as T exposing (Lang)
import View.NotReady
import View.TabNav
import View.UnitSidebar
import View.Watcher


type Model
    = Model String String { n : Maybe String } Session


create : String -> String -> { n : Maybe String } -> Session -> ( Model, Cmd Msg )
create tab unit ns session0 =
    case Session.game session0 of
        RemoteData.Success game0 ->
            let
                ( game, cmd ) =
                    game0 |> Game.setLastUnitByTab tab (Just unit) |> Session.persistWrite session0
            in
            ( Model tab unit ns (Session.setGame False game session0), cmd )

        _ ->
            ( Model tab unit ns session0, Cmd.none )


toSession : Model -> Session
toSession (Model _ _ _ s) =
    s


type Msg
    = GotSession Session
    | ClickedRoute Route
    | GotBuyNMsg View.BuyN.Msg
    | GotTabNavMsg View.TabNav.Msg
    | GotWatcherMsg View.Watcher.Msg
    | ClickedBuyUnit
    | VelocityExpanded
    | VelocityCollapsed


routeItem : String -> Item -> Route
routeItem tabSlug item =
    case item of
        Item.Spell s ->
            Route.Spell tabSlug s.slug

        Item.Unit u ->
            Route.unit tabSlug u.slug

        Item.Upgrade _ ->
            -- should never happen
            Route.Home


keyBindings : Model -> Result String (KeyCombo.Bindings Msg)
keyBindings (Model tabSlug unitSlug ns session) =
    itemKeyBindings tabSlug unitSlug session ClickedBuyUnit ClickedRoute


itemKeyBindings : String -> String -> Session -> msg -> (Route -> msg) -> Result String (KeyCombo.Bindings msg)
itemKeyBindings tabSlug itemSlug session clickedBuyUnitMsg clickedRouteMsg =
    case Session.game session of
        RemoteData.Success game ->
            let
                snap =
                    Game.units game
            in
            case
                ( Select.tabBySlug tabSlug game snap
                , Select.itemBySlug itemSlug game snap
                )
            of
                ( Just stab, Just item ) ->
                    let
                        units =
                            Select.unitsByTab stab game snap

                        spells =
                            Select.spellsByTab stab game snap

                        items =
                            List.map Item.Spell spells ++ List.map Item.Unit units

                        itemIndex : Maybe Int
                        itemIndex =
                            List.Extra.findIndex (\i -> Item.name i == Item.name item) items

                        next : Maybe Item
                        next =
                            itemIndex |> Maybe.andThen (\i -> List.Extra.getAt (i + 1) items)

                        prev : Maybe Item
                        prev =
                            itemIndex |> Maybe.andThen (\i -> List.Extra.getAt (i - 1) items)
                    in
                    (List.filterMap identity
                        [ prev |> Maybe.map (\u -> KeyCombo.bind "alt+ArrowUp" "nav.unit.prev" <| clickedRouteMsg <| routeItem tabSlug u)
                        , next |> Maybe.map (\u -> KeyCombo.bind "alt+ArrowDown" "nav.unit.next" <| clickedRouteMsg <| routeItem tabSlug u)
                        , KeyCombo.bind "alt+b" "unit.buy" clickedBuyUnitMsg |> Just
                        ]
                        ++ []
                    )
                        |> KeyCombo.bindings

                _ ->
                    Err "no such tab/unit"

        _ ->
            Err "game not loaded yet"


visibleOrders : Model -> List Order
visibleOrders (Model tabName unitName ns session) =
    case Session.game session of
        RemoteData.Success game0 ->
            let
                snapshot =
                    Session.safeUnits game0 session
            in
            case Select.unitBySlug unitName game0 snapshot of
                Just ( unit, _ ) ->
                    let
                        game =
                            case ns.n of
                                Nothing ->
                                    game0

                                Just buyN ->
                                    game0 |> Game.setBuyN (Item.Unit unit) buyN

                        upgrades =
                            Select.upgradesByUnitId unit.id game snapshot

                        items =
                            Item.Unit unit :: List.map Item.Upgrade upgrades
                    in
                    List.filterMap (Order.fromBuyN game snapshot >> Result.toMaybe) items

                _ ->
                    []

        _ ->
            []


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model tabName unitName ns session) as model) =
    case msg of
        GotSession s ->
            ( Model tabName unitName ns s, Cmd.none )

        ClickedRoute route ->
            ( Model tabName unitName ns session, Route.pushUrl (Session.nav session) (Session.query session) route )

        GotBuyNMsg submsg ->
            case Session.game session of
                RemoteData.Success game ->
                    View.BuyN.update (Route.Unit tabName unitName ns) submsg game session
                        |> Tuple.mapBoth
                            (Model tabName unitName ns)
                            (Cmd.map GotBuyNMsg)

                _ ->
                    ( model, Cmd.none )

        GotTabNavMsg submsg ->
            View.TabNav.update submsg session
                |> Tuple.mapFirst (Model tabName unitName ns)

        GotWatcherMsg submsg ->
            case Session.game session of
                RemoteData.Success game ->
                    View.Watcher.update session submsg game
                        |> Tuple.mapBoth
                            (\g -> Model tabName unitName ns (Session.setGame False g session))
                            (Cmd.map GotWatcherMsg)

                _ ->
                    ( model, Cmd.none )

        ClickedBuyUnit ->
            case Session.game session of
                RemoteData.Success game ->
                    let
                        snapshot =
                            Session.safeUnits game session
                    in
                    case Select.unitBySlug unitName game snapshot of
                        Just ( unit, _ ) ->
                            case Order.fromBuyN game snapshot (Item.Unit unit) of
                                Ok order ->
                                    View.BuyN.update (Route.Unit tabName unitName ns) (View.BuyN.ClickedBuy order) game session
                                        |> Tuple.mapBoth
                                            (Model tabName unitName ns)
                                            (Cmd.map GotBuyNMsg)

                                _ ->
                                    ( model, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        VelocityExpanded ->
            ( setVelocityUI Game.FancyVelocityUI model, Cmd.none )

        VelocityCollapsed ->
            ( setVelocityUI Game.HiddenVelocityUI model, Cmd.none )


setVelocityUI : Game.VelocityUI -> Model -> Model
setVelocityUI v ((Model tabName unitName ns session0) as model) =
    case Session.game session0 of
        RemoteData.Success game0 ->
            let
                game =
                    game0 |> Game.setVelocityUI v

                session =
                    session0 |> Session.setGame False game
            in
            Model tabName unitName ns session

        _ ->
            model


view : Model -> Game -> List (Html Msg)
view (Model tabSlug unitSlug ns session) game0 =
    -- This page has a lot going on! In general, my strategy here is to do any
    -- error-handling and monad-shuffling/complex-type-juggling at the top level
    -- - that is, here, in this function - while minimizing the number of
    -- parameters passed to children.
    --
    -- Don't bind things like `lang` or `query` into variables/params here. If
    -- they can't cause errors (and their types prove it), deal with those
    -- details in helper methods like `viewUnit`/`viewSidebar`/etc. This keeps
    -- this method sane, keeps the number of parameters passed to helpers sane,
    -- while avoiding the complexity of errors and monad types in our helpers.
    View.NotReady.viewResult session (Game.gameData game0) <|
        let
            snap =
                Session.safeUnits game0 session
        in
        case
            ( Select.tabBySlug tabSlug game0 snap
            , Select.unitBySlug unitSlug game0 snap
            )
        of
            ( Just stab, Just ( unit, count ) ) ->
                let
                    game =
                        -- apply `?n=xyz` here
                        case ns.n of
                            Nothing ->
                                game0

                            Just buyN ->
                                game0 |> Game.setBuyN (Item.Unit unit) buyN
                in
                (View.TabNav.view (Maybe.map .id <| Select.unwrapTab stab) session game snap
                    |> List.map (H.map GotTabNavMsg)
                )
                    ++ (View.UnitSidebar.wrap ClickedRoute stab (Item.Unit unit) session game snap <|
                            viewUnit stab session game snap unit count
                                ++ viewUpgrades session game snap unit
                       )
                    ++ View.ItemDebug.view session game snap [ Item.Unit unit ]
                    |> Ok

            ( Nothing, _ ) ->
                Err "no such tab"

            ( _, Nothing ) ->
                Err "no such unit"


viewUnit : Select.STab -> Session -> Game -> UnitsSnapshot -> Unit -> Decimal -> List (Html Msg)
viewUnit stab session game snapshot unit count =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session

        tabSlug =
            Select.tabToSlug stab

        production =
            Game.production unit game snapshot

        order =
            Order.fromBuyN game snapshot (Item.Unit unit)

        velocity =
            UnitsSnapshot.velocity unit.id snapshot

        hatchRate =
            Game.HatchableRate.hatchableRate unit game snapshot

        isVelocityUIToggleVisible =
            velocity /= Decimal.zero || Result.Extra.isOk hatchRate
    in
    [ h3 []
        [ if Feature.isActive Feature.MobileUI query then
            View.Device.onlyMobileInline [ a [ Route.href query <| Route.Tab tabSlug ] [ View.Icon.fas "bars", text " " ] ]

          else
            span [] []
        , lang.html <| T.ViewUnitHeader unit.id
        ]
    , View.Watcher.viewUnitWatcher game unit |> H.map GotWatcherMsg
    , div
        [ class "desc-icon-resource"
        , class <| "desc-icon-" ++ Unit.name unit
        , class <| "icon-" ++ Unit.name unit
        ]
        []
    , case View.Description.view session game (Item.Unit unit) of
        Just html ->
            html

        Nothing ->
            lang.html <| T.UnitDesc (View.DescriptionVars.evalVar game) unit.id
    , p [] <|
        case unit.maxCount of
            Nothing ->
                [ lang.html <| T.ViewUnitCount count unit.id
                , if isVelocityUIToggleVisible then
                    case Game.velocityUI game of
                        Game.HiddenVelocityUI ->
                            button [ class "btn btn-link", onClick VelocityExpanded ] [ View.Icon.fas "chevron-down" ]

                        Game.FancyVelocityUI ->
                            button [ class "btn btn-link", onClick VelocityCollapsed ] [ View.Icon.fas "chevron-up" ]

                  else
                    span [] []
                ]

            Just capExpr ->
                viewCappedCount unit count capExpr session game snapshot
    , p [] (production |> Maybe.Extra.unwrap [] (\prod -> [ lang.html <| T.ViewUnitProdEach prod ]))
    , p [] <|
        if count == Decimal.zero then
            []

        else
            production |> Maybe.Extra.unwrap [] (\prod -> [ lang.html <| T.ViewUnitProdTotal prod ])
    , div [] <|
        case Game.velocityUI game of
            Game.FancyVelocityUI ->
                (if velocity == Decimal.zero then
                    []

                 else
                    [ p [] [ lang.html <| T.ViewUnitVelocity unit.id velocity ] ]
                )
                    ++ (case hatchRate of
                            Ok hatch ->
                                [ p [] [ lang.html <| T.ViewUnitHatchableRate unit.id hatch ] ]

                            Err _ ->
                                []
                       )

            _ ->
                []
    , View.BuyN.view session game snapshot order { tabindex = tabindex0 } |> Maybe.withDefault (div [] []) |> H.map GotBuyNMsg
    , View.BuyN.viewHelpModal lang
    ]


{-| The first tabindex used on this page.
-}
tabindex0 =
    2


viewCappedCount : Unit -> Decimal -> Parser.Stat.Expr -> Session -> Game -> UnitsSnapshot -> List (Html msg)
viewCappedCount unit count capExpr session game snapshot =
    let
        lang =
            T.gameToLang session game

        cap =
            Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game) capExpr

        progress =
            if cap == Decimal.zero then
                { percent = 1, duration = Just Duration.zero }

            else
                { percent =
                    Decimal.div count cap |> Decimal.toFloat

                -- (cap - count) / velocity
                , duration =
                    Decimal.div (Decimal.sub cap count) (UnitsSnapshot.velocity unit.id snapshot)
                        |> Decimal.toFloat
                        |> Duration.fromSecs
                        |> Just
                }
    in
    [ View.BuyN.viewProgress progress
    , lang.html <| T.ViewUnitCappedCount count cap unit.id
    ]


viewUpgrades : Session -> Game -> UnitsSnapshot -> Unit -> List (Html Msg)
viewUpgrades session game snapshot unit =
    case
        Select.upgradesByUnitId unit.id game snapshot
            |> List.indexedMap (viewUpgrade session game snapshot)
            |> List.filterMap identity
    of
        [] ->
            []

        upgradesViews ->
            let
                lang =
                    T.gameToLang session game
            in
            [ hr [] []
            , h4 [] [ lang.html <| T.ViewUnitUpgradesTitle ]
            , div [] upgradesViews
            ]


viewUpgrade : Session -> Game -> UnitsSnapshot -> Int -> Upgrade -> Maybe (Html Msg)
viewUpgrade session game snapshot index upgrade =
    let
        lang =
            T.gameToLang session game

        count =
            Game.upgradeCount upgrade.id game

        order =
            Order.fromBuyN game snapshot (Item.Upgrade upgrade)
    in
    case Game.watched (Item.Upgrade upgrade) game of
        Just Watched.Hidden ->
            Just <|
                div [ style "clear" "right", style "opacity" "0.65" ]
                    [ View.Watcher.viewUpgradeWatcher game upgrade |> H.map GotWatcherMsg
                    , div [ class "upgrade-header" ] [ lang.html <| T.ViewUnitUpgradeHeader count upgrade.id ]
                    ]

        _ ->
            case View.BuyN.view session game snapshot order { tabindex = tabindex0 + index } |> Maybe.map (H.map GotBuyNMsg) of
                Nothing ->
                    -- If an upgrade's buyN can't be shown, hide the whole upgrade.
                    -- This can happen when an upgrade's currency isn't yet visible.
                    Nothing

                Just viewBuyN ->
                    Just <|
                        div [ style "clear" "right" ]
                            [ View.Watcher.viewUpgradeWatcher game upgrade |> H.map GotWatcherMsg
                            , div [ class "upgrade-header" ] [ lang.html <| T.ViewUnitUpgradeHeader count upgrade.id ]
                            , case View.Description.view session game (Item.Upgrade upgrade) of
                                Just html ->
                                    html

                                Nothing ->
                                    lang.html <| T.UpgradeDesc (View.DescriptionVars.evalVar game) upgrade.id
                            , viewBuyN
                            ]
