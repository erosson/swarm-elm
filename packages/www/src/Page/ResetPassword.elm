module Page.ResetPassword exposing (Model, Msg(..), create, toSession, update, view)

import GameData exposing (GameData)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Maybe.Extra
import PlayFab
import PlayFab.Error
import PlayFab.TitleId
import PlayFab.User as User exposing (User)
import Ports
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Session exposing (Session)
import View.FormUtil as FormUtil
import View.Icon
import View.Lang as T


type Model
    = Model
        { session : Session
        , email : String
        , response : RemoteData PlayFab.Error.Error ()
        }


create : Session -> Model
create session_ =
    Model
        { session = session_
        , email =
            case session_ |> Session.user of
                RemoteData.Success (Just user) ->
                    user |> User.email |> Maybe.withDefault ""

                _ ->
                    ""
        , response = RemoteData.NotAsked
        }


toSession : Model -> Session
toSession (Model m) =
    m.session



-- VIEW


view : Model -> GameData -> List (Html Msg)
view (Model model) gd =
    let
        lang =
            T.gameDataToLang model.session gd

        error =
            FormUtil.failure model.response

        qs =
            Session.query model.session
    in
    [ h3 [] [ lang.html T.ViewResetPasswordTitle ]
    , H.form [ onSubmit Submit ]
        [ div [ class "form-group" ]
            (label [ for "playfab-login-email" ] [ lang.html T.ViewResetPasswordFieldEmail ]
                :: FormUtil.field (PlayFab.errorField error "Email")
                    input
                    [ type_ "email", id "playfab-login-email", class "form-control", value model.email, onInput InputEmail ]
                    []
            )
        , PlayFab.Error.toMessages error |> FormUtil.error
        , case model.response of
            RemoteData.Loading ->
                button [ type_ "submit", class "btn btn-primary disabled", disabled True ] [ View.Icon.loading, text " ", lang.html T.ViewResetPasswordSubmit ]

            RemoteData.Success _ ->
                button [ type_ "submit", class "btn btn-success" ] [ lang.html T.ViewResetPasswordSubmit ]

            RemoteData.Failure _ ->
                button [ type_ "submit", class "btn btn-danger" ] [ lang.html T.ViewResetPasswordSubmit ]

            RemoteData.NotAsked ->
                button [ type_ "submit", class "btn btn-primary" ] [ lang.html T.ViewResetPasswordSubmit ]
        ]
    , case Session.user model.session of
        RemoteData.Success (Just user) ->
            div [] [ a [ class "btn btn-link", Route.href qs Route.User ] [ text <| User.displayName (Session.iframe model.session) user ] ]

        _ ->
            div []
                [ div [] [ a [ class "btn btn-link", Route.href qs Route.Register ] [ lang.html T.ViewLoginLinkRegister ] ]
                , div [] [ a [ class "btn btn-link", Route.href qs Route.Login ] [ lang.html T.ViewRegisterLinkLogin ] ]
                ]
    ]



-- UPDATE


type Msg
    = GotSession Session
    | Submit
    | InputEmail String
    | Response (Result PlayFab.Error.Error ())


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model model) as model0) =
    case msg of
        GotSession s ->
            ( Model { model | session = s }, Cmd.none )

        InputEmail val ->
            ( Model { model | email = val }, Cmd.none )

        Submit ->
            ( Model { model | response = RemoteData.Loading }
            , PlayFab.resetPassword (PlayFab.TitleId.fromSession model.session) model Response
            )

        Response res ->
            ( Model { model | response = res |> RemoteData.fromResult }, Cmd.none )
