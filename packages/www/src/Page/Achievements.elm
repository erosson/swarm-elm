module Page.Achievements exposing (Msg, toProgdesc, update, view)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Game exposing (Game)
import Game.Achievement
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Achievement as Achievement exposing (Achievement)
import GameData.Item as Item exposing (Item)
import GameData.Unit as Unit exposing (Unit)
import GameData.Unit.Id as UnitId
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Html.Lazy
import Parser.Stat
import Parser.Visibility
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Session exposing (Session)
import Time exposing (Posix)
import View.Icon
import View.Lang as T exposing (Lang)
import View.NotReady
import View.TabNav


type Msg
    = GotTabNavMsg View.TabNav.Msg
    | OnAchievementClick Achievement
    | OnSetOptionsEarned
    | OnSetOptionsUnearned
    | OnSetOptionsMasked
    | OnSetOptionsSortOrder Game.AchievementsSortOrder
    | OnSetOptionsReverse


update : Msg -> Session -> ( Session, Cmd msg )
update msg session =
    case msg of
        GotTabNavMsg submsg ->
            View.TabNav.update submsg session

        OnAchievementClick achievement ->
            case Achievement.name achievement of
                "clickme" ->
                    Session.grantAchievement "clickme" session

                _ ->
                    ( session, Cmd.none )

        OnSetOptionsEarned ->
            mapAchievementsShown (\s -> { s | earned = not s.earned }) session

        OnSetOptionsUnearned ->
            mapAchievementsShown (\s -> { s | unearned = not s.unearned }) session

        OnSetOptionsMasked ->
            mapAchievementsShown (\s -> { s | masked = not s.masked }) session

        OnSetOptionsSortOrder order ->
            mapAchievementsShown (\s -> { s | order = order }) session

        OnSetOptionsReverse ->
            mapAchievementsShown (\s -> { s | reverse = not s.reverse }) session


mapAchievementsShown : (Game.AchievementsShown -> Game.AchievementsShown) -> Session -> ( Session, Cmd msg )
mapAchievementsShown fn session =
    ( session |> Session.mapGame False (Game.mapAchievementsShown fn), Cmd.none )


view : Session -> Game -> List (Html Msg)
view session game =
    (View.TabNav.viewFromGame Nothing session game
        |> List.map (H.map GotTabNavMsg)
    )
        ++ viewReady session game


viewReady : Session -> Game -> List (Html Msg)
viewReady session game =
    let
        lang =
            T.gameToLang session game

        points =
            Game.achievementPoints game

        bonus : Maybe Float
        bonus =
            Game.gameData game
                |> GameData.unitByName "hatchery"
                |> Maybe.andThen .prod
                |> Maybe.andThen
                    (\( _, prod ) ->
                        Parser.Stat.evalFullWithDefault Decimal.zero (Game.getGameVar game) prod
                            |> .exports
                            |> Dict.get "achievementbonus"
                    )
                |> Maybe.map Decimal.toFloat

        options : Game.AchievementsShown
        options =
            Game.achievementsShown game
    in
    [ h2 [] [ lang.html <| T.ViewAchievementsTitle points.val ]
    , div [ class "progress" ]
        [ div [ class "progress-bar bg-success", style "width" (percent points.percent) ]
            [ lang.html <| T.ViewAchievementsProgress points.percent ]
        ]
    , let
        sortRadio : Game.AchievementsSortOrder -> T.Text -> Html Msg
        sortRadio sortBy body =
            label [] [ input [ type_ "radio", name "sort", checked (options.order == sortBy), onClick <| OnSetOptionsSortOrder sortBy ] [], lang.html body ]

        checkbox : Bool -> Msg -> T.Text -> Html Msg
        checkbox val msg body =
            label [] [ input [ type_ "checkbox", checked val, onClick msg ] [], lang.html body ]
      in
      H.form []
        [ div [] <|
            List.intersperse (text " ") <|
                [ lang.html T.ViewAchievementsOptionsVisible
                , checkbox options.earned OnSetOptionsEarned T.ViewAchievementsOptionsVisibleEarned
                , checkbox options.unearned OnSetOptionsUnearned T.ViewAchievementsOptionsVisibleUnearned
                , checkbox options.masked OnSetOptionsMasked T.ViewAchievementsOptionsVisibleMasked
                ]
        , div [] <|
            List.intersperse (text " ") <|
                [ lang.html T.ViewAchievementsOptionsSort
                , sortRadio Game.AchievementsSortByDefault T.ViewAchievementsOptionsSortDefault
                , sortRadio Game.AchievementsSortByCompletion T.ViewAchievementsOptionsSortCompletion
                , checkbox options.reverse
                    OnSetOptionsReverse
                    (if options.reverse then
                        T.ViewAchievementsOptionsSortReversed

                     else
                        T.ViewAchievementsOptionsSortNotReversed
                    )
                ]
        ]
    , case bonus of
        Nothing ->
            div [] []

        Just b ->
            div [] [ lang.html <| T.ViewAchievementsBonus b ]
    , hr [] []
    , ul [ class "achievements list-group" ]
        (Game.achievements game
            |> List.filter (Tuple.second >> isVisible options)
            |> sort options.order
            |> (if options.reverse then
                    List.reverse

                else
                    identity
               )
            -- `snapshotted` isn't quite correct here, it "should" be `now`.
            -- Animating earned-dates isn't worth breaking Html.Lazy's
            -- caching, though.
            |> List.map (\( a, s ) -> Html.Lazy.lazy5 viewEntry (Game.gameData game) lang (Game.snapshotted game) a s)
        )
    ]


percent : Float -> String
percent pct =
    (pct * 100 |> floor |> String.fromInt) ++ "%"


sort : Game.AchievementsSortOrder -> List ( Achievement, Game.Achievement.Status ) -> List ( Achievement, Game.Achievement.Status )
sort order =
    case order of
        Game.AchievementsSortByDefault ->
            List.sortBy sortByDefault

        Game.AchievementsSortByCompletion ->
            List.sortBy sortByCompletion


sortByDefault : ( Achievement, Game.Achievement.Status ) -> Int
sortByDefault ( achievement, status ) =
    case status of
        Game.Achievement.Earned _ _ ->
            -1

        Game.Achievement.Visible _ ->
            0

        Game.Achievement.Masked ->
            2

        Game.Achievement.Hidden ->
            3


sortByCompletion : ( Achievement, Game.Achievement.Status ) -> Float
sortByCompletion ( achievement, status ) =
    case status of
        Game.Achievement.Earned _ _ ->
            -1

        Game.Achievement.Visible progressList ->
            -- between 0 and 1
            progressList |> List.map .percent |> List.minimum |> Maybe.withDefault 1

        Game.Achievement.Masked ->
            2

        Game.Achievement.Hidden ->
            3


isVisible : Game.AchievementsShown -> Game.Achievement.Status -> Bool
isVisible options status =
    case status of
        Game.Achievement.Hidden ->
            False

        Game.Achievement.Masked ->
            options.masked

        Game.Achievement.Visible _ ->
            options.unearned

        Game.Achievement.Earned _ _ ->
            options.earned


toProgdesc : GameData -> List Parser.Visibility.Progress -> Maybe ( Item.Id, Decimal )
toProgdesc gameData progs_ =
    case progs_ |> List.head of
        Nothing ->
            Nothing

        Just p ->
            case GameData.itemByName p.name gameData |> Maybe.map Item.id of
                Nothing ->
                    Nothing

                Just lid ->
                    Just ( lid, p.max )


viewEntry : GameData -> Lang Msg -> Posix -> Achievement -> Game.Achievement.Status -> Html Msg
viewEntry gameData lang now achievement status =
    -- Uncomment me to prove that Html.Lazy's caching things. I should only log once.
    -- let
    -- _ =
    -- Debug.log "viewEntry" (Achievement.name achievement)
    -- in
    case status of
        Game.Achievement.Hidden ->
            -- I should never run, caller will filter me. Html.Lazy won't let this function return a Maybe though.
            div [] []

        Game.Achievement.Masked ->
            li [ class "list-group-item list-group-item-secondary" ]
                [ div [ class "row", onClick <| OnAchievementClick achievement ]
                    [ div [ class "icon col-2" ] [ View.Icon.fas "question-circle" ]
                    , div [ class "body col-8" ]
                        [ h3 [] [ lang.html T.ViewAchievementsEntryMasked ]

                        -- , div [] [ lang.html T.ViewAchievementsEntryMasked ]
                        ]
                    , div [ class "points col-2" ] [ text <| String.fromInt achievement.points ]
                    ]
                ]

        Game.Achievement.Visible progs ->
            li [ class "list-group-item list-group-item-info" ]
                [ div [ class "row" ]
                    [ div [ class "icon col-2" ] [ View.Icon.fas "trophy" ]
                    , div [ class "body col-8" ]
                        [ h3 [] [ lang.html <| T.AchievementName achievement.id ]
                        , div [] [ lang.html <| T.AchievementDesc achievement.id (toProgdesc gameData progs) ]
                        , div [] [ small [] [ i [] [ lang.html <| T.AchievementLongDesc achievement.id ] ] ]
                        , div []
                            (progs
                                |> List.map
                                    (\prog ->
                                        div [ class "progress" ]
                                            [ div [ class "progress-bar", style "width" (percent prog.percent) ]
                                                [ lang.html <| T.ViewAchievementsEntryProgress prog.percent prog.val prog.max ]
                                            ]
                                    )
                            )
                        ]
                    , div [ class "points col-2" ] [ text <| String.fromInt achievement.points ]
                    ]
                ]

        Game.Achievement.Earned date progs ->
            li [ class "list-group-item list-group-item-success" ]
                [ div [ class "row" ]
                    [ div [ class "icon col-2" ] [ View.Icon.fas "trophy" ]
                    , div [ class "body col-8" ]
                        [ h3 [] [ lang.html <| T.AchievementName achievement.id ]
                        , div [] [ lang.html <| T.AchievementDesc achievement.id (toProgdesc gameData progs) ]
                        , div [] [ small [] [ i [] [ lang.html <| T.AchievementLongDesc achievement.id ] ] ]
                        , div [] [ View.Icon.fas "check", text " ", lang.html <| T.ViewAchievementsEntryDate { at = date, now = now } ]
                        ]
                    , div [ class "points col-2" ] [ text <| "+" ++ String.fromInt achievement.points ]
                    ]
                ]
