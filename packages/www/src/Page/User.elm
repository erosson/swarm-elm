module Page.User exposing (Msg, update, view)

import Cooldown exposing (Cooldown)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.OfflineSync as OfflineSync
import GameData exposing (GameData)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Markdown
import Maybe.Extra
import PlayFab
import PlayFab.Error
import PlayFab.TitleId
import PlayFab.User as User exposing (User)
import Ports
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Route.Feature as Feature
import Session exposing (Session)
import Session.Iframe as Iframe exposing (Iframe)
import Time exposing (Posix)
import View.Error
import View.Icon
import View.Lang as T
import View.NotReady
import View.OfflineSync
import View.TabNav


type Msg
    = GotTabNavMsg View.TabNav.Msg
    | Logout
    | LogoutResponse (Result PlayFab.Error.Error User)
    | ManualPlayFabWriteRequest
    | ManualPlayFabWriteResponse (Maybe Posix) (Result PlayFab.Error.Error ())


update : Msg -> Session -> ( Session, Cmd Msg )
update msg session =
    case msg of
        GotTabNavMsg submsg ->
            View.TabNav.update submsg session

        Logout ->
            let
                ( s, cmd1 ) =
                    Session.logout session
            in
            ( s
              -- Logging out on *any* device invalidates the remember-me token
              -- on *all* devices. This isn't strictly necessary; we could skip
              -- the playfab call and it wouldn't affect other devices this way.
              --
              -- However, there *must* be a way for the user to clear the
              -- remember-id if it's compromised! What are my options for that?
              -- * No remember-me id resets: if a user's id is compromised,
              --   they're hosed without admin intervention. Not an option.
              --   Compromises are unlikely since the id is never visible,
              --   but some dummy will probably still find a way.
              -- * Change-password invalidates all: This is the ideal way to do it,
              --   but PlayFab doesn't have a change-password api.
              -- * A separate "logout all devices" button invalidates all:
              --   this implies we're invalidating all sessions too, but PlayFab,
              --   once again, has no API for invalidating a session.
              -- * A separate "clear all remembered devices": too confusing for
              --   the user. They'd expect it to work like "logout all devices",
              --   and it won't/can't.
              -- * Logout invalidates all: not ideal, but it's the least-worst.
            , case Session.user session of
                RemoteData.Success (Just user) ->
                    Cmd.batch
                        [ cmd1
                        , PlayFab.clearRememberId (PlayFab.TitleId.fromSession session) user LogoutResponse
                        ]

                _ ->
                    cmd1
            )

        LogoutResponse (Ok user) ->
            ( session |> Session.setRawUser user, Cmd.none )

        LogoutResponse (Err err) ->
            ( session |> Session.pushWarning ("LogoutResponse: " ++ PlayFab.Error.toString err), Cmd.none )

        ManualPlayFabWriteRequest ->
            case Session.offlineSync session of
                RemoteData.Success (Just (Ok _)) ->
                    -- only autopush when 1) we're ready, and 2) there's no conflict
                    -- no cooldown/throttling for manual pushes
                    ( Session.userSyncRequest { ignoreCooldown = True } session |> Maybe.withDefault session
                    , PlayFab.writeState session (Session.persistExport session) (ManualPlayFabWriteResponse <| Session.now session)
                    )

                _ ->
                    ( session, Cmd.none )

        ManualPlayFabWriteResponse now res ->
            session |> Session.userSyncResponse now res


view : Session -> Game -> List (Html Msg)
view session game =
    (View.TabNav.viewFromGame Nothing session game
        |> List.map (H.map GotTabNavMsg)
    )
        ++ viewBody session (Game.gameData game)


viewBody : Session -> GameData -> List (Html Msg)
viewBody session gd =
    let
        lang =
            T.gameDataToLang session gd
    in
    case Session.userState session of
        RemoteData.NotAsked ->
            []

        RemoteData.Loading ->
            [ View.Icon.loading ]

        RemoteData.Failure err ->
            -- (3) user's save is corrupt
            [ p [] [ text <| PlayFab.Error.toString err ] ]

        RemoteData.Success Nothing ->
            [ p [] [ lang.html T.ViewUserAnonymous ]
            , p []
                [ div [] [ a [ class "btn btn-link", Route.href (Session.query session) Route.Login ] [ lang.html T.ViewUserAnonymousLogin ] ]
                , div [] [ a [ class "btn btn-link", Route.href (Session.query session) Route.Register ] [ lang.html T.ViewUserAnonymousRegister ] ]
                ]
            ]

        RemoteData.Success (Just ustate) ->
            viewReady session gd ustate


viewReady : Session -> GameData -> Session.UserState -> List (Html Msg)
viewReady session gd ({ user } as ustate) =
    let
        lang =
            T.gameDataToLang session gd

        isConflict =
            Session.offlineSyncConflict session |> Maybe.Extra.isJust

        userId =
            User.idToString <| User.id user

        titleId =
            PlayFab.TitleId.fromSession session
    in
    [ h1 [] [ lang.html T.ViewUserTitle ]
    , p [] [ text <| User.displayName (Session.iframe session) user, text " #", text userId ]
    , p []
        (case User.role user of
            User.AdminRole ->
                [ text "role: admin" ]

            User.PlayerRole ->
                []
        )
    , div []
        (case ustate.sync of
            Err conflict ->
                -- this shouldn't ever appear, since main overrides the view if there's a conflict
                [ p [] [ text "Offline-sync conflict!" ]
                ]

            Ok sync ->
                let
                    since : Maybe Posix -> Maybe Duration
                    since =
                        Maybe.map2 (\after before -> Duration.since { before = before, after = after }) (Session.now session)
                in
                ([ [ lang.html T.ViewUserDescPush1
                   , lang.html <| T.ViewUserDescPush2 <| Session.userSyncCooldownDuration
                   ]
                 , case since sync.succeeded of
                    Just d ->
                        [ lang.html <| T.ViewUserPushSuccess d ]

                    Nothing ->
                        []
                 , case since sync.requested of
                    Just d ->
                        if sync.requested == sync.succeeded || sync.request == RemoteData.Loading then
                            []

                        else
                            [ lang.html <| T.ViewUserPushFailure d ]

                    Nothing ->
                        [ lang.html T.ViewUserPushNone ]
                 , case sync.request of
                    RemoteData.Failure err ->
                        [ lang.html <| T.ViewUserPushError err ]

                    _ ->
                        []
                 , case Session.now session of
                    Nothing ->
                        [ View.Icon.loading ]

                    Just now_ ->
                        case sync.cooldown |> Cooldown.tick now_ of
                            Cooldown.Ready ->
                                [ lang.html T.ViewUserPushReady ]

                            Cooldown.Until until ->
                                let
                                    d =
                                        Duration.since { before = now_, after = until }

                                    pct =
                                        Cooldown.percent now_ Session.userSyncCooldownDuration sync.cooldown
                                in
                                [ lang.html <| T.ViewUserPushNotReady d pct ]
                 ]
                    |> List.concat
                    |> List.map (\s -> p [] [ s ])
                )
                    ++ [ div []
                            [ button [ class "btn btn-info", onClick ManualPlayFabWriteRequest, disabled (isConflict || RemoteData.isLoading sync.request) ]
                                [ if RemoteData.isLoading sync.request then
                                    View.Icon.loading

                                  else
                                    text ""
                                , lang.html T.ViewUserButtonForcePush
                                ]
                            ]
                       , if Feature.isActive Feature.Mtx (Session.query session) then
                            div [] [ a [ class "btn btn-link", Route.href (Session.query session) Route.MtxHistory ] [ lang.html T.ViewMtxShopHistory ] ]

                         else
                            div [] []
                       ]
        )
    , div [] <|
        case Session.iframe session of
            Iframe.WWW ->
                [ div [] [ a [ class "btn btn-link", Route.href (Session.query session) Route.ResetPassword ] [ lang.html T.ViewUserButtonChangePassword ] ]
                , div [] [ button [ class "btn btn-secondary", onClick Logout ] [ lang.html T.ViewUserButtonLogout ] ]
                ]

            Iframe.Kongregate _ ->
                []
    ]
        ++ (if Feature.isActive Feature.Debug (Session.query session) then
                [ div [ class "card" ]
                    [ h6 [ class "card-header" ] [ text "user debug" ]
                    , div [ class "card-body" ]
                        [ p []
                            [ a
                                [ href <| PlayFab.TitleId.devSite titleId ++ "/players/" ++ userId ++ "/overview"
                                , target "_blank"
                                ]
                                [ text "overview #", text userId ]
                            , text ", "
                            , a
                                [ href <| PlayFab.TitleId.devSite titleId ++ "/players/" ++ userId ++ "/data"
                                , target "_blank"
                                ]
                                [ text "userdata #", text userId ]
                            ]
                        , let
                            id =
                                case User.rememberId user |> Maybe.map User.rememberIdToString of
                                    Nothing ->
                                        "(none)"

                                    Just rid ->
                                        rid
                          in
                          div [] [ text <| "remember-me: " ++ id ]
                        ]
                    ]
                ]

            else
                []
           )
