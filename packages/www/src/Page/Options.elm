module Page.Options exposing (Model, Msg(..), create, subscriptions, toSession, update, view)

import Duration exposing (Duration)
import Game exposing (Game)
import Game.NumberFormat as NumberFormat exposing (NumberFormat)
import Game.Theme as Theme exposing (Theme)
import Game.VelocityUnit as VelocityUnit exposing (VelocityUnit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Decode as D
import Locale exposing (Locale)
import Maybe.Extra
import NumberSuffix
import Persist
import Ports
import RemoteData exposing (RemoteData)
import Route exposing (Route)
import Route.Feature as Feature
import Session exposing (Session)
import Time exposing (Posix)
import View.Icon
import View.Lang as T exposing (Lang)
import View.NotReady
import View.TabNav


type Model
    = Model
        { session : Session
        , importStatus : RemoteData String ()
        }


create : Session -> Model
create session =
    Model
        { session = session
        , importStatus = RemoteData.NotAsked
        }


toSession : Model -> Session
toSession (Model m) =
    m.session


type Msg
    = GotSession Session
    | ClickedResetConfirm
    | UpdatedLocale Locale
    | UpdatedTheme Theme
    | UpdatedNumberFormat NumberFormat
    | UpdatedVelocityUnit VelocityUnit
    | ImportedSave String
    | PersistImportRead D.Value
    | GotTabNavMsg View.TabNav.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg ((Model model) as model0) =
    case msg of
        GotSession s ->
            ( Model { model | session = s }, Cmd.none )

        ClickedResetConfirm ->
            case Session.resetGame model.session of
                Ok session1 ->
                    ( Model { model | session = session1 }
                    , Route.pushUrl (Session.nav model.session) (Session.query model.session) Route.Home
                    )

                Err err ->
                    -- This can only fail if gamedata/now are missing - silent no-op for that is fine.
                    ( model0, Cmd.none )

        UpdatedLocale locale ->
            case Session.game model.session of
                RemoteData.Success game ->
                    game
                        |> Game.setLocale locale
                        |> Session.persistWrite model.session
                        |> Tuple.mapFirst (\g -> Model { model | session = Session.setGame False g model.session })

                _ ->
                    ( model0, Cmd.none )

        UpdatedTheme theme ->
            case Session.game model.session of
                RemoteData.Success game ->
                    game
                        |> Game.setTheme theme
                        |> Session.persistWrite model.session
                        |> Tuple.mapFirst (\g -> Model { model | session = Session.setGame False g model.session })

                _ ->
                    ( model0, Cmd.none )

        UpdatedNumberFormat format ->
            case Session.game model.session of
                RemoteData.Success game ->
                    game
                        |> Game.setNumberFormat format
                        |> Session.persistWrite model.session
                        |> Tuple.mapFirst (\g -> Model { model | session = Session.setGame False g model.session })

                _ ->
                    ( model0, Cmd.none )

        UpdatedVelocityUnit format ->
            case Session.game model.session of
                RemoteData.Success game ->
                    game
                        |> Game.setVelocityUnit format
                        |> Session.persistWrite model.session
                        |> Tuple.mapFirst (\g -> Model { model | session = Session.setGame False g model.session })

                _ ->
                    ( model0, Cmd.none )

        ImportedSave save ->
            ( Model
                { model
                    | session = model.session |> Session.setPersistExport (Just save)
                    , importStatus = RemoteData.Loading
                }
            , Ports.persistImport save
            )

        PersistImportRead json ->
            case D.decodeValue Persist.readDecoder json of
                Ok (Just ( r, e )) ->
                    let
                        session1 =
                            model.session
                                |> Session.setPersistRead (Ok <| Just r)
                                |> Session.setPersistExport (Just e)
                    in
                    case Session.game session1 of
                        RemoteData.Success _ ->
                            Session.grantAchievement "import" session1
                                |> Tuple.mapFirst
                                    (\s ->
                                        Model
                                            { model
                                                | importStatus = RemoteData.succeed ()
                                                , session = s
                                            }
                                    )

                        RemoteData.Failure err ->
                            ( Model
                                { model
                                    | importStatus = RemoteData.Failure err

                                    -- don't blindly override the session - if the import failed, keep the current game, don't crash the whole page
                                }
                            , Cmd.none
                            )

                        _ ->
                            ( Model model, Cmd.none )

                Ok Nothing ->
                    ( Model { model | importStatus = RemoteData.Failure "Imported save was empty?" }, Cmd.none )

                Err err ->
                    ( Model { model | importStatus = RemoteData.Failure (D.errorToString err) }, Cmd.none )

        GotTabNavMsg submsg ->
            View.TabNav.update submsg model.session
                |> Tuple.mapFirst (\s -> Model { model | session = s })


subscriptions : Model -> Sub Msg
subscriptions model =
    Ports.persistImportRead PersistImportRead


view : Model -> Game -> List (Html Msg)
view ((Model m) as model) game =
    (View.TabNav.viewFromGame Nothing m.session game
        |> List.map (H.map GotTabNavMsg)
    )
        ++ viewReady model game
        ++ (if Feature.isActive Feature.Debug (Session.query m.session) then
                viewDebug m.session game

            else
                []
           )


viewDebug : Session -> Game -> List (Html msg)
viewDebug session game =
    let
        now =
            Session.safeNow game session

        created =
            Duration.since { before = Game.created game, after = now }

        snapshotted =
            Duration.since { before = Game.snapshotted game, after = now }

        loaded =
            Session.loaded session |> Maybe.map (\t -> Duration.since { before = t, after = now })
    in
    [ hr [] []
    , div [] [ "since created: " ++ Duration.toString created |> text ]
    , div [] [ "since page loaded: " ++ Maybe.Extra.unwrap "???" Duration.toString loaded |> text ]
    , div [] [ "since snapshotted (reified): " ++ Duration.toString snapshotted |> text ]
    ]


viewReady : Model -> Game -> List (Html Msg)
viewReady (Model { session, importStatus }) game =
    let
        lang =
            T.gameToLang session game

        persistExport =
            Session.persistExport session

        query =
            Session.query session
    in
    [ h3 [] [ lang.html T.ViewOptionsTitle ]
    , div [ class "form-group" ]
        [ if Feature.isActive Feature.Fps query then
            a [ Route.href (Feature.deactivate Feature.Fps query) Route.Options, class "btn btn-primary fps-disable" ]
                [ lang.html <| T.ViewOptionsToggleFps False ]

          else
            a [ Route.href (Feature.activate Feature.Fps query) Route.Options, class "btn btn-primary fps-enable" ]
                [ lang.html <| T.ViewOptionsToggleFps True ]
        ]
    , div [ class "form-group" ]
        [ label [] [ lang.html T.ViewOptionsExport ]
        , input
            [ type_ "text"
            , class "form-control"
            , classList
                [ ( "is-invalid", RemoteData.isFailure importStatus )
                , ( "is-valid", RemoteData.isSuccess importStatus )
                ]
            , value (persistExport |> Maybe.withDefault (lang.str T.ViewOptionsNotYetSaved))
            , onInput ImportedSave
            ]
            []
        , div [] (View.NotReady.viewRemoteInline (RemoteData.map (always []) importStatus))
        ]
    , div [ class "form-group" ]
        [ div [ class "dropdown" ]
            [ button
                [ class "nav-link dropdown-toggle btn btn-primary"
                , attribute "data-toggle" "dropdown"
                , attribute "role" "button"
                , attribute "aria-haspopup" "true"
                ]
                [ lang.html <| T.ViewOptionsNumberFormat <| Game.numberFormat game ]
            , div
                [ class "dropdown-menu" ]
                (NumberFormat.list
                    |> List.map
                        (\format ->
                            button
                                [ class "dropdown-item"
                                , onClick (UpdatedNumberFormat format)
                                ]
                                [ lang.html <| T.ViewOptionsNumberFormat format ]
                        )
                )
            ]
        ]
    , div [ class "form-group" ]
        [ div [ class "dropdown" ]
            [ button
                [ class "nav-link dropdown-toggle btn btn-primary"
                , attribute "data-toggle" "dropdown"
                , attribute "role" "button"
                , attribute "aria-haspopup" "true"
                ]
                [ lang.html <| T.ViewOptionsVelocityUnit <| Game.velocityUnit game ]
            , div
                [ class "dropdown-menu" ]
                (VelocityUnit.list
                    |> List.map
                        (\format ->
                            button
                                [ class "dropdown-item"
                                , onClick (UpdatedVelocityUnit format)
                                ]
                                [ lang.html <| T.ViewOptionsVelocityUnit format ]
                        )
                )
            ]
        ]
    , div [ class "form-group" ]
        [ div [ class "dropdown" ]
            [ button
                [ class "nav-link dropdown-toggle btn btn-primary"
                , attribute "data-toggle" "dropdown"
                , attribute "role" "button"
                , attribute "aria-haspopup" "true"
                ]
                [ lang.html <| T.ViewOptionsLocale lang.locale ]
            , div
                [ class "dropdown-menu" ]
                ((Locale.list
                    -- don't show my test locales unless debug's enabled, or unless it's selected
                    -- |> List.filter (\loc -> model.route.flags.debug || loc /= ReadyModel.En_Reverse || model.locale == Just ReadyModel.En_Reverse || loc /= ReadyModel.En_UpsideDown || model.locale == Just ReadyModel.En_UpsideDown)
                    |> List.map
                        (\loc ->
                            button
                                [ class "dropdown-item"
                                , onClick (UpdatedLocale loc)
                                ]
                                [ lang.html <| T.ViewOptionsLocale loc ]
                        )
                 )
                    ++ [ div [ class "dropdown-divider" ] []
                       , a [ class "dropdown-item", Route.href query Route.Translator ]
                            [ lang.html <| T.ViewOptionsHelpTranslate
                            ]
                       ]
                )
            ]
        ]
    , div [ class "form-group" ]
        [ div [ class "dropdown theme-dropdown" ]
            [ button
                [ class "nav-link dropdown-toggle btn btn-primary"
                , attribute "data-toggle" "dropdown"
                , attribute "role" "button"
                , attribute "aria-haspopup" "true"
                ]
                [ lang.html <| T.ViewOptionsTheme <| Game.theme game ]
            , div
                [ class "dropdown-menu" ]
                (Theme.list
                    |> List.map
                        (\theme ->
                            button
                                [ class "dropdown-item"
                                , onClick (UpdatedTheme theme)
                                ]
                                [ lang.html <| T.ViewOptionsTheme theme ]
                        )
                )
            ]
        ]
    , div [ class "form-group" ]
        [ button [ class "btn btn-danger", attribute "data-toggle" "modal", attribute "data-target" ("#" ++ resetModalId) ]
            [ lang.html T.ViewOptionsResetSaveLink ]
        ]
    , viewResetModal lang
    , div []
        [ p []
            [ lang.html T.ViewOptionsAnalyticsDesc
            , text " "
            , a [ target "_blank", href "https://tools.google.com/dlpage/gaoptout" ] [ lang.html T.ViewOptionsAnalyticsOptOutLink ]
            , text "."
            ]
        , p [] [ a [ target "_blank", href "http://www.google.com/policies/privacy/partners/" ] [ lang.html T.ViewOptionsAnalyticsPrivacyLink ] ]
        ]
    , div [ class "form-group", style "float" "right" ]
        (if Feature.isActive Feature.Debug query then
            [ a [ Route.href query Route.Debug, class "btn btn-link" ]
                [ text "Debugging tools" ]
            , a [ Route.href (Feature.deactivate Feature.Debug query) Route.Options, class "btn btn-secondary debug-disable" ]
                [ lang.html <| T.ViewOptionsToggleDebug False ]
            ]

         else
            [ a [ Route.href (Feature.activate Feature.Debug query) Route.Options, class "btn btn-secondary debug-enable" ]
                [ lang.html <| T.ViewOptionsToggleDebug True ]
            ]
        )
    ]


resetModalId =
    "reset-modal"


viewResetModal : Lang Msg -> Html Msg
viewResetModal lang =
    -- https://getbootstrap.com/docs/4.2/components/modal/
    let
        example n =
            -- TODO link examples
            -- a [ href <| "?n=" ++ Url.percentEncode n ] [ text <| "\"" ++ n ++ "\"" ]
            text <| "\"" ++ n ++ "\""
    in
    div [ class "modal fade bg-danger", id resetModalId, tabindex -1, attribute "role" "dialog", attribute "aria-hidden" "true" ]
        [ div [ class "modal-dialog", attribute "role" "document" ]
            [ div [ class "modal-content" ]
                [ div [ class "modal-header" ]
                    [ h5 [ class "modal-title" ]
                        [ View.Icon.fas "exclamation-triangle"
                        ]
                    , button [ class "close", attribute "data-dismiss" "modal", attribute "aria-label" "close" ] [ text "×" ]
                    ]
                , div [ class "modal-body" ]
                    [ p [] [ lang.html T.ViewOptionsResetSaveWarning ]
                    , button [ class "btn btn-primary", attribute "data-dismiss" "modal", attribute "aria-label" "close" ] [ lang.html T.ViewOptionsResetSaveCancel ]
                    , br [] []
                    , br [] []
                    , br [] []
                    , br [] []
                    , br [] []
                    , button [ class "btn btn-danger", attribute "data-dismiss" "modal", onClick ClickedResetConfirm ]
                        [ View.Icon.fas "skull"
                        , text " "
                        , lang.html T.ViewOptionsResetSaveConfirm
                        , text " "
                        , View.Icon.fas "skull"
                        ]
                    ]
                , div [ class "modal-footer" ] []
                ]
            ]
        ]
