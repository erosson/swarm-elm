module Page.Translator exposing (Model, Msg(..), create, toSession, update, view)

import Game exposing (Game)
import GameData exposing (GameData)
import GameData.Lang
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Decode as D
import Json.Encode as E
import Markdown
import Ports
import RemoteData exposing (RemoteData)
import Session exposing (Session)
import Url
import View.Icon
import View.Lang as T
import View.NotReady
import View.TabNav


type Model
    = Model Session (RemoteData String ()) String


type Msg
    = GotSession Session
    | GotTabNavMsg View.TabNav.Msg
    | GotJsonChange String


create : Session -> ( Model, Cmd msg )
create session =
    let
        txt =
            session
                |> Session.translatorJson
                |> Maybe.map (E.encode 2)
                |> Maybe.withDefault (defaultText session)
    in
    ( Model session RemoteData.NotAsked txt, Cmd.none )


defaultText : Session -> String
defaultText =
    Session.gameData
        >> Result.map
            (GameData.lang Nothing
                >> .raw_en_us
                >> List.map (Tuple.mapSecond (List.map (Tuple.mapSecond E.string) >> E.object))
                >> E.object
                >> E.encode 2
            )
        >> Result.withDefault "{\n  \"header\": {\n    \"message\": \"Swarm Simulator\"\n  }\n}"


update : Msg -> Model -> ( Model, Cmd msg )
update msg ((Model session status txt) as model) =
    case msg of
        GotSession s ->
            ( Model s status txt, Cmd.none )

        GotTabNavMsg submsg ->
            View.TabNav.update submsg session |> Tuple.mapFirst (\s -> Model s status txt)

        GotJsonChange txt1 ->
            case D.decodeString GameData.Lang.translatorDecoder txt1 of
                Ok (( entry, json ) as t) ->
                    ( Model (session |> Session.setTranslator (Just t)) (RemoteData.Success ()) txt1
                    , Ports.translatorWrite json
                    )

                Err err ->
                    ( Model session (RemoteData.Failure <| D.errorToString err) txt1, Cmd.none )


toSession : Model -> Session
toSession (Model session _ _) =
    session


view : Model -> Game -> List (Html Msg)
view ((Model session _ _) as model) game =
    (View.TabNav.viewFromGame Nothing session game
        |> List.map (H.map GotTabNavMsg)
    )
        ++ viewReady model game


viewReady : Model -> Game -> List (Html Msg)
viewReady (Model session status txt) game =
    let
        lang =
            T.gameToLang session game
    in
    [ h1 [] [ T.text "Translating Tools" ]
    , p []
        [ text "Text throughout the game will change as you type below. For example, try "
        , code [] [ text "{\"header\": {\"message\": \"群れシミュレータ\"}}" ]
        , text " and watch the top of this screen."
        ]
    , p []
        [ text "Your changes are visible when "
        , code [] [ text "?translator=1" ]
        , text " is in the URL, or when you're on this page."
        ]
    , p []
        [ text "You can upload "
        , a [ target "_blank", href "/lang/en-us.json" ] [ text " the English translation file " ]
        , text " to "
        , a [ target "_blank", href "https://translate.google.com/toolkit/" ] [ text "Google's translation toolkit" ]
        , text " to get started."
        ]
    , p []
        [ a [ target "_blank", href "https://gitlab.com/erosson/swarm-elm/blob/master/CONTRIBUTING.md#contributing-translations" ] [ text "Detailed translation instructions are here" ]
        , text ". Thanks for your help translating Swarm Simulator!"
        ]
    , textarea
        [ style "width" "100%"
        , style "height" "100%"
        , style "min-height" "50vh"
        , onInput GotJsonChange
        ]
        [ text txt ]
    , case status of
        RemoteData.Failure err ->
            pre [ class "alert alert-danger" ] [ text err ]

        _ ->
            a [ href <| "data:text/plain;charset:utf-8," ++ Url.percentEncode txt, download "translation.json" ] [ text "Download your translation file" ]
    ]
