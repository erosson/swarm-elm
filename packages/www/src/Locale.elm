module Locale exposing (Locale(..), decoder, encoder, list, toString)

import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E


type Locale
    = En_US
    | En_Reverse
    | En_UpsideDown


list : List Locale
list =
    [ En_US
    , En_Reverse
    , En_UpsideDown
    ]


toString : Locale -> String
toString loc =
    case loc of
        En_US ->
            "En_US"

        En_Reverse ->
            "En_Reverse"

        En_UpsideDown ->
            "En_UpsideDown"


dict : Dict String Locale
dict =
    list |> List.map (\loc -> ( toString loc, loc )) |> Dict.fromList


encoder : Locale -> E.Value
encoder =
    toString >> E.string


decoder : D.Decoder Locale
decoder =
    D.string
        |> D.andThen
            (\str ->
                case Dict.get str dict of
                    Just loc ->
                        D.succeed loc

                    Nothing ->
                        D.fail <| "no such locale: " ++ str
            )
