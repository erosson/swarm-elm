module Route.Config exposing (Config(..), get, insert, list, nextEnum, remove, setNextEnum, toString)

{-| String configuration values. Usually an enum with a list of valid values, but not always.

Input sources:

  - other features
  - other configs
  - flags
  - persisted state/`Game` (but must be able to resolve without this!)
  - querystring

-}

import Dict exposing (Dict)
import Route.QueryDict as QueryDict exposing (QueryDict)


type
    Config
    -- Use the dev playfab endpoint
    = PlayFab
      -- Override process.env.NODE_ENV
    | Environment
      -- use april fools css when it's not april fools. [on|after|off]
    | Fools
      -- Sometimes we're not in our own tab, but in an iframe of some other site,
      -- like Kongregate. Tailor our behavior to that site.
      -- This duplicates the legacy `kongregate` flag: `?kongregate=1` is
      -- identical to `?iframe=kongregate`
    | Iframe
      -- Paypal urls - sandbox/elm-beta/production
    | PaypalEnv
      -- hostname, used for paypal env - "elm.swarmsim.com", "www.swarmsim.com", "localhost", or some other way-less-common value
    | Hostname


list : List Config
list =
    [ PlayFab
    , Environment
    , Fools
    , Iframe
    , PaypalEnv
    , Hostname
    ]


toString : Config -> String
toString c =
    case c of
        PlayFab ->
            "playFab"

        Environment ->
            "environment"

        Fools ->
            "fools"

        Iframe ->
            "iframe"

        PaypalEnv ->
            "paypalenv"

        Hostname ->
            "hostname"


{-| Example values for each configuration key.

These are used on the debug screen, to easily select a reasonable "next" value.

These are not enforced in any way - any config key can be set to any string.
Callers deal with any unexpected values.

-}
enum : Config -> List String
enum c =
    case c of
        PlayFab ->
            [ "dev", "prod" ]

        Environment ->
            [ "development", "production" ]

        Fools ->
            [ "off", "on", "after" ]

        Iframe ->
            [ "www", "kongregate" ]

        PaypalEnv ->
            [ "production", "sandbox", "none" ]

        Hostname ->
            [ "localhost", "www.swarmsim.com", "elm.swarmsim.com" ]


get : Config -> QueryDict -> Maybe String
get k =
    Dict.get (toString k)


insert : Config -> String -> QueryDict -> QueryDict
insert k =
    Dict.insert (toString k)


remove : Config -> QueryDict -> QueryDict
remove k =
    Dict.remove (toString k)


set : Config -> Maybe String -> QueryDict -> QueryDict
set k mv =
    case mv of
        Nothing ->
            remove k

        Just v ->
            insert k v


nextEnum : Config -> Maybe String -> Maybe String
nextEnum c value =
    let
        values =
            enum c

        rvalues =
            List.reverse values

        valuesMap : Dict String (Maybe String)
        valuesMap =
            List.map2 Tuple.pair rvalues (Nothing :: List.map Just rvalues)
                |> Dict.fromList
    in
    case value of
        Nothing ->
            List.head values

        Just v ->
            Dict.get v valuesMap |> Maybe.andThen identity


setNextEnum : Config -> QueryDict -> QueryDict
setNextEnum c d =
    set c (nextEnum c <| get c d) d
