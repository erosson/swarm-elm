module Route.QueryDict exposing (QueryDict, fromString, fromUrl, toQueryParameters, toString)

import Dict exposing (Dict)
import Dict.Extra
import Maybe.Extra
import Route.Feature as Feature
import Url exposing (Url)
import Url.Builder


type alias QueryDict =
    Dict String String


fromUrl : Url -> QueryDict
fromUrl { path, query } =
    Maybe.Extra.unwrap Dict.empty fromString query
        |> applyPath path


applyPath : String -> QueryDict -> QueryDict
applyPath path =
    case path of
        "/debug" ->
            Dict.insert "debug" "1"

        "/translator" ->
            Dict.insert "translator" "1"

        _ ->
            identity


fromString : String -> QueryDict
fromString =
    let
        parseEntry entry =
            case entry |> String.split "=" |> List.map Url.percentDecode |> Maybe.Extra.combine of
                Just [ k, v ] ->
                    Just ( k, v )

                _ ->
                    Nothing
    in
    String.split "&"
        >> List.filterMap parseEntry
        >> Dict.fromList


toQueryParameters : QueryDict -> List Url.Builder.QueryParameter
toQueryParameters =
    Feature.filterDefaults >> Dict.toList >> List.map (\( k, v ) -> Url.Builder.string k v)


toString : QueryDict -> String
toString =
    toQueryParameters >> Url.Builder.toQuery
