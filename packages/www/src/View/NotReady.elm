module View.NotReady exposing (viewRemoteInline, viewResult)

import Game exposing (Game)
import GameData exposing (GameData)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import RemoteData exposing (RemoteData)
import Session exposing (Session)
import View.Error
import View.Icon
import View.Lang as T



-- Generic error wrappers


viewResult : Session -> GameData -> Result String (List (Html msg)) -> List (Html msg)
viewResult session gd res =
    case res of
        Ok html ->
            html

        Err err ->
            let
                lang =
                    T.gameDataToLang session gd
            in
            [ h1 [] [ lang.html T.ViewErrorTitle ]
            , p [] [ text err ]
            ]


viewRemoteInline : RemoteData String (List (Html msg)) -> List (Html msg)
viewRemoteInline remote =
    case remote of
        RemoteData.NotAsked ->
            []

        RemoteData.Loading ->
            [ View.Icon.loading ]

        RemoteData.Failure err ->
            [ text err ]

        RemoteData.Success html ->
            html
