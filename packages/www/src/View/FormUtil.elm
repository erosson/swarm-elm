module View.FormUtil exposing (error, failure, field)

import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import RemoteData exposing (RemoteData)


field :
    List String
    -> (List (H.Attribute msg) -> List (Html msg) -> Html msg)
    -> List (H.Attribute msg)
    -> List (Html msg)
    -> List (Html msg)
field errors element attrs content =
    case errors of
        [] ->
            [ element attrs content ]

        _ ->
            [ element (attrs ++ [ class "is-invalid" ]) content
            , div [ class "invalid-feedback" ] (errors |> List.map (\err -> div [] [ text err ]))
            ]


error : List String -> Html msg
error errors =
    div [] (errors |> List.map (\e -> div [ class "invalid-feedback", style "display" "block" ] [ text e ]))


failure : RemoteData e s -> Maybe e
failure res =
    case res of
        RemoteData.Failure e ->
            Just e

        _ ->
            Nothing
