module View.LangFormatter exposing (Formatter)

import Decimal exposing (Decimal)
import Duration exposing (Duration)
import Locale exposing (Locale(..))
import Time exposing (Posix)


type alias Formatter =
    { locale : Locale
    , decimal : Decimal -> String
    , smallDecimal : Decimal -> String
    , decimalShort : Decimal -> String
    , smallDecimalShort : Decimal -> String
    , int : Int -> String
    , fullInt : Int -> String
    , percent : Float -> String
    , percent1 : Float -> String
    , date : Posix -> String
    , reldate : Posix -> Posix -> String
    , roman : Int -> String
    , list : List String -> String
    , money : Int -> String
    , shortDuration : Duration -> String
    , longDuration : Duration -> String
    , timerDuration : Duration -> String
    }
