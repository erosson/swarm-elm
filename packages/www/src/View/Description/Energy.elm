module View.Description.Energy exposing (view)

import Game exposing (Game)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Route
import Session exposing (Session)
import View.DescriptionVars
import View.Lang as T exposing (Lang)


view : Session -> Game -> Unit -> Html msg
view session game unit =
    let
        lang =
            T.gameToLang session game
    in
    div []
        [ lang.html <| T.UnitDesc (View.DescriptionVars.evalVar game) unit.id
        , p [] [ a [ Route.href (Session.query session) (Route.unit "energy" "crystal") ] [ lang.html T.ViewEnergyLinkCrystals ] ]
        ]
