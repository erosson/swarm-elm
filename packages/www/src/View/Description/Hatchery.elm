module View.Description.Hatchery exposing (crystalStats, view, viewExpansion)

import Decimal exposing (Decimal)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item.Id as ItemId
import GameData.OnBuy as OnBuy
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Maybe.Extra
import Parser.Stat
import Session exposing (Session)
import View.DescriptionVars
import View.Lang as T exposing (Lang)


view : Session -> Game -> Unit -> Html msg
view session game unit =
    let
        lang =
            T.gameToLang session game

        mutagen =
            mutagenStats game (Game.unitCount unit.id game) unit.onBuy.addRandomUnits

        crystal =
            crystalStats session game unit.onBuy.addUnitsCooldown
    in
    div []
        [ p [] [ lang.html <| T.UnitDesc (View.DescriptionVars.evalVar game) unit.id ]
        , viewMutagenStats lang (ItemId.UnitId unit.id) mutagen
        , p [] (Maybe.Extra.unwrap [] (T.DescHatcheryCrystals (ItemId.UnitId unit.id) >> lang.html >> List.singleton) crystal)
        ]


viewExpansion : Session -> Game -> Upgrade -> Html msg
viewExpansion session game upgrade =
    let
        lang =
            T.gameToLang session game

        mutagen =
            mutagenStats game (Game.upgradeCount upgrade.id game) upgrade.onBuy.addRandomUnits

        crystal =
            crystalStats session game upgrade.onBuy.addUnitsCooldown
    in
    div []
        [ p [] [ lang.html <| T.UpgradeDesc (View.DescriptionVars.evalVar game) upgrade.id ]
        , viewMutagenStats lang (ItemId.UpgradeId upgrade.id) mutagen
        , p [] (Maybe.Extra.unwrap [] (T.DescHatcheryCrystals (ItemId.UpgradeId upgrade.id) >> lang.html >> List.singleton) crystal)
        ]


type MutagenState
    = MutagenReady { chance : Float, count : Decimal }
    | MutagenAlmost { count : Decimal, threshold : Decimal }
    | MutagenNotReady


mutagenStats : Game -> Decimal -> List OnBuy.AddRandomUnitsData -> MutagenState
mutagenStats game parentCount =
    List.head
        >> Maybe.map
            (\d ->
                if Decimal.gte (Decimal.add Decimal.epsilon parentCount) (Decimal.fromInt d.minimum) then
                    MutagenReady
                        { chance = Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game) d.chance |> Decimal.toFloat
                        , count = Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game) d.count
                        }

                else if Decimal.gte (Decimal.add Decimal.epsilon parentCount) (Decimal.fromInt d.minimum |> Decimal.flipDiv two) then
                    MutagenAlmost
                        { threshold = Decimal.fromInt d.minimum
                        , count = parentCount
                        }

                else
                    MutagenNotReady
            )
        >> Maybe.withDefault MutagenNotReady


two =
    Decimal.fromInt 2


viewMutagenStats : Lang msg -> ItemId.Id -> MutagenState -> Html msg
viewMutagenStats lang id state =
    case state of
        MutagenReady mutagen ->
            p [ title <| lang.str T.DescHatcheryMutagenLol ] [ lang.html <| T.DescHatcheryMutagen id mutagen ]

        MutagenAlmost mutagen ->
            let
                percent =
                    Decimal.div mutagen.count mutagen.threshold |> Decimal.toFloat

                percentStr =
                    (100 * percent |> floor |> String.fromInt) ++ "%"
            in
            p [ class "progress" ]
                [ div [ class "progress-bar", style "width" percentStr ]
                    [ lang.html <| T.DescHatcheryMutagenUnlock id mutagen ]
                ]

        MutagenNotReady ->
            p [] []


crystalStats : Session -> Game -> List ( Unit.Id, Parser.Stat.Expr, OnBuy.Cooldown ) -> Maybe { cooldown : Duration, count : Decimal }
crystalStats session game =
    let
        units =
            Game.units game
    in
    List.head
        >> Maybe.andThen
            (\( crystalId, countExpr, cooldownMax ) ->
                if
                    UnitsSnapshot.isUnitVisible crystalId units
                        -- crystals are always visible so we can link to the mtxshop externally.
                        -- We need better criteria than that for showing this description.
                        && UnitsSnapshot.isNameVisible "energy" units
                then
                    Just
                        { cooldown = Game.cooldownRemaining (Session.safeNow game session) cooldownMax game
                        , count = Parser.Stat.evalWithDefault Decimal.zero (Game.getGameVar game) countExpr
                        }

                else
                    Nothing
            )
