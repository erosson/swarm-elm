module View.Description.CloneLarvae exposing (view)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot
import GameData exposing (GameData)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab.Id as TabId
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Parser.Stat
import Route exposing (Route)
import Session exposing (Session)
import View.Description.Cocoon
import View.DescriptionVars
import View.Lang as T exposing (Lang)


view : Session -> Game -> Spell -> Html msg
view session game spell =
    let
        lang =
            T.gameToLang session game

        query =
            Session.query session
    in
    div []
        [ p [] [ lang.html <| T.DescCloneLarvae1 (View.DescriptionVars.evalVar game) ]
        , p [] [ lang.html <| T.DescCloneLarvae2 query (View.Description.Cocoon.capHref game) (View.DescriptionVars.evalVar game) ]
        , p [] [ lang.html <| T.DescCloneLarvae3 (View.DescriptionVars.evalVar game) ]
        ]
