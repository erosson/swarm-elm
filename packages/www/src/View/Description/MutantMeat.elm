module View.Description.MutantMeat exposing (view)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot
import GameData exposing (GameData)
import GameData.Tab.Id as TabId
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Parser.Stat
import Session exposing (Session)
import View.Lang as T exposing (Lang)


view : Session -> Game -> Html msg
view session game =
    let
        gd =
            Game.gameData game

        bonus : Unit -> Maybe Decimal
        bonus unit =
            unit.prod
                |> Maybe.andThen
                    (Tuple.second
                        >> Parser.Stat.evalFullWithDefault Decimal.zero (Game.getGameVar game)
                        >> .exports
                        >> Dict.get "mutation"
                    )

        units =
            game
                |> Game.gameData
                |> GameData.unitsByTabId (TabId.fromString "meat")
                |> List.filter (\u -> UnitsSnapshot.isUnitVisible u.id (Game.units game))

        bonuses : List ( Unit, Decimal )
        bonuses =
            units |> List.filterMap (\u -> bonus u |> Maybe.map (Tuple.pair u))

        lang =
            T.gameToLang session game

        viewBonus ( u, b ) =
            div [] [ lang.html <| T.DescMutantMeatProd u.id b ]
    in
    p [] (bonuses |> List.map viewBonus)



-- div [] [ text "lolmeat", div [] (bonuses |> List.map (\( u, b ) -> div [] [ text <| Unit.name u, text ": ", text <| Debug.toString b ])) ]
