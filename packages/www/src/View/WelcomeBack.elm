module View.WelcomeBack exposing (Msg, update, view)

import Decimal exposing (Decimal)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Json.Decode as D
import Route exposing (Route)
import Session exposing (Session)
import View.Lang as T exposing (Lang)


type Msg
    = Close
    | Noop


update : Msg -> Session -> Session
update msg session =
    case msg of
        Close ->
            Session.clearWelcomeBack session

        Noop ->
            session


view : Session -> Game -> List (Html Msg)
view session game =
    case Session.welcomeBack session of
        Just wb ->
            let
                lang =
                    T.gameToLang session game

                duration =
                    Duration.since { before = UnitsSnapshot.at wb.before, after = UnitsSnapshot.at wb.after }
            in
            -- Manually render the open modal - display:block and modal-backdrop.
            -- This is not how you're supposed to do it according to the docs, but
            -- it seems to work, and avoids ports and a bootstrap-JS call.
            [ div
                [ class "modal modal-open show"
                , style "display" "block"
                , tabindex -1
                , attribute "role" "dialog"
                , attribute "aria-hidden" "true"
                , onClick Close
                ]
                [ div [ class "modal-dialog", attribute "role" "document" ]
                    [ div
                        [ class "modal-content"

                        -- Closing when the background is clicked is a bit tricky.
                        -- `onClick` on the `modal-backdrop` does not work.
                        -- `onClick` on the `modal` itself does - it catches all
                        -- clicks for both the dialog and the background.
                        -- We don't want to close when the dialog itself is
                        -- clicked, though - just the background - so this
                        -- `stopPropagation` replaces the close with a no-op.
                        , stopPropagationOn "click" <| D.succeed ( Noop, True )
                        ]
                        [ div [ class "modal-header" ]
                            [ h5 [ class "modal-title" ]
                                [ lang.html T.ViewWelcomeBackTitle
                                ]
                            , button [ class "close", onClick Close, attribute "aria-label" "close" ] [ text "×" ]
                            ]
                        , div [ class "modal-body" ]
                            [ p [ title <| lang.str T.ViewWelcomeBackLol ]
                                [ lang.html <| T.ViewWelcomeBack duration ]
                            , ul [] (diffUnits game wb |> List.map (viewUnitsDiff session game))
                            ]
                        , div [ class "modal-footer" ]
                            [ button [ style "width" "100%", class "btn btn-primary", onClick Close ] [ lang.html T.ViewWelcomeBackClose ]
                            ]
                        ]
                    ]
                ]
            , div [ class "modal-backdrop show" ] []
            ]

        Nothing ->
            []


diffUnits : Game -> { before : UnitsSnapshot, after : UnitsSnapshot } -> List ( Unit, Decimal )
diffUnits game { before, after } =
    -- show all tab-leading units, and three leading generations of meat
    -- this is how old-swarmsim did it
    let
        gd =
            Game.gameData game

        isVisible : Unit -> Bool
        isVisible unit =
            UnitsSnapshot.isUnitVisible unit.id after

        diff1 : Unit.Id -> Decimal
        diff1 id =
            Decimal.sub (UnitsSnapshot.count id after) (UnitsSnapshot.count id before)
                |> Decimal.max Decimal.zero

        tabUnits : List Unit
        tabUnits =
            gd
                |> GameData.tabs
                |> List.filterMap (\tab -> GameData.unitById tab.unitId gd)
                |> List.filter isVisible

        meatUnits : List Unit
        meatUnits =
            gd
                |> GameData.unitsByTabName "meat"
                -- Show the top three meat units having a nonzero velocity.
                -- Never show meat itself, `tabUnits` already guarantees it
                |> List.filter (\unit -> Unit.name unit /= "meat")
                |> List.filter isVisible
                |> List.filter (\unit -> Decimal.gt (UnitsSnapshot.velocity unit.id after) Decimal.zero)
                |> List.take 3
    in
    meatUnits
        ++ tabUnits
        |> List.map (\u -> ( u, diff1 u.id ))
        -- if a unit's gained nothing, don't show it
        |> List.filter (\( u, c ) -> Decimal.gt c Decimal.zero)


viewUnitsDiff : Session -> Game -> ( Unit, Decimal ) -> Html msg
viewUnitsDiff session game ( unit, count ) =
    let
        lang =
            T.gameToLang session game

        route =
            Route.unit (Tab.idToString unit.tab) (Unit.name unit)
    in
    li [] [ a [ Route.href (Session.query session) route ] [ lang.html <| T.ViewWelcomeBackEntry unit.id count ] ]
