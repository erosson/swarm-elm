module View.Watcher exposing (Msg(..), update, viewUnitWatcher, viewUpgradeWatcher)

import Game exposing (Game)
import Game.Watched as Watched exposing (Watched)
import GameData exposing (GameData)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Html as H exposing (..)
import Html.Attributes as A exposing (..)
import Html.Events as E exposing (..)
import Session exposing (Session)
import View.Icon


type Msg
    = SetWatched Item Watched


update : Session -> Msg -> Game -> ( Game, Cmd Msg )
update session msg game =
    case msg of
        SetWatched item watched ->
            game |> Game.insertWatched item watched |> Session.persistWrite session


viewUnitWatcher : Game -> Unit -> Html Msg
viewUnitWatcher game unit =
    viewWatcher [ Watched.Cost1, Watched.Cost2, Watched.Cost4, Watched.Never ] game (Item.Unit unit)


viewUpgradeWatcher : Game -> Upgrade -> Html Msg
viewUpgradeWatcher game upgrade =
    viewWatcher [ Watched.Cost1, Watched.Cost2, Watched.Cost4, Watched.Never, Watched.Hidden ] game (Item.Upgrade upgrade)


viewWatcher : List Watched -> Game -> Item -> Html Msg
viewWatcher options game item =
    case Game.watched item game of
        Nothing ->
            -- This kind of item cannot be watched, ever - ex. most units.
            -- Don't show its ui (also, don't make callers do the visibility-check)
            div [] []

        Just active ->
            div [ class "float-right" ]
                [ button
                    [ class "btn dropdown-toggle"
                    , type_ "button"
                    , attribute "data-toggle" "dropdown"
                    , attribute "aria-haspopup" "true"
                    ]
                    (viewWatchedIcon active)
                , div [ class "dropdown-menu" ]
                    (options |> List.map (viewWatcherEntry game item active))
                ]


viewWatcherEntry : Game -> Item -> Watched -> Watched -> Html Msg
viewWatcherEntry game item active entry =
    button
        [ class "dropdown-item"
        , classList [ ( "active", active == entry ) ]
        , onClick <| SetWatched item entry
        ]
        (viewWatchedIcon entry ++ [ text <| viewWatchedDesc entry ])


viewWatchedDesc : Watched -> String
viewWatchedDesc watched =
    case watched of
        Watched.Cost1 ->
            " Notify when buyable"

        Watched.Cost2 ->
            " Notify at 2x cost"

        Watched.Cost4 ->
            " Notify at 4x cost"

        Watched.Never ->
            " Never notify"

        Watched.Hidden ->
            " Hide upgrade"


viewWatchedIcon : Watched -> List (Html msg)
viewWatchedIcon watched =
    case watched of
        Watched.Hidden ->
            [ View.Icon.upgradable, text " ", View.Icon.fas "eye-slash" ]

        Watched.Never ->
            [ View.Icon.upgradable, text " ", View.Icon.fas "ban" ]

        Watched.Cost1 ->
            [ View.Icon.upgradable, text " ×1" ]

        Watched.Cost2 ->
            [ View.Icon.upgradable, text " ×2" ]

        Watched.Cost4 ->
            [ View.Icon.upgradable, text " ×4" ]
