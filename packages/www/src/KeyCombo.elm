module KeyCombo exposing
    ( Binding
    , Bindings
    , Input
    , KeyCombo
    , bind
    , bindings
    , emptyBindings
    , fromString
    , getBinding
    , getMsg
    , inputDecoder
    , isAcceptedFromTextField
    , mapBindings
    , names
    , normalize
    , toString
    , union
    )

{-| Backend for keyboard shortcuts/hotkeys.

"Where's the list of keycombos?" Main.elm:globalKeyBindings and Page/\*.elm:keyBindings

-}

import Dict exposing (Dict)
import Json.Decode as D
import Result.Extra



-- https://developer.mozilla.org/en-US/docs/Web/API/Document/keydown_event


type alias KeyCombo =
    { key : String
    , ctrlKey : Bool
    , altKey : Bool
    , shiftKey : Bool
    , metaKey : Bool
    }


fromString : String -> Result String KeyCombo
fromString str =
    case str |> String.replace " " "" |> String.split "+" |> List.reverse of
        [] ->
            Err <| "empty KeyCombo: " ++ str

        key :: mods ->
            List.foldl (\c -> Result.andThen (applyModFromString c)) (Ok <| KeyCombo key False False False False) mods


applyModFromString : String -> KeyCombo -> Result String KeyCombo
applyModFromString mod c =
    case String.toLower mod of
        "ctrl" ->
            Ok { c | ctrlKey = True }

        "alt" ->
            Ok { c | altKey = True }

        "shift" ->
            Ok { c | shiftKey = True }

        "meta" ->
            Ok { c | metaKey = True }

        _ ->
            Err <| "not a modifier key: " ++ mod


toString : KeyCombo -> String
toString c =
    [ modToString c.metaKey "meta"
    , modToString c.ctrlKey "ctrl"
    , modToString c.altKey "alt"

    -- , modToString c.shiftKey "shift"
    , Just c.key
    ]
        |> List.filterMap identity
        |> String.join "+"


modToString : Bool -> String -> Maybe String
modToString val str =
    if val then
        Just str

    else
        Nothing


normalize : String -> Result String String
normalize =
    fromString >> Result.map toString


type alias Input =
    { key : KeyCombo
    , isTextField : Bool
    }


{-| Can we accept this key combo while the user is typing into a text field?

This decision is based solely on the keys used, no app-specific context.
If the input is ambiguous - that is, if they could be typing it as text without
intending it as a key combo - the text field wins.

-}
isAcceptedFromTextField : KeyCombo -> Bool
isAcceptedFromTextField c =
    c.altKey || c.ctrlKey || c.metaKey


inputDecoder : D.Decoder Input
inputDecoder =
    D.map2 Input
        keyComboDecoder
        isTextDecoder


keyComboDecoder : D.Decoder KeyCombo
keyComboDecoder =
    D.map5 KeyCombo
        (D.field "key" D.string)
        (D.field "ctrlKey" D.bool)
        (D.field "altKey" D.bool)
        (D.field "shiftKey" D.bool)
        (D.field "metaKey" D.bool)


{-| Is this keyboard input occurring while typing into an input box?

TODO: catch contentEditable too

-}
isTextDecoder : D.Decoder Bool
isTextDecoder =
    D.at [ "target", "tagName" ] D.string
        |> D.map isTextField


isTextField tagName =
    case String.toLower tagName of
        "input" ->
            True

        "textarea" ->
            True

        _ ->
            False


type alias Binding m =
    { name : String, msg : m, key : KeyCombo }


type Bindings m
    = Bindings { list : List (Binding m), byKey : Dict String (Binding m) }


bind : String -> String -> m -> Result String (Binding m)
bind key name msg =
    fromString key |> Result.map (Binding name msg)


bindings : List (Result String (Binding m)) -> Result String (Bindings m)
bindings =
    Result.Extra.combine >> Result.map bindings_


bindings_ : List (Binding m) -> Bindings m
bindings_ l =
    Bindings
        { list = l
        , byKey =
            l
                |> List.map (\b -> ( toString b.key, b ))
                |> Dict.fromList
        }


union : Bindings m -> Bindings m -> Bindings m
union a b =
    bindings_ <| list a ++ list b


list : Bindings m -> List (Binding m)
list (Bindings b) =
    b.list


names : Bindings m -> List ( KeyCombo, String )
names =
    list >> List.map (\c -> ( c.key, c.name ))


emptyBindings : Bindings m
emptyBindings =
    bindings_ []


mapBinding : (a -> b) -> Binding a -> Binding b
mapBinding fn b =
    { name = b.name, key = b.key, msg = fn b.msg }


mapBindings : (a -> b) -> Bindings a -> Bindings b
mapBindings fn (Bindings bs) =
    bs.list |> List.map (mapBinding fn) |> bindings_


getBinding : Input -> Bindings m -> Maybe (Binding m)
getBinding input (Bindings { byKey }) =
    if input.isTextField && not (isAcceptedFromTextField input.key) then
        Nothing

    else
        Dict.get (toString input.key) byKey


getMsg : Input -> Bindings m -> Maybe m
getMsg input =
    getBinding input >> Maybe.map .msg
