module Cooldown exposing (Cooldown(..), decoder, encoder, percent, repeat, tick, trigger)

{-| After triggering an event, wait for a duration before it can be triggered again.
-}

import Duration exposing (Duration)
import Json.Decode as D
import Json.Encode as E
import JsonUtil
import Maybe.Extra
import Time exposing (Posix)


type Cooldown
    = Ready
    | Until Posix


tick : Posix -> Cooldown -> Cooldown
tick now status =
    case status of
        Until until_ ->
            if Time.posixToMillis now >= Time.posixToMillis until_ then
                Ready

            else
                status

        _ ->
            status


trigger : Posix -> Duration -> Cooldown -> Maybe Cooldown
trigger now duration cooldown =
    case tick now cooldown of
        Ready ->
            Just <| Until <| Duration.addPosix duration now

        Until until ->
            Nothing


percent : Posix -> Duration -> Cooldown -> Float
percent now duration cooldown =
    case tick now cooldown of
        Ready ->
            1

        Until end ->
            let
                start : Posix
                start =
                    Duration.addPosix (Duration.negate duration) end

                elapsed : Duration
                elapsed =
                    Duration.since { before = start, after = now }
            in
            (elapsed |> Duration.toMillis |> toFloat)
                / (duration |> Duration.toMillis |> toFloat |> max 1)


repeat : Posix -> Duration -> Cooldown -> ( Int, Cooldown )
repeat now duration cooldown =
    case cooldown of
        Ready ->
            -- we haven't been repeating, but start now
            ( 1, Until <| Duration.addPosix duration now )

        Until until ->
            let
                diff =
                    Time.posixToMillis now - (Time.posixToMillis until - Duration.toMillis duration)

                cycles =
                    diff // Duration.toMillis duration |> max 0
            in
            ( cycles, Until <| Duration.addPosix (cycles * Duration.toMillis duration |> Duration.fromMillis) until )


encoder : Cooldown -> Maybe E.Value
encoder status =
    case status of
        Ready ->
            Nothing

        Until until_ ->
            Just <| JsonUtil.posixEncoder until_


decoder : D.Decoder Cooldown
decoder =
    D.nullable JsonUtil.posixDecoder
        |> D.map (Maybe.Extra.unwrap Ready Until)
