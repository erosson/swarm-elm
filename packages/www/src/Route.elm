module Route exposing (PaypalReturnArgs, Route(..), fromUrl, href, pushUrl, toString, unit)

import Browser.Navigation as Nav
import Dict exposing (Dict)
import Html
import Html.Attributes
import Maybe.Extra
import Route.QueryDict as QueryDict exposing (QueryDict)
import Url exposing (Url)
import Url.Builder as Builder
import Url.Parser as P exposing ((</>), (<?>), Parser)
import Url.Parser.Query as Query


type Route
    = Home
    | Debug
    | Tab String
    | MtxShop String (Maybe PaypalReturnArgs)
    | Unit String String { n : Maybe String }
    | Spell String String
    | Achievements
    | Options
    | Statistics
    | Changelog
    | User
    | MtxHistory
    | Login
    | Register
    | ResetPassword
    | PaypalDone String (Maybe PaypalReturnArgs)
    | MoreMenu
    | Translator



-- PARSE


fromUrl : Url -> Maybe Route
fromUrl =
    P.parse parser


parser : Parser (Route -> a) a
parser =
    P.oneOf
        [ P.map Home <| P.top
        , P.map Debug <| P.s "debug"
        , P.map Tab <| P.s "tab" </> escString
        , P.map MtxShop <| P.s "tab" </> escString </> P.s "unit" </> P.s "crystal" <?> paypalReturnArgsParser
        , P.map Unit <| P.s "tab" </> escString </> P.s "unit" </> escString <?> Query.map (\n -> { n = n }) (Query.string "n")
        , P.map Spell <| P.s "tab" </> escString </> P.s "spell" </> escString
        , P.map Achievements <| P.s "achievements"
        , P.map Options <| P.s "options"
        , P.map Statistics <| P.s "statistics"
        , P.map Changelog <| P.s "changelog"
        , P.map User <| P.s "auth" </> P.s "user"
        , P.map MtxHistory <| P.s "auth" </> P.s "user" </> P.s "orders"
        , P.map Login <| P.s "auth" </> P.s "login"
        , P.map Register <| P.s "auth" </> P.s "register"
        , P.map ResetPassword <| P.s "auth" </> P.s "password"
        , P.map PaypalDone <| P.s "paypal" </> escString <?> paypalReturnArgsParser
        , P.map MoreMenu <| P.s "moremenu"
        , P.map Translator <| P.s "translator"
        ]


type alias PaypalReturnArgs =
    { tx : String
    }


paypalReturnArgsParser =
    Query.map (Maybe.map PaypalReturnArgs)
        (Query.string "tx")


{-| Strings in `/paths/like/this/` aren't escaped by default. Use this instead.
-}
escString : Parser (String -> a) a
escString =
    P.string |> P.map (String.toLower >> (\s -> s |> Url.percentDecode |> Maybe.withDefault s))



-- RENDER


href : QueryDict -> Route -> Html.Attribute msg
href query route =
    Html.Attributes.href <| toString query route


pushUrl : Nav.Key -> QueryDict -> Route -> Cmd msg
pushUrl key query route =
    Nav.pushUrl key <| toString query route


{-| Prefer `href` or `pushUrl` over this function, where possible.
-}
toString : QueryDict -> Route -> String
toString query route =
    let
        ( path, qs ) =
            toBuilder route
    in
    -- TODO: removing n from querydict is a filthy hack. Figure out some better way to do this
    path ++ Builder.toQuery (qs ++ QueryDict.toQueryParameters (Dict.remove "n" query))


toBuilder : Route -> ( String, List Builder.QueryParameter )
toBuilder route =
    case route of
        Home ->
            ( "/", [] )

        Debug ->
            ( "/debug", [] )

        Tab tab ->
            ( "/tab/" ++ tab, [] )

        MtxShop tab paypalReturnArgs ->
            ( "/tab/" ++ tab ++ "/unit/crystal", paypalReturnArgs |> Maybe.Extra.unwrap [] paypalReturnArgsToBuilder )

        Unit tab unit_ { n } ->
            ( "/tab/" ++ tab ++ "/unit/" ++ unit_, n |> Maybe.Extra.unwrap [] (Builder.string "n" >> List.singleton) )

        Spell tab spell ->
            ( "/tab/" ++ tab ++ "/spell/" ++ spell, [] )

        Achievements ->
            ( "/achievements", [] )

        Options ->
            ( "/options", [] )

        Statistics ->
            ( "/statistics", [] )

        Changelog ->
            ( "/changelog", [] )

        User ->
            ( "/auth/user", [] )

        MtxHistory ->
            ( "/auth/user/orders", [] )

        Login ->
            ( "/auth/login", [] )

        Register ->
            ( "/auth/register", [] )

        ResetPassword ->
            ( "/auth/password", [] )

        PaypalDone _ _ ->
            -- This should never happen - paypal purchases point here, not our own site
            ( "/paypal/error", [] )

        MoreMenu ->
            ( "/moremenu", [] )

        Translator ->
            ( "/translator", [] )


paypalReturnArgsToBuilder : PaypalReturnArgs -> List Builder.QueryParameter
paypalReturnArgsToBuilder args =
    [ Builder.string "tx" args.tx ]



-- BUILD


unit : String -> String -> Route
unit tab name =
    Unit tab name { n = Nothing }
