module Romanize exposing (romanize)

import List.Extra


{-| Int to roman-numeral converter.

Based on the first comment of <http://blog.stevenlevithan.com/archives/javascript-roman-numeral-converter>

-}
digits =
    [ ( "M", 1000 )
    , ( "CM", 900 )
    , ( "D", 500 )
    , ( "CD", 400 )
    , ( "C", 100 )
    , ( "XC", 90 )
    , ( "L", 50 )
    , ( "XL", 40 )
    , ( "X", 10 )
    , ( "IX", 9 )
    , ( "V", 5 )
    , ( "IV", 4 )
    , ( "I", 1 )
    ]


loop : Int -> List String -> List String
loop num ret =
    case List.Extra.find (\( _, limit ) -> num >= limit) digits of
        Just ( digit, limit ) ->
            loop (num - limit) (digit :: ret)

        Nothing ->
            ret


romanize : Int -> String
romanize num =
    loop num [] |> List.reverse |> String.join ""
