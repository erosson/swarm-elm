module Game.Watched exposing (Watched(..), costPercent, decoder, encoder, get)

import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E
import JsonUtil


type Watched
    = Hidden
    | Never
    | Cost1
    | Cost2
    | Cost4


watches : List Watched
watches =
    [ Hidden, Never, Cost1, Cost2, Cost4 ]


costPercent : Watched -> Maybe Float
costPercent w =
    case w of
        Cost1 ->
            Just 1

        Cost2 ->
            Just <| 1 / 2

        Cost4 ->
            Just <| 1 / 4

        _ ->
            Nothing


get : String -> Dict String Watched -> Watched
get name =
    Dict.get name >> Maybe.withDefault Cost1


toInt : Watched -> Int
toInt w =
    case w of
        Hidden ->
            -1

        Never ->
            0

        Cost1 ->
            1

        Cost2 ->
            2

        Cost4 ->
            4


byInt : Dict Int Watched
byInt =
    watches |> List.map (\w -> ( toInt w, w )) |> Dict.fromList


fromInt : Int -> Maybe Watched
fromInt n =
    Dict.get n byInt


encoder : Watched -> E.Value
encoder =
    toInt >> E.int


boolToInt01 b =
    if b then
        1

    else
        0


decoder : D.Decoder Watched
decoder =
    D.oneOf
        [ D.int

        -- older save formats
        , D.string |> D.andThen (String.toInt >> JsonUtil.decodeMaybe "invalid watched value")
        , D.bool |> D.map boolToInt01
        ]
        |> D.andThen
            (\n ->
                case fromInt n of
                    Nothing ->
                        D.fail ("invalid watched state: " ++ String.fromInt n)

                    Just w ->
                        D.succeed w
            )
