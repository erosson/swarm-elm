module Game.UnitsSnapshot exposing
    ( Production
    , UnitsSnapshot
    , add
    , addName
    , at
    , count
    , countName
    , create
    , eval
    , insert
    , isItemVisible
    , isNameVisible
    , isSpellVisible
    , isTabVisible
    , isUnitVisible
    , isUpgradeVisible
    , mapCounts
    , mul
    , production
    , remove
    , since
    , sub
    , toDict
    , update
    , velocities
    , velocity
    , visibleNames
    )

import Decimal exposing (Decimal)
import DecimalDict
import Dict exposing (Dict)
import Duration exposing (Duration)
import GameData exposing (GameData)
import GameData.Cost as Cost exposing (Cost)
import GameData.Item as Item exposing (Item)
import GameData.Spell as Spell exposing (Spell)
import GameData.Tab as Tab exposing (Tab)
import GameData.Unit as Unit exposing (Unit)
import GameData.Upgrade as Upgrade exposing (Upgrade)
import Parser.Stat
import Parser.Visibility
import Polynomial exposing (Polynomial)
import Set exposing (Set)
import Time exposing (Posix)


type UnitsSnapshot
    = UnitsSnapshot Data


type alias Data =
    { at : Posix
    , counts : Dict String Decimal
    , velocities : Dict String Decimal
    , visible : Set String
    }


create : Posix -> Dict String Decimal -> Dict String Decimal -> Set String -> UnitsSnapshot
create now counts velocities_ visible =
    UnitsSnapshot
        { at = now
        , counts = counts
        , velocities = velocities_
        , visible = visible
        }


{-| Calculate units at some point in the future. Timestep.
-}
eval : (UnitsSnapshot -> String -> Maybe Decimal) -> GameData -> Posix -> Duration -> Dict String Polynomial -> List ( Unit, Decimal ) -> Set String -> UnitsSnapshot
eval getVar gameData now dt polys caps visible0 =
    let
        uncappedCounts : Dict String Decimal
        uncappedCounts =
            polys |> Dict.map (\_ -> Polynomial.evaluate dt)

        applyCap ( u, max ) =
            Dict.update (Unit.name u) (Maybe.map (Decimal.min max))

        counts =
            List.foldl applyCap uncappedCounts caps

        velocities_ : Dict String Decimal
        velocities_ =
            polys |> Dict.map (\_ -> List.drop 1 >> Polynomial.evaluate dt)

        visible : Set String
        visible =
            evalVisibility gameData (getVar <| create now counts velocities_ visible0) visible0
    in
    create now counts velocities_ visible


evalVisibility : GameData -> (String -> Maybe Decimal) -> Set String -> Set String
evalVisibility gameData getVar visible0 =
    let
        visRules : List ( String, Parser.Visibility.Expr )
        visRules =
            gameData |> GameData.items |> List.map (\i -> ( Item.name i, Item.visible i ))

        maxedUpgrades : Set String
        maxedUpgrades =
            gameData
                |> GameData.upgrades
                |> List.filter (\u -> Maybe.map2 Decimal.gte (getVar <| Upgrade.name u) u.maxLevel |> Maybe.withDefault False)
                |> List.map Upgrade.name
                |> Set.fromList
    in
    Parser.Visibility.evalSetWithDefault Decimal.zero getVar visRules visible0
        -- maxed upgrades are never visible
        |> (\v -> Set.diff v maxedUpgrades)


unwrap : UnitsSnapshot -> Data
unwrap (UnitsSnapshot s) =
    s


at : UnitsSnapshot -> Posix
at =
    unwrap >> .at


since : Posix -> UnitsSnapshot -> Duration
since now snapshot =
    Duration.since { before = at snapshot, after = now }


toDict : UnitsSnapshot -> Dict String Decimal
toDict =
    unwrap >> .counts


velocities : UnitsSnapshot -> Dict String Decimal
velocities =
    unwrap >> .velocities


count : Unit.Id -> UnitsSnapshot -> Decimal
count id =
    countName (Unit.idToString id)


countName : String -> UnitsSnapshot -> Decimal
countName name =
    toDict >> Dict.get name >> Maybe.withDefault Decimal.zero


velocity : Unit.Id -> UnitsSnapshot -> Decimal
velocity id =
    velocities >> Dict.get (Unit.idToString id) >> Maybe.withDefault Decimal.zero


visibleNames : UnitsSnapshot -> Set String
visibleNames =
    unwrap >> .visible


isTabVisible : Tab.Id -> UnitsSnapshot -> Bool
isTabVisible id =
    visibleNames >> Set.member (Tab.idToString id)


isUnitVisible : Unit.Id -> UnitsSnapshot -> Bool
isUnitVisible id =
    visibleNames >> Set.member (Unit.idToString id)


isUpgradeVisible : Upgrade.Id -> UnitsSnapshot -> Bool
isUpgradeVisible id =
    visibleNames >> Set.member (Upgrade.idToString id)


isSpellVisible : Spell.Id -> UnitsSnapshot -> Bool
isSpellVisible id =
    visibleNames >> Set.member (Spell.idToString id)


isItemVisible : Item.Id -> UnitsSnapshot -> Bool
isItemVisible id =
    visibleNames >> Set.member (Item.idToString id)


isNameVisible : String -> UnitsSnapshot -> Bool
isNameVisible name =
    visibleNames >> Set.member name


type alias Production =
    { produces : Unit
    , base : Decimal
    , bonus : Decimal
    , each : Decimal
    , total : Decimal
    }


production : Unit -> GameData -> (UnitsSnapshot -> String -> Maybe Decimal) -> UnitsSnapshot -> Maybe Production
production parent gameData getVar snapshot =
    case parent.prod of
        Nothing ->
            Nothing

        Just ( id, prodExpr ) ->
            case GameData.unitByName (Unit.idToString id) gameData of
                Nothing ->
                    Nothing

                Just child ->
                    let
                        ctx =
                            Parser.Stat.evalFullWithDefault Decimal.zero (getVar snapshot) prodExpr

                        each =
                            ctx.out

                        base =
                            Dict.get "base" ctx.exports |> Maybe.withDefault Decimal.zero

                        bonus =
                            Dict.get "bonus" ctx.exports |> Maybe.withDefault Decimal.zero

                        total =
                            Decimal.mul each <| count parent.id snapshot
                    in
                    Just
                        { produces = child
                        , base = base
                        , bonus = bonus
                        , each = each
                        , total = total
                        }



-- Updates


mapCounts : (Dict String Decimal -> Dict String Decimal) -> UnitsSnapshot -> UnitsSnapshot
mapCounts fn (UnitsSnapshot data) =
    UnitsSnapshot { data | counts = fn data.counts }


insert : Unit.Id -> Decimal -> UnitsSnapshot -> UnitsSnapshot
insert id d =
    mapCounts <| Dict.insert (Unit.idToString id) d


remove : Unit.Id -> UnitsSnapshot -> UnitsSnapshot
remove id =
    mapCounts <| Dict.remove (Unit.idToString id)


update : Unit.Id -> (Maybe Decimal -> Maybe Decimal) -> UnitsSnapshot -> UnitsSnapshot
update id fn =
    mapCounts <| Dict.update (Unit.idToString id) fn


add : Unit.Id -> Decimal -> UnitsSnapshot -> UnitsSnapshot
add id =
    addName (Unit.idToString id)


addName : String -> Decimal -> UnitsSnapshot -> UnitsSnapshot
addName name d =
    mapCounts <| DecimalDict.add name d


mul : Unit.Id -> Decimal -> UnitsSnapshot -> UnitsSnapshot
mul id d =
    mapCounts <| DecimalDict.mul (Unit.idToString id) d


sub : Unit.Id -> Decimal -> UnitsSnapshot -> Result String UnitsSnapshot
sub id d (UnitsSnapshot data) =
    let
        name =
            Unit.idToString id
    in
    DecimalDict.sub ("not enough resources: " ++ name) name d data.counts
        |> Result.map (\counts -> UnitsSnapshot { data | counts = counts })
