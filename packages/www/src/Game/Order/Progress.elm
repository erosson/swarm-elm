module Game.Order.Progress exposing (DurationStats, OrderProgress, Progress, Status(..), duration, remaining, updates)

import Decimal exposing (Decimal)
import DecimalDict
import Dict exposing (Dict)
import Duration exposing (Duration)
import Game exposing (Game)
import Game.Order as Order exposing (Order)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import GameData.Item as Item exposing (Item)
import GameData.Unit as Unit exposing (Unit)
import Polynomial
import Time exposing (Posix)


type alias OrderProgress =
    List Progress


type alias Progress =
    { currency : Unit.Id
    , cost : Decimal
    , bank : Decimal
    , status : Status
    }


type Status
    = CostMet Float
    | CostNotMet Float (Maybe DurationStats)


type alias DurationStats =
    { from : Posix, now : Posix, total : Duration }


duration : Status -> Maybe DurationStats
duration status =
    case status of
        CostNotMet _ d ->
            d

        _ ->
            Nothing


remaining : DurationStats -> Duration
remaining { from, now, total } =
    Duration.sub total
        (Duration.since { before = from, after = now })
        |> Duration.max Duration.zero


calculate : Game -> UnitsSnapshot -> Order -> OrderProgress
calculate game snapshot order =
    update game snapshot order Nothing


updates : Game -> UnitsSnapshot -> List Order -> Dict String OrderProgress -> Dict String OrderProgress
updates game snapshot orders dict =
    List.foldl (\o -> Dict.update (Item.name o.item) (update game snapshot o >> Just)) dict orders


update : Game -> UnitsSnapshot -> Order -> Maybe OrderProgress -> OrderProgress
update game units order mprogress =
    let
        costs =
            if Decimal.gt (Item.resToDec order.untwinnedCount) Decimal.zero then
                order.cost

            else
                order.costNext

        progress2Line ( currency, cost ) =
            let
                progress0 : () -> Maybe Progress
                progress0 () =
                    mprogress
                        |> Maybe.withDefault []
                        |> List.filter (\p -> p.currency == currency)
                        |> List.head

                bank =
                    UnitsSnapshot.count currency units

                status =
                    -- Our decimal library may have rounding/floating-point errors.
                    -- Treat having almost-enough, off by `subEpsilon`, as good as
                    -- having actually-enough.
                    if order.isPossible || Decimal.gte bank (Decimal.mulFloat DecimalDict.subEpsilon cost) then
                        CostMet (Decimal.div cost bank |> Decimal.toFloat)

                    else
                        let
                            dur =
                                case progress0 () |> Maybe.map .status of
                                    Just (CostNotMet _ (Just d)) ->
                                        Just { d | now = UnitsSnapshot.at units }

                                    _ ->
                                        estimateDuration game units currency cost bank
                                            |> Maybe.map (DurationStats (UnitsSnapshot.at (Game.units game)) (UnitsSnapshot.at units))
                        in
                        CostNotMet (Decimal.div bank cost |> Decimal.toFloat) dur
            in
            { currency = currency
            , cost = cost
            , bank = bank
            , status = status
            }
    in
    List.map progress2Line costs


estimateDuration : Game -> UnitsSnapshot -> Unit.Id -> Decimal -> Decimal -> Maybe Duration
estimateDuration game units currency cost bank =
    let
        velocity =
            UnitsSnapshot.velocity currency units

        ns =
            { cost = cost, bank = bank, velocity = velocity }
    in
    case Game.polynomials game |> Dict.get (Unit.idToString currency) of
        Nothing ->
            estimateDurationLinear ns

        Just poly ->
            if List.length poly <= 2 then
                -- degree 0-1 polynomials get linear estimates
                estimateDurationLinear ns

            else
                -- nonlinear polynomials get polynomial estimates
                -- estimateDurationLinear ns
                estimateDurationBisect poly ns


estimateDurationLinear : { cost : Decimal, bank : Decimal, velocity : Decimal } -> Maybe Duration
estimateDurationLinear { cost, bank, velocity } =
    if Decimal.gt velocity Decimal.zero then
        Decimal.div (Decimal.sub cost bank) velocity
            |> Decimal.toFiniteFloat
            |> Maybe.map Duration.fromSecs

    else
        Nothing


{-| Use the bisection method to estimate durations for a nonlinear polynomial.

<https://en.wikipedia.org/wiki/Bisection_method#Algorithm>

Guess at possible times by evaluating the polynomial for each guess.
Start with a simple linear estimate, the maximum possible duration.
Bisect from zero until our guess is precise enough, or we've tried too many times.

-}
estimateDurationBisect poly ({ cost, velocity } as u) =
    let
        fn t =
            Polynomial.evaluate t poly |> Decimal.flipSub cost
    in
    estimateDurationBisectStep
        (Duration.fromMillis 100)
        fn
        50
        { bottom = Duration.zero

        -- , top = estimateDurationLinear u |> Maybe.withDefault (Duration.fromSecs <| 999 * 365 * 24 * 60 * 60 * 1000)
        , top = Duration.fromSecs <| 999 * 365 * 24 * 60 * 60 * 1000
        , fnBottom = fn Duration.zero
        }


estimateDurationBisectStep : Duration -> (Duration -> Decimal) -> Int -> { top : Duration, bottom : Duration, fnBottom : Decimal } -> Maybe Duration
estimateDurationBisectStep tolerance fn stepsLeft { top, bottom, fnBottom } =
    -- heavily based on pseudocode at https://en.wikipedia.org/wiki/Bisection_method#Algorithm
    -- evaluating a large polynomial is slow, so we parameterize/cache it as fnBottom to save some cpu
    if stepsLeft <= 0 then
        -- Too many steps!
        Nothing

    else
        let
            mid =
                (Duration.add top bottom |> Duration.toMillis |> toFloat) / 2 |> floor |> Duration.fromMillis

            diff =
                (Duration.sub top bottom |> Duration.toMillis |> toFloat) / 2 |> floor |> Duration.fromMillis

            fnMid =
                fn mid
        in
        if fnMid == Decimal.zero || Duration.toMillis diff < Duration.toMillis tolerance then
            Just mid

        else
            estimateDurationBisectStep tolerance fn (stepsLeft - 1) <|
                if Decimal.compare fnMid Decimal.zero == Decimal.compare fnBottom Decimal.zero then
                    { top = top, bottom = mid, fnBottom = fnMid }

                else
                    { top = mid, bottom = bottom, fnBottom = fnBottom }
