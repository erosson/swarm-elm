module Game.Tutorial exposing (Step(..), step, steps, toString)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Game exposing (Game)
import Game.UnitsSnapshot as UnitsSnapshot exposing (UnitsSnapshot)
import Maybe.Extra


type Step
    = FirstDrone
    | MoreDrones
    | FirstHatchery
    | FirstNotify
    | FirstQueen
    | MoreQueens
    | FirstTerritory
    | MoreTerritory
    | FirstExpansion
    | MoreExpansions
    | FirstAscension
    | FirstMutagen


steps =
    [ FirstDrone
    , MoreDrones
    , FirstHatchery
    , FirstNotify
    , FirstQueen
    , MoreQueens
    , FirstTerritory
    , MoreTerritory
    , FirstExpansion
    , MoreExpansions
    , FirstAscension
    , FirstMutagen
    ]


toString : Step -> String
toString s =
    case s of
        FirstDrone ->
            "firstDrone"

        MoreDrones ->
            "moreDrones"

        FirstHatchery ->
            "firstHatchery"

        FirstNotify ->
            "firstNotify"

        FirstQueen ->
            "firstQueen"

        MoreQueens ->
            "moreQueens"

        FirstTerritory ->
            "firstTerritory"

        MoreTerritory ->
            "moreTerritory"

        FirstExpansion ->
            "firstExpansion"

        MoreExpansions ->
            "moreExpansions"

        FirstAscension ->
            "firstAscension"

        FirstMutagen ->
            "firstMutagen"


step : { undo : Maybe Game, game : Game, units : UnitsSnapshot } -> Maybe Step
step { undo, game, units } =
    let
        unitsDict =
            UnitsSnapshot.toDict units

        upgradesDict =
            Game.upgradesDict game

        unit : String -> Float
        unit name =
            unitsDict |> Dict.get name |> Maybe.Extra.unwrap 0 Decimal.toFloat

        upgrade : String -> Float
        upgrade name =
            upgradesDict |> Dict.get name |> Maybe.Extra.unwrap 0 Decimal.toFloat

        undoUnitsDict =
            undo |> Maybe.map (Game.units >> UnitsSnapshot.toDict)

        undoUnit : String -> Float -> Float
        undoUnit name default =
            undoUnitsDict |> Maybe.Extra.unwrap default (Dict.get name >> Maybe.Extra.unwrap 0 Decimal.toFloat)
    in
    if unit "ascension" > 0 then
        if unit "ascension" == 1 && undoUnit "ascension" -1 == 0 then
            -- the player just ascended for the first time.
            -- using undoUnit means this message is only visible until the player's next purchase.
            Just FirstMutagen

        else
            -- except for just after the first ascension, there are no tutorial
            -- messages post-ascension
            Nothing

    else if unit "premutagen" > 0 && undoUnit "premutagen" -1 == 0 then
        -- the mutagen tab/ascension button was just unlocked for the first time
        Just FirstAscension

    else if upgrade "expansion" >= 5 then
        -- tutorial complete
        Nothing

    else if upgrade "expansion" > 0 then
        Just MoreExpansions

    else if unit "hatchery" > 1 then
        if unit "queen" >= 5 then
            if unit "territory" > 5 then
                Just FirstExpansion

            else if unit "territory" > 0 then
                Just MoreTerritory

            else
                Just FirstTerritory

        else if unit "queen" > 0 then
            Just MoreQueens

        else
            Just FirstQueen

    else if unit "drone" >= 10 then
        if unit "meat" >= 300 then
            Just FirstNotify

        else
            Just FirstHatchery

    else if unit "drone" > 0 then
        Just MoreDrones

    else
        Just FirstDrone
