module Game.OfflineSync exposing (ConflictData, ConflictState(..), create, isConflict, modalId, rawCreate, toConflict, toLocalOverwrite)

import Game exposing (Game)
import Maybe.Extra
import PlayFab.User as User exposing (User)
import RemoteData exposing (RemoteData)
import Session.GameLoader as GameLoader exposing (GameLoader)


modalId : String
modalId =
    "offlineSyncConflict"


type ConflictState
    = Conflict ConflictData
    | UseLocal
    | UseRemote SourceData


type alias ConflictData =
    { remote : SourceData, local : SourceData }


type alias SourceData =
    { game : Game, encoded : String }


toConflict : ConflictState -> Maybe ConflictData
toConflict state =
    case state of
        Conflict conflict ->
            Just conflict

        _ ->
            Nothing


isConflict : ConflictState -> Bool
isConflict =
    toConflict >> Maybe.Extra.isJust


toLocalOverwrite : ConflictState -> Maybe SourceData
toLocalOverwrite state =
    case state of
        UseRemote src ->
            Just src

        _ ->
            Nothing


create : RemoteData String (Maybe User) -> GameLoader -> Maybe String -> RemoteData String ConflictState
create remoteRes localRes =
    -- first thing: strip the GameLoader and User dependencies. This makes the rest of `create` unit-testable.
    -- The nested maybes are super awkward, but the distinction is important:
    -- * Success (Nothing): no user; not logged in
    -- * Success (Just (Nothing)): user with an empty saved-game; logged in
    rawCreate
        (remoteRes |> RemoteData.map (Maybe.map User.fullGame))
        (localRes |> GameLoader.toRemote)


{-| `create` after we've stripped the `GameLoader` and `User` dependencies. Exposed only for unit testing.
-}
rawCreate :
    RemoteData String (Maybe (Maybe ( RemoteData String Game, String )))
    -> RemoteData String Game
    -> Maybe String
    -> RemoteData String ConflictState
rawCreate remote localGame_ localExport_ =
    RemoteData.map2 tryResolve
        (createRemoteGame remote)
        (createLocalGame localGame_ localExport_)


createLocalGame : RemoteData String Game -> Maybe String -> RemoteData String (Maybe SourceData)
createLocalGame game mencoded_ =
    case mencoded_ of
        Nothing ->
            RemoteData.Success Nothing

        Just encoded_ ->
            RemoteData.map (\g -> Just { game = g, encoded = encoded_ }) game


createRemoteGame : RemoteData String (Maybe (Maybe ( RemoteData String Game, String ))) -> RemoteData String (Maybe SourceData)
createRemoteGame userRes =
    userRes
        |> RemoteData.andThen
            (\muser ->
                case muser of
                    Nothing ->
                        -- not logged in
                        RemoteData.Success Nothing

                    Just Nothing ->
                        -- logged in, but no game saved to this user
                        RemoteData.Success Nothing

                    Just (Just ( remoteGame_, encoded )) ->
                        case remoteGame_ of
                            RemoteData.NotAsked ->
                                -- shouldn't be possible - no save-state decodes to empty
                                RemoteData.Failure "user.game is empty"

                            RemoteData.Loading ->
                                RemoteData.Loading

                            RemoteData.Failure err ->
                                RemoteData.Failure err

                            RemoteData.Success game_ ->
                                RemoteData.Success <| Just { game = game_, encoded = encoded }
            )


tryResolve : Maybe SourceData -> Maybe SourceData -> ConflictState
tryResolve remote local =
    case ( remote, local ) of
        ( Nothing, Nothing ) ->
            -- Brand new visitor to the site: nothing remote, and the local game is new/not saved.
            UseLocal

        ( Nothing, Just lsrc ) ->
            -- Not logged in, or otherwise empty remote-save.
            UseLocal

        ( Just rsrc, Nothing ) ->
            -- Logged in on a new machine
            UseRemote rsrc

        ( Just rsrc, Just lsrc ) ->
            if rsrc.encoded == lsrc.encoded then
                -- identical games, use either, doesn't matter. The common case,
                -- when playing on a single machine or pressing refresh.
                UseLocal

            else if Game.created rsrc.game /= Game.created lsrc.game then
                -- if start-time is different, these games are definitely from different origins!
                -- human input is absolutely required.
                Conflict { remote = rsrc, local = lsrc }

            else if Game.stateDicts rsrc.game == Game.stateDicts lsrc.game then
                UseLocal

            else
                -- same creation-date, (probably) same origin game. They branched at some point.
                -- TODO examine the two games and see if we can merge them without the user's input. (is the branch linear?)
                -- Until we implement that, always assume it's a real conflict and ask for the user's input.
                Conflict { remote = rsrc, local = lsrc }
