module Game.Achievement exposing (Points, Status(..), decoder, earnedAt, emptyPoints, encoder, grant, isEarned, isHidden, isMasked, isVisible, points, progress, push, statuses)

import Decimal exposing (Decimal)
import Dict exposing (Dict)
import Game.Statistics as Statistics exposing (Statistics)
import GameData exposing (GameData)
import GameData.Achievement as Achievement exposing (Achievement)
import Json.Decode as D
import Json.Encode as E
import JsonUtil
import Maybe.Extra
import Parser.Visibility
import Time exposing (Posix)


type Status
    = Earned Posix (List Parser.Visibility.Progress)
    | Visible (List Parser.Visibility.Progress)
    | Masked
    | Hidden


isEarned : Status -> Bool
isEarned s =
    case s of
        Earned _ _ ->
            True

        _ ->
            False


earnedAt : Status -> Maybe Posix
earnedAt s =
    case s of
        Earned t _ ->
            Just t

        _ ->
            Nothing


isVisible : Status -> Bool
isVisible s =
    case s of
        Visible _ ->
            True

        _ ->
            False


isMasked : Status -> Bool
isMasked s =
    case s of
        Masked ->
            True

        _ ->
            False


isHidden : Status -> Bool
isHidden s =
    case s of
        Hidden ->
            True

        _ ->
            False


type alias Points =
    { val : Int, max : Int, percent : Float }


emptyPoints : Points
emptyPoints =
    Points 0 0 0


type alias Model m =
    { m
        | statistics : Statistics
        , gameData : GameData
        , upgrades : Dict String Decimal
        , achievements : Dict String Posix
        , newAchievements : Maybe ( Posix, Achievement, List Achievement )
        , achievementStatuses : List ( Achievement, Status )
        , achievementPoints : Points
    }


points : GameData -> Dict String Posix -> Points
points gameData dates =
    let
        val =
            GameData.achievements gameData
                |> List.filter (\a -> Dict.member (Achievement.name a) dates)
                |> List.map .points
                |> List.sum

        max =
            GameData.achievements gameData
                |> List.map .points
                |> List.sum
    in
    { val = val, max = max, percent = toFloat val / toFloat max }


getVar : Model a -> String -> Maybe Decimal
getVar game name =
    Dict.get name game.statistics.byUnit
        |> Maybe.map .twinnum
        -- |> Maybe.Extra.orElse (Dict.get name statistics.byUpgrade)
        |> Maybe.Extra.orElse (Dict.get name game.upgrades)


eval : Model a -> Parser.Visibility.Expr -> Bool
eval game =
    Parser.Visibility.evalWithDefault Decimal.zero (getVar game)


status : Model a -> Dict String Posix -> Achievement -> Status
status game dates achievement =
    case Dict.get (Achievement.name achievement) dates of
        Just date ->
            Earned date (progress game achievement)

        Nothing ->
            if eval game achievement.visible then
                Visible (progress game achievement)
                -- lines below are handy for inspecting achievement descriptions
                -- else if True then
                -- Visible (progress game achievement)

            else if achievement.points > 0 then
                Masked

            else
                Hidden


progress : Model a -> Achievement -> List Parser.Visibility.Progress
progress game achievement =
    case achievement.trigger of
        Achievement.Expr expr ->
            Parser.Visibility.progress (getVar game) expr

        _ ->
            []


statuses : Model a -> Dict String Posix -> List ( Achievement, Status )
statuses game dates =
    GameData.achievements game.gameData |> List.map (\a -> ( a, status game dates a ))


updateGame : Posix -> Model m -> Model m
updateGame now model =
    let
        new =
            GameData.achievements model.gameData
                |> List.filter (\a -> not <| Dict.member (Achievement.name a) model.achievements)
                |> List.filter (Achievement.triggerExpression >> Maybe.Extra.unwrap False (eval model))
    in
    grantList new now model


grantList : List Achievement -> Posix -> Model m -> Model m
grantList new now model =
    case new of
        [] ->
            model

        first :: rest ->
            let
                achievements =
                    new
                        |> List.map (\a -> ( Achievement.name a, now ))
                        |> Dict.fromList
                        |> Dict.union model.achievements
            in
            { model
                | achievements = achievements
                , newAchievements =
                    case model.newAchievements of
                        Nothing ->
                            Just ( now, first, rest )

                        Just ( time, first0, rest0 ) ->
                            Just ( time, first0, rest0 ++ new )
                , achievementPoints = points model.gameData achievements
                , achievementStatuses = statuses model achievements
            }


{-| Grant achievements relevant to this order.

TODO: we try to grant all achievements. Possible optimization: restrict to just
this order, for speed. (Doesn't feel necessary yet.)

-}
push : Statistics.Order o -> Posix -> Model m -> Model m
push =
    always updateGame


{-| Grant a single achievement by name. Event-based achievements.
-}
grant : String -> Posix -> Model m -> Maybe (Model m)
grant name now model =
    case GameData.achievementByName name model.gameData of
        Nothing ->
            Nothing

        Just achievement ->
            if Dict.member name model.achievements then
                Just model

            else
                Just <| grantList [ achievement ] now model


encoder : Dict String Posix -> E.Value
encoder =
    E.dict identity JsonUtil.posixEncoder


decoder : D.Decoder (Dict String Posix)
decoder =
    let
        decoder0 : Posix -> Dict String Int -> Dict String Posix
        decoder0 started dates =
            -- old-swarmsim encodes achievements as time-since-game-started; elm
            -- encodes them as posix timestamps. It's nice that the old format
            -- prevents achievement-dates from ever being before the game started -
            -- but it's not worth making them different from other posix dates.
            if dates |> Dict.values |> List.any (\earned -> Time.posixToMillis started > earned) then
                -- If any achievement was earned before this game started, it must be
                -- imported from old-swarmsim; update it.
                dates |> Dict.map (\_ -> (+) (Time.posixToMillis started) >> Time.millisToPosix)

            else
                dates |> Dict.map (\_ -> Time.millisToPosix)
    in
    D.map2 decoder0
        (D.oneOf
            [ D.at [ "date", "started" ] JsonUtil.posixDecoder
            , D.field "created" JsonUtil.posixDecoder
            ]
        )
        (D.field "achievements" (D.dict D.int))
