module Game.Theme exposing (Theme(..), decoder, encoder, list, toString)

import Dict exposing (Dict)
import Json.Decode as D
import Json.Encode as E


type Theme
    = Default
    | Cerulean
    | Cosmo
    | Cyborg
    | Darkly
    | Flatly
    | Journal
    | Litera
    | Lumen
    | Lux
    | Materia
    | Minty
    | Pulse
    | Sandstone
    | Simplex
    | Sketchy
    | Slate
    | Solar
    | Spacelab
    | Superhero
    | United
    | Yeti


list : List Theme
list =
    [ Default
    , Cerulean
    , Cosmo
    , Cyborg
    , Darkly
    , Flatly
    , Journal
    , Litera
    , Lumen
    , Lux
    , Materia
    , Minty
    , Pulse
    , Sandstone
    , Simplex
    , Sketchy
    , Slate
    , Solar
    , Spacelab
    , Superhero
    , United
    , Yeti
    ]


toString : Theme -> String
toString theme =
    case theme of
        Default ->
            "default"

        Cerulean ->
            "cerulean"

        Cosmo ->
            "cosmo"

        Cyborg ->
            "cyborg"

        Darkly ->
            "darkly"

        Flatly ->
            "flatly"

        Journal ->
            "journal"

        Litera ->
            "litera"

        Lumen ->
            "lumen"

        Lux ->
            "lux"

        Materia ->
            "materia"

        Minty ->
            "minty"

        Pulse ->
            "pulse"

        Sandstone ->
            "sandstone"

        Simplex ->
            "simplex"

        Sketchy ->
            "sketchy"

        Slate ->
            "slate"

        Solar ->
            "solar"

        Spacelab ->
            "spacelab"

        Superhero ->
            "superhero"

        United ->
            "united"

        Yeti ->
            "yeti"


byString : Dict String Theme
byString =
    list |> List.map (\theme -> ( toString theme, theme )) |> Dict.fromList


fromString : String -> Maybe Theme
fromString name =
    Dict.get name byString


encoder : Theme -> E.Value
encoder =
    toString >> E.string


decoder : D.Decoder Theme
decoder =
    let
        decodeName name =
            case fromString name of
                Just theme ->
                    D.succeed theme

                Nothing ->
                    D.fail ("no such theme: " ++ name)
    in
    D.string |> D.andThen decodeName
