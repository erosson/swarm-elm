module Game.ProductionGraph exposing
    ( Graph, graph, empty
    , polynomials, parentCoefficients
    , unitChildren, unitParents
    , toString, toDOTString, toDOTUrl, toTGFString
    )

{-| A graph representing unit production.

Used to create the polynomials describing unit count at any time `t`.
One polynomial per unit-type.
These polynomials are how we instantly calculate years of unit production!

The polynomials built by this graph are good until unit-counts are changed
outside of the polynomial - for example, buying a unit. Snapshot the polynomials'
output, apply whatever unit-count transforms you like (ex. buy the unit), and
use the result as the initial conditions for the next graph/polynomial-set.

Old-swarmsim didn't represent this problem as a graph, but as a chain of
dependencies. This way's cleaner, and supports fancy drawings
(though those drawings are really only good for debugging).


# Defintion

@docs Graph, graph, empty


# Evaluation

@docs polynomials, parentCoefficients


# Subgraphs

@docs unitChildren, unitParents


# Formatting and printing

@docs toString, toDOTString, toDOTUrl, toTGFString

-}

import Array exposing (Array)
import Decimal exposing (Decimal)
import Dict exposing (Dict)
import GameData exposing (GameData)
import GameData.Unit as Unit exposing (Unit)
import Graph
import Graph.DOT
import Graph.TGF
import Graph.Tree
import IntDict
import Maybe.Extra
import Parser.Stat
import Polynomial exposing (Polynomial)
import Set exposing (Set)
import Url


type alias Node =
    Graph.Node Unit


type alias Edge =
    Graph.Edge Decimal


type alias Graph =
    Graph.Graph Unit Decimal


type alias NodeContext =
    Graph.NodeContext Unit Decimal


graph : GameData -> (String -> Maybe Decimal) -> Graph
graph gd getVar =
    let
        units : List Unit
        units =
            gd |> GameData.units

        nodes : List Node
        nodes =
            units |> List.map (\u -> Graph.Node u.ordinal u)

        byName : Dict String Node
        byName =
            nodes |> List.map (\n -> ( Unit.name n.label, n )) |> Dict.fromList

        edge : Unit -> Maybe Edge
        edge parent =
            case parent.prod of
                Nothing ->
                    Nothing

                Just ( childId, prodExpr ) ->
                    case Dict.get (Unit.idToString childId) byName of
                        Nothing ->
                            Nothing

                        Just cnode ->
                            Parser.Stat.evalWithDefault Decimal.zero getVar prodExpr |> Graph.Edge parent.ordinal cnode.id |> Just

        edges : List Edge
        edges =
            units |> List.map edge |> List.filterMap identity
    in
    Graph.fromNodesAndEdges nodes edges


empty : Graph
empty =
    Graph.fromNodesAndEdges [] []


get : Unit -> Graph -> Maybe NodeContext
get unit =
    Graph.get unit.ordinal



-- traversal


type alias SimpleNodeVisitor acc =
    Graph.SimpleNodeVisitor Unit Float acc


{-| A subgraph containing all units produced by this unit, directly or indirectly.
-}
unitChildren : Unit -> Graph -> Graph
unitChildren u g =
    let
        nodeIds : List Graph.NodeId
        nodeIds =
            g |> Graph.dfsTree u.ordinal |> Graph.Tree.preOrderList |> List.map (.node >> .id)
    in
    g |> Graph.inducedSubgraph nodeIds


{-| A subgraph containing all units that produce this unit, directly or indirectly.
-}
unitParents : Unit -> Graph -> Graph
unitParents u =
    Graph.reverseEdges >> unitChildren u >> Graph.reverseEdges


{-| The edge-labels between the supplied nodes.
-}
path : Graph.NodeContext n e -> List (Graph.NodeContext n e) -> List e
path =
    let
        tailrec accum node nodes =
            case nodes of
                [] ->
                    accum |> List.filterMap identity

                parent :: ancestors ->
                    tailrec (IntDict.get parent.node.id node.outgoing :: accum) parent ancestors
    in
    tailrec []


paths : Graph.NodeContext n e -> List (Graph.NodeContext n e) -> List ( Graph.NodeContext n e, List e )
paths =
    let
        tailrec accum visited node unvisited =
            case unvisited of
                [] ->
                    accum

                parent :: ancestors ->
                    tailrec (( parent, path parent (node :: visited) ) :: accum) (node :: visited) parent ancestors
    in
    tailrec [] []


type alias ParentAccum =
    { path : List NodeContext, coeffs : Dict String (List ( Unit, List Decimal )) }


roots : Graph -> List Graph.NodeId
roots =
    let
        fold nc acc =
            if IntDict.isEmpty nc.incoming then
                nc.node.id :: acc

            else
                acc
    in
    Graph.fold fold []


{-| Coefficients for each possible child-ancestor pair. Used to construct polynomials.
-}
parentCoefficients : Graph -> Dict String (List ( Unit, List Decimal ))
parentCoefficients g =
    let
        visitor : Graph.DfsNodeVisitor Unit Decimal ParentAccum
        visitor nc acc0 =
            -- Run when the depth-first traversal *starts* this node:
            -- add it to the current path, and add all nodes in the current path
            ( { acc0
                | path = nc :: acc0.path
                , coeffs =
                    acc0.coeffs
                        |> Dict.update (Unit.name nc.node.label)
                            (Maybe.withDefault []
                                >> (++) (paths nc acc0.path |> List.map (Tuple.mapFirst (.node >> .label)))
                                >> Just
                            )
              }
              -- Run when the depth-first traversal *finishes* this node:
              -- remove it from the current path
            , \acc1 ->
                { acc1 | path = acc1.path |> List.drop 1 }
            )
    in
    -- TODO: we should be able to do this in one dfs traversal by having `visitor` modify visited nodes too!
    List.foldl
        (\root accum -> Graph.guidedDfs Graph.alongOutgoingEdges visitor [ root ] accum g |> Tuple.first)
        (ParentAccum [] Dict.empty)
        (roots g)
        |> .coeffs


fact : Int -> Int
fact =
    let
        loop accum n =
            if n <= 1 then
                accum

            else
                loop (accum * n) (n - 1)
    in
    loop 1


prune : Dict String Decimal -> Graph -> Graph
prune counts g =
    let
        visitor : Graph.DfsNodeVisitor Unit Decimal (Set Graph.NodeId)
        visitor nodeCtx preserved0 =
            -- node discovery order is unpredictible, and edges of processed
            -- nodes are removed from the graph - to get all edges, we must
            -- examine both incoming and outgoing nodes of every node.
            if
                -- have we processed one of this node's parents *before* discovery?
                Set.member nodeCtx.node.id preserved0
                    -- is this one of the initial nodes?
                    || Dict.member (Unit.name nodeCtx.node.label) counts
            then
                -- all outgoing nodes of any preserved node are also preserved.
                ( preserved0 |> Set.insert nodeCtx.node.id |> Set.union (nodeCtx.outgoing |> IntDict.keys |> Set.fromList), identity )

            else
                -- onFinish: add nodes with preserved parents to `preserved` after parents are processed
                ( preserved0
                , \preserved ->
                    -- have we processed one of this node's parents *after* discovery?
                    if nodeCtx.incoming |> IntDict.keys |> List.any (\k -> Set.member k preserved) then
                        preserved |> Set.insert nodeCtx.node.id |> Set.union (nodeCtx.outgoing |> IntDict.keys |> Set.fromList)

                    else
                        -- it's neither an initial-node or has one as an ancestor - pruned
                        preserved
                )

        preservedNodes =
            -- Graph.guidedDfs Graph.alongOutgoingEdges visitor (Graph.nodeIds g) Set.empty g
            Graph.guidedDfs Graph.alongIncomingEdges visitor (Graph.nodeIds g |> List.reverse) Set.empty g
                -- Graph.guidedDfs Graph.alongIncomingEdges visitor (Graph.nodeIds g) Set.empty g
                |> Tuple.first
                |> Set.toList
    in
    Graph.inducedSubgraph preservedNodes g


polynomials : Dict String Decimal -> Graph -> Dict String Polynomial
polynomials counts g0 =
    let
        g =
            prune counts g0

        getCount : String -> Decimal
        getCount unitName =
            Dict.get unitName counts |> Maybe.withDefault Decimal.zero

        toPoly : String -> List ( Unit, List Decimal ) -> List Decimal
        toPoly unit ks =
            let
                degreeCoeffs : List ( Int, Decimal )
                degreeCoeffs =
                    ks |> List.map toDegreeAndCoeff |> (::) ( 0, getCount unit )

                maxDegree =
                    degreeCoeffs |> List.map Tuple.first |> List.maximum |> Maybe.withDefault 0

                fold ( degree, coeff ) accum =
                    accum |> Array.set degree (coeff |> Decimal.add (accum |> Array.get degree |> Maybe.withDefault Decimal.zero))
            in
            degreeCoeffs
                |> List.foldl fold (Array.repeat (maxDegree + 1) Decimal.zero)
                |> Array.toList

        toDegreeAndCoeff : ( Unit, List Decimal ) -> ( Int, Decimal )
        toDegreeAndCoeff ( unit, ks ) =
            let
                degree =
                    List.length ks

                numerator =
                    List.foldl Decimal.mul Decimal.one (getCount (Unit.name unit) :: ks)
            in
            ( degree, numerator |> Decimal.flipDivFloat (toFloat <| fact degree) )
    in
    parentCoefficients g |> Dict.map toPoly



-- visualization


toString : Graph -> String
toString g =
    Graph.toString
        (.slug >> Just)
        (Decimal.toString >> Just)
        g


toTGFString : Graph -> String
toTGFString g =
    Graph.TGF.output
        .slug
        Decimal.toString
        g


toDOTString : Graph -> String
toDOTString g =
    let
        style0 =
            Graph.DOT.defaultStyles
    in
    Graph.DOT.outputWithStyles
        style0
        (.slug >> Just)
        (Decimal.toString >> Just)
        g


toDOTUrl : Graph -> String
toDOTUrl =
    -- https://package.elm-lang.org/packages/elm-community/graph/latest/Graph-DOT sent me here
    toDOTString >> Url.percentEncode >> (++) "https://dreampuf.github.io/GraphvizOnline/#"
