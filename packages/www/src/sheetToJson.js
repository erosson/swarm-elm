/**
 * Write public/gamedata.json and tests/GameDataFixture.elm based on the spreadsheet, public/gamedata.ods.
 *
 * This isn't used anywhere in the js pipeline - webpack processes the spreadsheet for us there.
 * This is only used for tests (the JSON fixture) and for the prod build (it's nice to expose a json file).
 */
const loader = require('@erosson/xlsx-loader').default
const fs = require('fs').promises

function main() {
  const json = JSON.parse(loader.call({
    resourcePath: './public/gamedata.ods',
  }))

  const jsonFile = fs.writeFile('./public/gamedata.json', JSON.stringify(json, null, 2))

  // Tests can't read json files, so write the json to an elm file.
  //
  // Why not write an elm file for use in prod too? - because the json port
  // already exists, and because external json forces me to store gamedata in
  // the model and avoid a funky global var.
  const fixtureFile = Promise.all([
    fs.readFile('./tests/GameDataFixture.elm.template', 'utf8'),
    fs.readFile('./public/lang/en-us.json', 'utf8'),
  ])
  .then(([tmpl, langTxt]) => {
    // Stringifying twice escapes the JSON string so it pastes into Elm cleanly.
    // Elm isn't JSON, but the escaping seems similar enough!
    const lang = JSON.parse(langTxt)
    const elm = tmpl
    .replace('%(gameData)s', JSON.stringify(JSON.stringify(json)))
    .replace('%(lang_en-us)s', JSON.stringify(JSON.stringify({en_us: lang})))
    // console.log(elm)
    return fs.writeFile('./tests/GameDataFixture.elm', elm)
  })

  Promise.all([jsonFile, fixtureFile])
  .then(() => {
    console.log('gamedata.ods processed')
  })
  .catch(console.error)
}

if (typeof require !== 'undefined' && require.main === module) {
  main()
}
