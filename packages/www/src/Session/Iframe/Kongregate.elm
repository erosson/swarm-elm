module Session.Iframe.Kongregate exposing
    ( GameAuthToken
    , Model
    , UserId
    , fromQuery
    , gameAuthTokenToFullString
    , gameAuthTokenToString
    , update
    , userIdToString
    )

{-| Kongregate.

<https://www.kongregate.com/games/swarmsim/swarm-simulator>
<https://www.kongregate.com/games/swarmsim/swarm-simulator-dev_preview>

Kongregate development's a little tricky these days, since <http://>
iframes aren't allowed and hosting <https://> locally is a pain.
To develop with a local server:

  - run `yarn start:https`
  - visit <https://localhost:3001>
  - allow the https cert
  - visit <https://www.kongregate.com/games/swarmsim/swarm-simulator-dev_preview>
    (it's aimed at <https://localhost:3001>)

-}

import Dict as Dict exposing (Dict)
import Ports.Kongregate exposing (LoginData)
import RemoteData exposing (RemoteData)
import Route.QueryDict as QueryDict exposing (QueryDict)


type alias Model =
    { api : RemoteData String ()
    , user : Maybe User
    }


type alias User =
    { userId : UserId
    , username : String
    , gameAuthToken : GameAuthToken
    , isGuest : Bool
    }


fromQuery : QueryDict -> Model
fromQuery query =
    { api = RemoteData.NotAsked
    , user =
        case
            ( Dict.get "kongregate_user_id" query
            , Dict.get "kongregate_username" query
            , Dict.get "kongregate_game_auth_token" query
            )
        of
            ( Just uid, Just uname, Just auth ) ->
                Just <| User (UserId uid) uname (GameAuthToken auth) (uid == "0")

            _ ->
                Nothing
    }


update : LoginData -> Model -> Model
update login _ =
    { api = RemoteData.Success ()
    , user = Just <| User (UserId login.userId) login.username (GameAuthToken login.gameAuthToken) login.isGuest
    }


type UserId
    = UserId String


type GameAuthToken
    = GameAuthToken String


userIdToString : UserId -> String
userIdToString (UserId id) =
    id


gameAuthTokenToString : GameAuthToken -> String
gameAuthTokenToString (GameAuthToken t) =
    String.left 5 t ++ "..." ++ String.right 5 t


gameAuthTokenToFullString : GameAuthToken -> String
gameAuthTokenToFullString (GameAuthToken t) =
    t
