module Session.Environment exposing (Environment(..), fromString, toString)

import Dict exposing (Dict)


type Environment
    = Production
    | Development
    | Unknown String


toString : Environment -> String
toString env =
    case env of
        Production ->
            "production"

        Development ->
            "development"

        Unknown str ->
            "unknown: " ++ str


byString =
    [ Production
    , Development
    ]
        |> List.map (\s -> ( toString s, s ))
        |> Dict.fromList


fromString : String -> Environment
fromString env =
    Dict.get env byString |> Maybe.withDefault (Unknown env)
