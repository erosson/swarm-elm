module Session.Iframe exposing (Iframe(..), fromQuery, mapKongregate, toString)

{-| Are we in an iframe on some other site?
-}

import Dict exposing (Dict)
import Maybe.Extra
import RemoteData exposing (RemoteData)
import Route.Config as Config
import Route.Feature as Feature
import Route.QueryDict as QueryDict exposing (QueryDict)
import Session.Iframe.Kongregate as Kongregate


type
    Iframe
    -- no iframe; independent page
    = WWW
      -- https://www.kongregate.com/games/swarmsim/swarm-simulator
      -- https://www.kongregate.com/games/swarmsim/swarm-simulator-dev_preview
    | Kongregate Kongregate.Model


toString : Iframe -> String
toString env =
    case env of
        WWW ->
            "www"

        Kongregate _ ->
            "kongregate"


kongregate : QueryDict -> Iframe
kongregate =
    Kongregate.fromQuery >> Kongregate


mapKongregate : (Kongregate.Model -> Kongregate.Model) -> Iframe -> Iframe
mapKongregate map iframe =
    case iframe of
        Kongregate kong ->
            Kongregate <| map kong

        _ ->
            iframe


fromQuery : QueryDict -> Iframe
fromQuery query =
    if Feature.isActive Feature.Kongregate query then
        kongregate query

    else
        case Config.get Config.Iframe query of
            Just "kongregate" ->
                kongregate query

            _ ->
                WWW
