module Session.GameLoader exposing (GameLoader, empty, fromGame, gameData, map, set, setJson, toMaybe, toRemote, tryReady, undo, undoDuration, undoState, undoableSet)

import Duration exposing (Duration)
import Game exposing (Game)
import GameData exposing (GameData)
import Json.Decode as D
import Maybe.Extra
import RemoteData exposing (RemoteData)
import Time exposing (Posix)


type GameLoader
    = NotReady (Result String GameData) (RemoteData String (Maybe D.Value))
    | Ready { game : Game, undo : Maybe ( Posix, Game ) }


empty : Result String GameData -> GameLoader
empty gd =
    -- NotAsked doesn't make sense here, we listen without asking.
    -- The difference isn't enough that it's worth making our own type, though.
    NotReady gd RemoteData.Loading


fromGame : Game -> GameLoader
fromGame game =
    Ready { game = game, undo = Nothing }


set : Game -> GameLoader -> GameLoader
set game loader =
    case loader of
        Ready g ->
            Ready { g | game = game }

        _ ->
            Ready { game = game, undo = Nothing }


undoableSet : Posix -> Game -> GameLoader -> GameLoader
undoableSet now game loader =
    case loader of
        Ready g ->
            Ready { game = game, undo = Just ( now, g.game ) }

        _ ->
            Ready { game = game, undo = Nothing }


map : (Game -> Game) -> GameLoader -> GameLoader
map fn loader =
    case loader of
        Ready g ->
            Ready { g | game = fn g.game }

        _ ->
            loader


undoState : GameLoader -> Maybe ( Posix, Game )
undoState loader =
    case loader of
        Ready g ->
            g.undo

        _ ->
            Nothing


undoSince : Posix -> GameLoader -> Maybe Duration
undoSince now =
    undoState >> Maybe.map (Tuple.first >> (\b -> Duration.since { before = b, after = now }))


undoStartDuration =
    Duration.fromSecs 30


undoDuration : Posix -> GameLoader -> Maybe Duration
undoDuration now loader =
    Maybe.map (Duration.sub undoStartDuration)
        (undoSince now loader)
        |> Maybe.Extra.filter (\dur -> Duration.toMillis dur >= 0)


undo : Posix -> GameLoader -> ( GameLoader, Cmd msg )
undo now loader =
    case ( loader, undoDuration now loader ) of
        ( Ready g, Just _ ) ->
            case g.undo of
                Just ( _, game ) ->
                    Game.persistWrite now game
                        |> Tuple.mapFirst (\pgame -> Ready { undo = Nothing, game = pgame })

                Nothing ->
                    ( loader, Cmd.none )

        _ ->
            ( loader, Cmd.none )


toRemote : GameLoader -> RemoteData String Game
toRemote loader =
    case loader of
        Ready { game } ->
            RemoteData.Success game

        NotReady (Err err) _ ->
            RemoteData.Failure err

        NotReady _ (RemoteData.Failure err) ->
            RemoteData.Failure err

        NotReady _ (RemoteData.Success val) ->
            -- Incomplete information - we've got the game json, but we need
            -- some more of its deps before it's a success
            RemoteData.Loading

        NotReady _ RemoteData.NotAsked ->
            RemoteData.NotAsked

        NotReady _ RemoteData.Loading ->
            RemoteData.Loading


toMaybe : GameLoader -> Maybe Game
toMaybe loader =
    case loader of
        Ready { game } ->
            Just game

        _ ->
            Nothing


gameData : GameLoader -> Result String GameData
gameData loader =
    case loader of
        Ready { game } ->
            Ok <| Game.gameData game

        NotReady gd _ ->
            gd


setJson : RemoteData String (Maybe D.Value) -> GameLoader -> GameLoader
setJson json loader =
    case loader of
        Ready { game } ->
            NotReady (Ok <| Game.gameData game) json |> tryReady (Just <| Game.snapshotted game)

        NotReady gd _ ->
            NotReady gd json


tryReady : Maybe Posix -> GameLoader -> GameLoader
tryReady now loader =
    case loader of
        NotReady (Ok gd) (RemoteData.Success maybeJson) ->
            case
                Maybe.map (Game.load gd)
                    now
            of
                Just load ->
                    case load maybeJson of
                        Ok game ->
                            Ready { game = game, undo = Nothing }

                        Err err ->
                            NotReady (Ok gd) <| RemoteData.Failure <| D.errorToString err

                _ ->
                    loader

        _ ->
            loader
