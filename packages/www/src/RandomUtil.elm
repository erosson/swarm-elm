module RandomUtil exposing (chance, decimal, nearDecimal, seedInt)

import Decimal exposing (Decimal)
import Random exposing (Generator)


chance : Float -> Generator Bool
chance threshold =
    Random.float 0 1 |> Random.map (\f -> f <= threshold)


decimal : Decimal -> Decimal -> Generator Decimal
decimal min max =
    let
        minExp =
            Decimal.exponent min

        maxExp =
            Decimal.exponent max
    in
    Random.int minExp maxExp
        |> Random.andThen
            (\exp ->
                let
                    sigMin =
                        if exp == minExp then
                            Decimal.significand min

                        else
                            1

                    sigMax =
                        if exp == maxExp then
                            Decimal.significand max

                        else
                            10
                in
                Random.float sigMin sigMax |> Random.map (\sig -> Decimal.fromSigExp sig exp)
            )


nearDecimal : Float -> Float -> Decimal -> Generator Decimal
nearDecimal mulMin mulMax d =
    decimal (Decimal.mulFloat mulMin d) (Decimal.mulFloat mulMax d)


seedInt : Generator Int
seedInt =
    Random.int (-1 * 2 ^ 31) (2 ^ 31)
