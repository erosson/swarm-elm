module PlayFab.User exposing
    ( Id
    , RememberId
    , Role(..)
    , SessionTicket
    , User
    , decodeGameReq
    , decoder
    , decoderConfig
    , defaultRole
    , displayName
    , email
    , encodedGame
    , fullGame
    , game
    , gameDecoder
    , generateRememberId
    , id
    , idToString
    , readUserSessionTicket
    , rememberId
    , rememberIdDecoder
    , rememberIdToFullString
    , rememberIdToString
    , role
    , sessionTicket
    , setGame
    , setRememberId
    , stateKey
    , ticketToString
    )

{-| PlayFab user accounts.

<https://developer.playfab.com/en-US/F810/players/80DB2CB7E5712462/overview>
<https://developer.playfab.com/en-US/F810/players/60E709970B8F87F2/overview>

-}

import Game exposing (Game)
import GameData exposing (GameData)
import Json.Decode as D
import Json.Decode.Pipeline as P
import Maybe.Extra
import PlayFab.ChunkedState as ChunkedState
import Ports
import RemoteData exposing (RemoteData)
import Session.Iframe as Iframe exposing (Iframe)
import Time exposing (Posix)


type User
    = User Data


type alias Data =
    { id : Id
    , sessionTicket : SessionTicket
    , rememberId : Maybe RememberId
    , role : Role
    , email : Maybe String
    , displayName : String

    -- Loading a user and decoding its game are two separate steps. The state below reflects that:
    -- * Nothing: This user account has no saved state at all. Its Playfab `userData` is empty.
    -- * Just (Pending, encoded): This user account has a save state, `encoded`. We've asked JS to decode it through a port, and we're waiting for its response.
    -- * Just (Success game, encoded): This user account has a save state, and we successfully decoded it.
    -- * Just (Failure err, encoded): This user account has a save state, but we failed to decode it!
    , game : Maybe ( RemoteData String Game, String )
    }


type Id
    = Id String


idToString : Id -> String
idToString (Id i) =
    i


{-| PlayFab session token. Unrelated to `Session.elm`. This is how we show the PlayFab API we're authed.
-}
type SessionTicket
    = SessionTicket String


ticketToString : SessionTicket -> String
ticketToString (SessionTicket t) =
    t


{-| "Remember me" token. Playfab CustomId.
-}
type RememberId
    = RememberId String


{-| An abbreviated rememberId. Safe for showing in the UI.

Never show the full rememberId string in the ui, even with debug on. Too easy for users to compromise it.

-}
rememberIdToString : RememberId -> String
rememberIdToString (RememberId r) =
    String.left 8 r ++ "..." ++ String.right 8 r


{-| A non-abbreviated rememberId. Used internally.

Never show this in the ui, even with debug on. Too easy for users to compromise it.

-}
rememberIdToFullString : RememberId -> String
rememberIdToFullString (RememberId r) =
    r


readUserSessionTicket : (( Maybe SessionTicket, Maybe RememberId ) -> msg) -> Sub msg
readUserSessionTicket toMsg =
    Ports.readUserSessionTicket
        (\res ->
            ( res.sessionTicket |> Maybe.map SessionTicket
            , res.rememberId |> Maybe.map RememberId
            )
                |> toMsg
        )


{-| Admin users can use the save editor on our debug page.

To grant admin access, set a user's UserReadOnlyData key named "role" to "admin" in the PlayFab admin UI.

Example: <https://developer.playfab.com/en-US/F810/players/80DB2CB7E5712462/data>

This field isn't related to the PlayFab website's auth system at all.

-}
type Role
    = PlayerRole
    | AdminRole


defaultRole : Role
defaultRole =
    PlayerRole


unwrap : User -> Data
unwrap (User d) =
    d


id : User -> Id
id =
    unwrap >> .id


email : User -> Maybe String
email =
    unwrap >> .email


sessionTicket : User -> SessionTicket
sessionTicket =
    unwrap >> .sessionTicket


rememberId : User -> Maybe RememberId
rememberId =
    unwrap >> .rememberId


setRememberId : Maybe RememberId -> User -> User
setRememberId rid (User user) =
    User { user | rememberId = rid }


generateRememberId : User -> RememberId
generateRememberId (User u) =
    -- public, but guarantees a namespace for our persistent logins.
    -- can't accidentally give two accounts the same id.
    [ idToString u.id

    -- private; can't be guessed by account thieves. This makes rememberIds
    -- private/unguessable too.
    -- sessionids are already prefixed with the player id, but there's no
    -- guarantee playfab will always do it that way, so I'll do it myself.
    -- (Not faulting them; I'd expect no such guarantee.)
    , ticketToString u.sessionTicket |> String.reverse
    ]
        |> String.join ":::"
        -- playfab's maximum customid size
        |> String.slice 0 100
        |> RememberId


role : User -> Role
role =
    unwrap >> .role


displayName : Iframe -> User -> String
displayName iframe =
    case iframe of
        Iframe.Kongregate m ->
            case m.user of
                Just user ->
                    always user.username

                Nothing ->
                    unwrap >> .displayName

        Iframe.WWW ->
            unwrap >> .displayName


decodeGameReq : User -> ( User, Cmd msg )
decodeGameReq (User u) =
    case u.game of
        Just ( RemoteData.NotAsked, encoded ) ->
            ( User { u | game = Just ( RemoteData.Loading, encoded ) }
            , Ports.playfabDecodeSaveReq
                { userId = u.id |> idToString
                , encoded = encoded
                }
            )

        _ ->
            ( User u, Cmd.none )


gameDecoder : GameData -> Posix -> D.Decoder ( Id, Game )
gameDecoder gd now =
    let
        builder status =
            case status of
                "empty" ->
                    D.fail "decoded empty user.game, but that should've been dealt with earlier"

                "success" ->
                    D.map2 Tuple.pair
                        (D.field "userId" (D.string |> D.map Id))
                        (D.field "value" <| Game.decoder gd now)

                "error" ->
                    -- let the decoder handle error context
                    D.fail "user.game javascript error"

                _ ->
                    D.fail <| "persistRead javascript unknown-status: " ++ status
    in
    D.field "status" D.string |> D.andThen builder


game : User -> RemoteData String Game
game =
    unwrap >> .game >> Maybe.Extra.unwrap RemoteData.NotAsked Tuple.first


encodedGame : User -> Maybe String
encodedGame =
    unwrap >> .game >> Maybe.map Tuple.second


fullGame : User -> Maybe ( RemoteData String Game, String )
fullGame =
    unwrap >> .game


setGame : Result String Game -> User -> User
setGame mgame (User u) =
    case u.game of
        Just ( RemoteData.Loading, encoded ) ->
            case mgame of
                Err err ->
                    User { u | game = Just ( RemoteData.Failure err, encoded ) }

                Ok game_ ->
                    User { u | game = Just ( RemoteData.Success game_, encoded ) }

        _ ->
            User u



-- serialization


decoderConfig =
    { ticket = Nothing, email = Nothing }


stateKey : String
stateKey =
    "elm-state"


decoder : { ticket : Maybe SessionTicket, email : Maybe String } -> D.Decoder User
decoder r =
    D.succeed Data
        |> P.requiredAt [ "data", "PlayFabId" ] idDecoder
        |> (case r.ticket of
                Nothing ->
                    P.requiredAt [ "data", "SessionTicket" ] sessionTicketDecoder

                -- DEBUG: the lines below simulate a session expiring after login
                -- Just (SessionTicket t) ->
                -- P.hardcoded (SessionTicket <| "xxx" ++ t)
                Just t ->
                    P.hardcoded t
           )
        |> P.optionalAt [ "data", "InfoResultPayload", "AccountInfo", "CustomIdInfo", "CustomId" ] (rememberIdDecoder |> D.map Just) Nothing
        |> P.optionalAt [ "data", "InfoResultPayload", "UserReadOnlyData", "role", "Value" ] roleDecoder PlayerRole
        |> P.optionalAt [ "data", "InfoResultPayload", "AccountInfo", "PrivateInfo", "Email" ] (D.string |> D.map Just) r.email
        |> P.custom
            (D.oneOf
                [ D.at [ "data", "InfoResultPayload", "AccountInfo", "PrivateInfo", "Email" ] D.string
                , D.at [ "data", "InfoResultPayload", "AccountInfo", "Username" ] D.string
                , r.email |> Maybe.Extra.unwrap (D.fail "no hardcoded email") D.succeed
                , D.at [ "data", "PlayFabId" ] D.string
                ]
            )
        -- |> P.optionalAt [ "data", "InfoResultPayload", "UserData", "state", "Value" ] (D.string |> D.map (Tuple.pair RemoteData.NotAsked >> Just)) Nothing
        |> P.custom
            (ChunkedState.decoder stateKey [ "Value" ]
                |> D.map (Tuple.pair RemoteData.NotAsked)
                |> D.at [ "data", "InfoResultPayload", "UserData" ]
                |> D.maybe
            )
        -- |> D.map (Debug.log "user-decoder")
        |> D.map User


idDecoder : D.Decoder Id
idDecoder =
    D.string |> D.map Id


rememberIdDecoder : D.Decoder RememberId
rememberIdDecoder =
    D.string |> D.map RememberId


sessionTicketDecoder : D.Decoder SessionTicket
sessionTicketDecoder =
    D.string |> D.map SessionTicket


roleDecoder : D.Decoder Role
roleDecoder =
    D.string
        |> D.andThen
            (\str ->
                case str of
                    "admin" ->
                        D.succeed AdminRole

                    "player" ->
                        D.succeed PlayerRole

                    other ->
                        D.fail <| "invalid role: " ++ other
            )
