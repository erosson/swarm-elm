module PlayFab.TitleId exposing (TitleId, devSite, encode, fromSession, fromString, host, toString)

import Json.Encode as E
import Route.Config as Config
import Session exposing (Session)
import Session.Environment as Environment exposing (Environment)


type TitleId
    = TitleId String


endpoints =
    { prod = TitleId "7847"
    , dev = TitleId "F810"
    }


toString : TitleId -> String
toString =
    host


fromString : Environment -> Maybe String -> TitleId
fromString env str =
    case str of
        Just "prod" ->
            endpoints.prod

        Just "7847" ->
            endpoints.prod

        Just "dev" ->
            endpoints.dev

        Just "F810" ->
            endpoints.dev

        _ ->
            case env of
                Environment.Production ->
                    endpoints.prod

                _ ->
                    endpoints.dev


fromSession : Session -> TitleId
fromSession session =
    session |> Session.query |> Config.get Config.PlayFab |> fromString (Session.environment session)


host : TitleId -> String
host (TitleId id) =
    "https://" ++ id ++ ".playfabapi.com"


devSite : TitleId -> String
devSite (TitleId id) =
    "https://developer.playfab.com/en-US/" ++ id


encode : TitleId -> E.Value
encode (TitleId id) =
    E.string id
