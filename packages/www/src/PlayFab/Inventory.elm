module PlayFab.Inventory exposing (InstanceId, Inventory, Item, ItemId, decoder, instanceIdToString, itemIdToString, paypalTxnId)

import Iso8601
import Json.Decode as D
import Time exposing (Posix)


type alias Inventory =
    List Item


type ItemId
    = ItemId String


itemIdToString : ItemId -> String
itemIdToString (ItemId id) =
    id


type InstanceId
    = InstanceId String


instanceIdToString : InstanceId -> String
instanceIdToString (InstanceId id) =
    id


type alias Item =
    { itemId : ItemId
    , instanceId : InstanceId
    , purchaseDate : Posix
    , displayName : String
    , annotation : String
    , catalogVersion : String
    }


paypalTxnId : Item -> String
paypalTxnId =
    .annotation >> String.replace "paypal tx=" ""


decoder : D.Decoder Item
decoder =
    D.map6 Item
        (D.field "ItemId" <| D.map ItemId D.string)
        (D.field "ItemInstanceId" <| D.map InstanceId D.string)
        (D.field "PurchaseDate" Iso8601.decoder)
        (D.field "DisplayName" D.string)
        (D.field "Annotation" D.string)
        (D.field "CatalogVersion" D.string)
