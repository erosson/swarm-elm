module PlayFab.Error exposing (Error(..), FormErrorData, toMessages, toString)

import Dict exposing (Dict)


type
    Error
    -- Report these! I'm not expecting to ever see them.
    = UnexpectedError String
      -- Don't report these. The user's session has expired - it used to be valid,
      -- but isn't anymore. Playfab sessions expire after 24 hours. The user must
      -- log in again (or we do that for them, if they checked remember-me).
    | AuthError Int String
      -- Don't report these. They'll go away if we try again.
    | TransientError String
      -- Don't report these. User entered wrong password, etc.
    | FormError FormErrorData


type alias FormErrorData =
    { error : String, fields : Dict String (List String) }


toMessages : Maybe Error -> List String
toMessages error =
    case error of
        Just (FormError e) ->
            [ e.error ]

        Just (UnexpectedError e) ->
            [ e ]

        Just (TransientError e) ->
            [ e ]

        Just (AuthError status e) ->
            [ "AuthError " ++ String.fromInt status ++ ": " ++ e ]

        Nothing ->
            []


toString : Error -> String
toString =
    Just >> toMessages >> String.join "\n"
