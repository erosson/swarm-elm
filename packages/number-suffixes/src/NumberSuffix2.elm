module NumberSuffix2 exposing (format, formatInt, formatSigExp, formatString)

import NumberSuffix2.Config as Config exposing (Config)
import NumberSuffix2.Locale as Locale exposing (Locale)
import NumberSuffix2.Parsed as Parsed exposing (Parsed)


format : Config -> Float -> String
format cfg =
    Parsed.parseFloat cfg >> toString cfg


formatInt : Config -> Int -> String
formatInt cfg =
    Parsed.parseInt cfg >> toString cfg


formatSigExp : Config -> Float -> Int -> String
formatSigExp cfg sig =
    Parsed.parseSigExp cfg sig >> toString cfg


formatString : Config -> String -> Maybe String
formatString cfg =
    Parsed.parseString cfg.locale >> Maybe.map (toString cfg)


toString : Config -> Parsed -> String
toString cfg p =
    if p.exp >= cfg.minSuffixExp then
        toSuffixedString cfg p

    else
        toUnsuffixedString cfg p


toUnsuffixedString : Config -> Parsed -> String
toUnsuffixedString cfg p =
    let
        numWholeDigits : Int
        numWholeDigits =
            1 + p.exp

        wholeDigits : String
        wholeDigits =
            p.digits |> String.padRight numWholeDigits '0' |> String.left numWholeDigits
    in
    wholeDigits |> separateThousands cfg |> sign cfg.locale p


toSuffixedString : Config -> Parsed -> String
toSuffixedString cfg p =
    let
        exp : Int
        exp =
            (p.exp // cfg.suffixDivisor) * cfg.suffixDivisor

        numWholeDigits : Int
        numWholeDigits =
            1 + p.exp - exp

        digits =
            significantDigits cfg numWholeDigits p.digits

        sig : String
        sig =
            if numWholeDigits >= String.length digits then
                digits |> String.padRight numWholeDigits '0'

            else
                String.left numWholeDigits digits
                    ++ cfg.locale.decimalSeparator
                    ++ (digits |> String.dropLeft numWholeDigits)
    in
    sign cfg.locale p sig ++ cfg.getSuffix p.exp


separateThousands : Config -> String -> String
separateThousands cfg =
    -- TODO localeify thousands groupings
    splitLengthRight 3 >> String.join cfg.locale.thousandSeparator


splitLengthRight : Int -> String -> List String
splitLengthRight len =
    let
        loop : List String -> String -> List String
        loop accum s =
            if String.length s <= len then
                s :: accum

            else
                loop (String.left len s :: accum) (String.dropLeft len s)
    in
    String.reverse >> loop [] >> List.map String.reverse


significantDigits : Config -> Int -> String -> String
significantDigits cfg numWholeDigits digits =
    -- parsed digits include no trailing zeros (even in whole numbers).
    -- Insert those zeros for correct output.
    digits
        |> String.padRight (max numWholeDigits cfg.sigfigs) '0'
        |> String.left cfg.sigfigs


sign : Locale -> Parsed -> String -> String
sign loc p s =
    if p.neg then
        loc.negativePrefix ++ s ++ loc.negativeSuffix

    else
        loc.positivePrefix ++ s ++ loc.positiveSuffix
