module NumberSuffix2.Locale exposing (Locale, frenchLocale, spanishLocale, usLocale)

{-| Localization configuration..

Based on cuducos/elm-format-number: `FormatNumber.Locales`.

-}


type alias Locale =
    { decimals : Int
    , thousandSeparator : String
    , decimalSeparator : String
    , negativePrefix : String
    , negativeSuffix : String
    , positivePrefix : String
    , positiveSuffix : String
    }


usLocale : Locale
usLocale =
    { decimals = 2
    , thousandSeparator = ","
    , decimalSeparator = "."
    , negativePrefix = "-"
    , negativeSuffix = ""
    , positivePrefix = ""
    , positiveSuffix = ""
    }


frenchLocale : Locale
frenchLocale =
    { decimals = 3
    , thousandSeparator = " "
    , decimalSeparator = ","
    , negativePrefix = "-"
    , negativeSuffix = ""
    , positivePrefix = ""
    , positiveSuffix = ""
    }


spanishLocale : Locale
spanishLocale =
    { decimals = 3
    , thousandSeparator = "."
    , decimalSeparator = ","
    , negativePrefix = "-"
    , negativeSuffix = ""
    , positivePrefix = ""
    , positiveSuffix = ""
    }
