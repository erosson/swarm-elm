module NumberSuffix2.Round exposing (round, sigfigsToPrecision)

{-| Utilities for rounding formatted numbers with `elm-round`.

`elm-round` works with precision - round to the nth digit from the decimal place.
`number-suffixes` works with significant figures - round to the nth largest digit.
This file converts between the two.

-}

import NumberSuffix2.Config as Config exposing (Config, standardConfig)
import NumberSuffix2.Parsed as Parsed exposing (Parsed)


{-| Convert sigfigs to precision for working with elm-round.

This may be off by one, depending on which way we round. Should be good enough.

-}
sigfigsToPrecision : Int -> Float -> Int
sigfigsToPrecision sigfigs val =
    let
        { exp } =
            Parsed.parseFloat standardConfig val
    in
    sigfigs - exp


round : (Int -> Float -> String) -> Int -> Float -> String
round roundFromDecimal sigfigs val =
    roundFromDecimal (sigfigsToPrecision sigfigs val - 1) val
