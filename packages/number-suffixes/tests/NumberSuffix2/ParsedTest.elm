module NumberSuffix2.ParsedTest exposing (all)

import Expect
import NumberSuffix2.Config as C
import NumberSuffix2.Locale as L
import NumberSuffix2.Parsed as P
import Test exposing (..)


parseFloat =
    P.parseFloat C.standardConfig


parseString =
    P.parseString L.usLocale


all : Test
all =
    describe "Parsed"
        [ test "3" <| \_ -> parseFloat 3 |> Expect.equal { neg = False, digits = "3", exp = 0 }
        , test "0" <| \_ -> parseFloat 0 |> Expect.equal { neg = False, digits = "0", exp = 0 }
        , test "-0" <| \_ -> parseString "-0" |> Expect.equal (Just { neg = False, digits = "0", exp = 0 })
        , test "-0.00000" <| \_ -> parseString "-0.00000" |> Expect.equal (Just { neg = False, digits = "0", exp = 0 })
        , test "-3" <| \_ -> parseFloat -3 |> Expect.equal { neg = True, digits = "3", exp = 0 }
        , test "33" <| \_ -> parseFloat 33 |> Expect.equal { neg = False, digits = "33", exp = 1 }
        , test "33.33" <| \_ -> parseFloat 33.33 |> Expect.equal { neg = False, digits = "3333", exp = 1 }
        , test "123e45" <| \_ -> parseFloat 1.23e47 |> Expect.equal { neg = False, digits = "123", exp = 47 }
        , test "123e45 (s)" <| \_ -> parseString "123e45" |> Expect.equal (Just { neg = False, digits = "123", exp = 47 })
        , test "-12.3e45" <| \_ -> parseFloat -1.23e46 |> Expect.equal { neg = True, digits = "123", exp = 46 }
        , test "-12.3e45 (s)" <| \_ -> parseString "-12.3e45" |> Expect.equal (Just { neg = True, digits = "123", exp = 46 })
        , test "3.00000" <| \_ -> parseString "3.00000" |> Expect.equal (Just { neg = False, digits = "3", exp = 0 })
        , test "3e6 (no-e)" <| \_ -> parseString "3000000" |> Expect.equal (Just { neg = False, digits = "3", exp = 6 })
        , test "3e6 (e)" <| \_ -> parseString "3e6" |> Expect.equal (Just { neg = False, digits = "3", exp = 6 })
        , test "no-commas" <| \_ -> P.parseString L.usLocale "123456.78" |> Expect.equal (Just { neg = False, digits = "12345678", exp = 5 })
        , test "commas (usa)" <| \_ -> P.parseString L.usLocale "123,456.78" |> Expect.equal (Just { neg = False, digits = "12345678", exp = 5 })
        , test "commas (non-usa)" <| \_ -> P.parseString L.spanishLocale "123.456,78" |> Expect.equal (Just { neg = False, digits = "12345678", exp = 5 })
        ]
