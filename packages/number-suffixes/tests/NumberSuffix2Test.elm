module NumberSuffix2Test exposing (all)

import Expect
import NumberSuffix2 as N
import NumberSuffix2.Config as C exposing (scientificConfig, standardConfig)
import NumberSuffix2.Locale as L
import Test exposing (..)


all : Test
all =
    describe "NumberSuffix2"
        [ test "3" <| \_ -> N.format standardConfig 3 |> Expect.equal "3"
        , test "0" <| \_ -> N.format standardConfig 0 |> Expect.equal "0"
        , test "3e6" <| \_ -> N.format standardConfig 3.0e6 |> Expect.equal "3.00 million"
        , test "300e6" <| \_ -> N.format standardConfig 3.0e8 |> Expect.equal "300 million"
        , test "-3e6" <| \_ -> N.format standardConfig -3.0e6 |> Expect.equal "-3.00 million"
        , test "3.33e6" <| \_ -> N.format standardConfig 3.33e6 |> Expect.equal "3.33 million"
        , test "-3.33e6" <| \_ -> N.format standardConfig -3.33e6 |> Expect.equal "-3.33 million"
        , test "3.33e6 (non-usa)" <| \_ -> N.formatString { standardConfig | locale = L.spanishLocale } "3,33e6" |> Expect.equal (Just "3,33 million")
        , test "3e66" <| \_ -> N.format standardConfig 3.0e66 |> Expect.equal "3.00 unvigintillion"
        , test "-3e66" <| \_ -> N.format standardConfig -3.0e66 |> Expect.equal "-3.00 unvigintillion"
        , test "3.33e66" <| \_ -> N.format standardConfig 3.33e66 |> Expect.equal "3.33 unvigintillion"
        , test "3.33e66 (non-usa)" <| \_ -> N.formatString { standardConfig | locale = L.spanishLocale } "3,33e66" |> Expect.equal (Just "3,33 unvigintillion")
        , test "3e66 (sci)" <| \_ -> N.format scientificConfig 3.0e66 |> Expect.equal "3.00e66"
        , test "-3e66 (sci)" <| \_ -> N.format scientificConfig -3.0e66 |> Expect.equal "-3.00e66"
        ]
