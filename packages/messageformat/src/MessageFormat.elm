module MessageFormat exposing
    ( Message, parse
    , format, formats
    , codegen, codegens
    )

{-| A subset of the ICU MessageFormat.

Based on:

<http://userguide.icu-project.org/formatparse/messages#TOC-MessageFormat>

<http://icu-project.org/apiref/icu4j/com/ibm/icu/text/MessageFormat.html>

This module implements plural forms, keyword selection, and quoting special characters. It notably does not support:

  - the "offset" plural clause (yet)
  - the "few" and "many" plural keywords (yet)
  - number formatting - no plans to add this. We can't do so type-safely, and
    number formatting can be too finicky a beast to try to generalize here.
    Format your numbers _before_ calling this module.


# Definition and parsing

@docs Message, parse


# Type-unsafe message formatting

@docs format, formats


# Type-safe code generation

@docs codegen, codegens

-}

-- for testing by hand: https://format-message.github.io/icu-message-format-for-translators/editor.html
-- TODO number formatting based on http://icu-project.org/apiref/icu4j/com/ibm/icu/number/NumberFormatter.html ?

import Char
import Dict exposing (Dict)
import Parser as P exposing ((|.), (|=), Parser)
import Result.Extra
import Set exposing (Set)


type Message
    = Message (List Token)


type Token
    = Text String
    | Var String
    | Plural String (Dict String Message) Message
    | Select String (Dict String Message) Message



-- Parsing


parse : String -> Result String Message
parse msg =
    P.run (message True |. P.end) msg
        -- |> Debug.log ("messageformat.parse: `" ++ msg ++ "`")
        -- |> Result.mapError Debug.toString
        |> Result.mapError P.deadEndsToString


message : Bool -> Parser Message
message isTopLevel =
    let
        push tokens token =
            token :: tokens |> P.Loop

        loop tokens =
            P.oneOf
                [ P.succeed (push tokens)
                    |= argument isTopLevel
                , P.succeed (push tokens)
                    |= quotedText
                , P.succeed (push tokens)
                    |= unquotedText
                , P.succeed (tokens |> List.reverse |> Message |> P.Done)
                ]
    in
    P.loop [] loop


unquotedText : Parser Token
unquotedText =
    let
        pred c =
            c /= '\'' && c /= '{' && c /= '}' && c /= '#'
    in
    P.succeed ()
        |. P.chompIf pred
        |. P.chompWhile pred
        |> P.getChompedString
        |> P.map Text


quotedText : Parser Token
quotedText =
    let
        loop strings =
            P.oneOf
                [ P.succeed (\_ -> "'" :: strings |> P.Loop)
                    |= P.symbol "''"
                , P.succeed (\str -> str :: strings |> P.Loop)
                    |= (P.getChompedString <|
                            P.succeed ()
                                |. P.chompIf (\c -> c /= '\'')
                                |. P.chompWhile (\c -> c /= '\'')
                       )
                , P.succeed
                    (\_ ->
                        strings
                            |> List.reverse
                            |> String.join ""
                            -- special case: empty quotedText - '' - escapes to a single ', just like a double-apostrophe inside 'quotes'.
                            -- "A pair of adjacent apostrophes always results in a single apostrophe in the output, even when the pair is between two single, text-quoting apostrophes":
                            -- http://icu-project.org/apiref/icu4j/com/ibm/icu/text/MessagePattern.ApostropheMode.html
                            |> (\s ->
                                    if s == "" then
                                        "'"

                                    else
                                        s
                               )
                            |> P.Done
                    )
                    |= P.symbol "'"
                ]
    in
    P.succeed Text
        |. P.symbol "'"
        |= P.loop [] loop


argument : Bool -> Parser Token
argument isTopLevel =
    P.oneOf
        [ (if isTopLevel then
            P.problem "invalid outer `#`"

           else
            P.succeed (Var "#")
          )
            |. P.symbol "#"
        , P.succeed (\var tokenFn -> tokenFn var)
            |. P.symbol "{"
            |. P.spaces
            |= P.variable
                { start = Char.isAlpha
                , inner = \c -> Char.isAlphaNum c || c == '_' || c == '.'
                , reserved = Set.fromList [ "plural", "select", "offset" ]
                }
            |. P.spaces
            |= P.oneOf
                [ P.succeed Var
                    |. P.symbol "}"
                , P.succeed identity
                    |. P.symbol ","
                    |. P.spaces
                    |= P.oneOf
                        [ P.succeed (\( cases, other ) key -> Plural key (cases |> List.map (Tuple.mapFirst String.fromInt) |> Dict.fromList) other)
                            |. P.keyword "plural"
                            |. P.spaces
                            |. P.symbol ","
                            |. P.spaces
                            |= pluralCases
                            |. P.spaces
                            |. P.symbol "}"
                        , P.succeed (\( cases, other ) key -> Select key (cases |> Dict.fromList) other)
                            |. P.keyword "select"
                            |. P.spaces
                            |. P.symbol ","
                            |. P.spaces
                            |= selectCases
                            |. P.spaces
                            |. P.symbol "}"
                        ]
                ]
        ]


selectCases : Parser ( List ( String, Message ), Message )
selectCases =
    let
        loop cases =
            P.oneOf
                [ P.succeed (\other -> ( cases |> List.reverse, other ) |> P.Done)
                    |. P.keyword "other"
                    |. P.spaces
                    |. P.symbol "{"
                    |= P.lazy (\() -> message False)
                    |. P.symbol "}"
                    |. P.spaces
                , P.succeed (\key msg -> ( key, msg ) :: cases |> P.Loop)
                    |= P.variable
                        { start = Char.isAlphaNum
                        , inner = \c -> Char.isAlphaNum c || c == '_' || c == '.'
                        , reserved = Set.fromList [ "other" ]
                        }
                    |. P.spaces
                    |. P.symbol "{"
                    |= P.lazy (\() -> message False)
                    |. P.symbol "}"
                    |. P.spaces
                ]
    in
    P.loop [] loop


pluralCases : Parser ( List ( Int, Message ), Message )
pluralCases =
    let
        loop cases =
            P.oneOf
                [ P.succeed (\c -> c :: cases |> P.Loop)
                    |= pluralCase
                    |. P.spaces
                , P.succeed (\other -> ( cases |> List.reverse, other ) |> P.Done)
                    |. P.keyword "other"
                    |. P.spaces
                    |. P.symbol "{"
                    |= P.lazy (\() -> message False)
                    |. P.symbol "}"
                ]
    in
    P.loop [] loop


pluralCase : Parser ( Int, Message )
pluralCase =
    P.oneOf
        [ P.succeed (Tuple.pair 0)
            |. P.keyword "zero"
            |. P.spaces
            |. P.symbol "{"
            |= P.lazy (\() -> message False)
            |. P.symbol "}"
        , P.succeed (Tuple.pair 1)
            |. P.keyword "one"
            |. P.spaces
            |. P.symbol "{"
            |= P.lazy (\() -> message False)
            |. P.symbol "}"
        , P.succeed (Tuple.pair 2)
            |. P.keyword "two"
            |. P.spaces
            |. P.symbol "{"
            |= P.lazy (\() -> message False)
            |. P.symbol "}"
        , P.succeed Tuple.pair
            |. P.symbol "="
            |= P.int
            |. P.spaces
            |. P.symbol "{"
            |= P.lazy (\() -> message False)
            |. P.symbol "}"
        ]



-- Type-unsafe formatting


format : (String -> Result String String) -> Message -> Result String String
format getVar (Message tokens) =
    let
        eval : Token -> Result String String
        eval token =
            case token of
                Text text ->
                    Ok text

                Var key ->
                    getVar key

                Plural key cases other ->
                    getVar key
                        |> Result.andThen
                            (\n ->
                                format
                                    (\nextKey ->
                                        if nextKey == "#" then
                                            Ok n

                                        else
                                            getVar nextKey
                                    )
                                    (Dict.get n cases |> Maybe.withDefault other)
                            )

                Select key cases other ->
                    getVar key
                        |> Result.andThen
                            (\n ->
                                format
                                    getVar
                                    (Dict.get n cases |> Maybe.withDefault other)
                            )
    in
    List.map eval tokens |> Result.Extra.combine |> Result.map (String.join "")


formats : (String -> Result String String) -> String -> Result String String
formats getVar =
    parse >> Result.andThen (format getVar)



-- Codegen
-- TODO: the whole plan here is wrong - exported functions are hard to language-switch. export either a record or a union type.
-- TODO: should probably break this down into more primitives like codegenString
-- TODO: webpack loader, file i/o, spreadsheet integration


codegen : String -> List ( String, Message ) -> String
codegen module_ msgs =
    "module "
        ++ module_
        ++ " exposing ("
        ++ (msgs |> List.map Tuple.first |> String.join ", ")
        ++ ")\n\n"
        ++ String.join "\n\n" (msgs |> List.map (\( name, msg ) -> codegenMsg name msg))
        ++ "\n"


codegens : String -> List ( String, String ) -> Result String String
codegens module_ msgs =
    msgs
        |> List.map (\( key, msg ) -> parse msg |> Result.map (Tuple.pair key))
        |> Result.Extra.combine
        |> Result.map (codegen module_)


codegenMsg : String -> Message -> String
codegenMsg name msg =
    let
        args =
            codegenMsgArgs msg

        sig =
            case args of
                [] ->
                    ""

                _ ->
                    " args"
    in
    name ++ " : " ++ argsToType args ++ "\n" ++ name ++ sig ++ " = " ++ codegenMsgBody msg


argsToType : List String -> String
argsToType vars =
    case vars of
        [] ->
            "String"

        _ ->
            "{ a | " ++ String.join ", " (List.map (\var -> var ++ " : String") vars) ++ " } -> String"


codegenMsgArgs : Message -> List String
codegenMsgArgs (Message tokens) =
    tokens |> List.concatMap codegenTokenArgs |> Set.fromList |> Set.toList


codegenTokenArgs : Token -> List String
codegenTokenArgs token =
    case token of
        Text _ ->
            []

        Var var ->
            [ var ]

        Plural var dict other ->
            var :: (other :: (dict |> Dict.values) |> List.concatMap codegenMsgArgs)

        Select var dict other ->
            var :: (other :: (dict |> Dict.values) |> List.concatMap codegenMsgArgs)


codegenMsgBody : Message -> String
codegenMsgBody (Message tokens) =
    tokens |> List.map codegenTokenBody |> String.join " ++ "


codegenString : String -> String
codegenString str =
    "\"" ++ (str |> String.replace "\\" "\\\\" |> String.replace "\"" "\\\"") ++ "\""


codegenTokenBody : Token -> String
codegenTokenBody token =
    case token of
        Text text ->
            codegenString text

        Var var ->
            "args." ++ var

        Plural var cases other ->
            -- identical, for now
            codegenTokenBody <| Select var cases other

        Select var cases other ->
            "(case args."
                ++ var
                ++ " of\n"
                ++ (cases
                        |> Dict.toList
                        |> List.map
                            (\( match, msg ) ->
                                "    "
                                    ++ codegenString match
                                    ++ " -> "
                                    ++ codegenMsgBody msg
                                    ++ "\n"
                            )
                        |> String.concat
                   )
                ++ "    _ -> "
                ++ codegenMsgBody other
                ++ "\n)"
