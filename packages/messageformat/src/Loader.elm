module Loader exposing (main)

import Platform


main : Platform.Program Flags Model ()
main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = always Sub.none
        }


update model _ =
    model


init () =
    "hello"
