import Elm from './Loader.elm'
import utils from 'loader-utils'
const fs = require('fs').promises

export default function loader(content, map, meta) {
  this.cacheable && this.cacheable()
  const callback = this.async()
  const opts = utils.getOptions(this) || {}
  fs.readFile(this.resourcePath)
  .then(JSON.parse)
  .then(json => {
    console.log(json)
    const js = "module.exports = \"TODO: generated elm filename\""
    callback(null, js, map, meta)
  })
}
