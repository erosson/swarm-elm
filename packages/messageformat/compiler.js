// from https://webpack.js.org/contribute/writing-a-loader/#testing
import path from 'path'
import webpack from 'webpack'
import memoryfs from 'memory-fs'

export default (fixture, options = {}) => {
  const config = {
    context: __dirname,
    entry: `./${fixture}`,
    output: {
      path: path.resolve(__dirname),
      filename: `./${fixture.replace(/\.ods$/,'.js')}`,
    },
    module: {
      rules: [{
        test: /\.ods$/,
        use: [
          'json-loader',
          {
            loader: path.resolve(__dirname, '../src/loader.js'),
            options,
          },
        ]
      }]
    }
  }
  const compiler = webpack(config);

  compiler.outputFileSystem = new memoryfs();

  return new Promise((resolve, reject) => {
    compiler.run((err, stats) => {
      // if (err || stats.hasErrors()) reject(err);
      if (err) reject(err);

      resolve(stats);
    });
  });
};
