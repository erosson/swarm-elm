// import loader from '../xsrc/loader.js' // unused, but checks for parse errors
import compiler from './compiler.js'

//function sourceJson(source) {
//  return JSON.parse(source.replace(/^module.exports = /, ''))
//}

test('runs', async () => {
  let {stats, outputFileSystem} = (await compiler('example.messages'))
  stats = stats.toJson()
  // console.log(stats)
  expect(stats.errors).toEqual([])
  expect(stats.modules[0].source).toEqual("module.exports = \"TODO: generated elm filename\"")
  // console.log(outputFileSystem.readdirSync('/'))
  expect(outputFileSystem.readdirSync(process.cwd()+'/tests')).toEqual([])
})
